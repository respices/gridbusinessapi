<?php

namespace Database\Factories;

use App\Models\Privacy;
use Illuminate\Database\Eloquent\Factories\Factory;

class PrivacyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Privacy::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'partnerId' => $this->faker->randomDigitNotNull,
        'showEmailInDirectory' => $this->faker->word,
        'showPhoneNumberInDirectory' => $this->faker->word,
        'showBranches' => $this->faker->word,
        'showInDirectory' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
