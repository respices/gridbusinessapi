<?php

namespace Database\Factories;

use App\Models\PartnerSubscription;
use Illuminate\Database\Eloquent\Factories\Factory;

class PartnerSubscriptionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PartnerSubscription::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'partnerId' => $this->faker->word,
            'subscriptionId' => $this->faker->randomDigitNotNull,
            'dateSubscribed' =>$this->faker->date('Y-m-d H:i:s'),
            'status' => 'Trialing',
            'paymentId' => $this->faker->randomDigitNotNull,
            'created_at' => $this->faker->date('Y-m-d H:i:s'),
            'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
