<?php

namespace Database\Factories;

use App\Models\BusinessAddress;
use Illuminate\Database\Eloquent\Factories\Factory;

class BusinessAddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BusinessAddress::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'grideCode' => $this->faker->word,
        'cityId' => $this->faker->randomDigitNotNull,
        'stateId' => $this->faker->randomDigitNotNull,
        'countryId' => $this->faker->randomDigitNotNull,
        'partnerId' => $this->faker->randomDigitNotNull,
        'area' => $this->faker->word,
        'telephone' => $this->faker->word,
        'telephone2' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
