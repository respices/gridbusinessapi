<?php

namespace Database\Factories;

use App\Models\PartnerCredit;
use Illuminate\Database\Eloquent\Factories\Factory;

class PartnerCreditFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PartnerCredit::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'partnerId' => $this->faker->word,
        'partnerSubscriptionId' => $this->faker->randomDigitNotNull,
        'credits' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
