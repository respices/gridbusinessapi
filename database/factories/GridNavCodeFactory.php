<?php

namespace Database\Factories;

use App\Models\GridNavCode;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class GridNavCodeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = GridNavCode::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'gridCodeId' => Str::random(5),
            'gridCode' => Str::random(10),
            'latitude' => Str::random(10),
            'longitude' => Str::random(10),
            'title' => $this->faker->paragraph,
        ];
    }
}
