<?php

namespace Database\Factories;

use App\Models\BusinessContact;
use Illuminate\Database\Eloquent\Factories\Factory;

class BusinessContactFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BusinessContact::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'businessContactId' => $this->faker->randomDigitNotNull,
        'partnerId' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
