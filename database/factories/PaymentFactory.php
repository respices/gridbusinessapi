<?php

namespace Database\Factories;

use App\Models\Payment;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaymentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Payment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'amount' => $this->faker->word,
        'currencyId' => $this->faker->randomDigitNotNull,
        'paymentStatus' => $this->faker->word,
        'paymentMethod' => $this->faker->word,
        'paymentConfirmationId' => $this->faker->word,
        'paymentGatewayId' => $this->faker->randomDigitNotNull,
        'partnerId' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
