<?php

namespace Database\Factories;

use App\Models\BusinessRegistration;
use Illuminate\Database\Eloquent\Factories\Factory;

class BusinessRegistrationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BusinessRegistration::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'partnerId' => $this->faker->word,
            'businessName' => $this->faker->word,
            'businessEmail' => $this->faker->word,
            'description' => $this->faker->word,
            'website' => $this->faker->word,
            'logo' => $this->faker->word,
            'status' => 'Inactive',
            'isHeadQuarter' => $this->faker->word,
            'headQuarterId' => $this->faker->word,
            'businessCategoryId' => $this->faker->randomDigitNotNull,
            'userId' => $this->faker->randomDigitNotNull,
            'created_at' => $this->faker->date('Y-m-d H:i:s'),
            'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
