<?php

namespace Database\Factories;

use App\Models\PartnerConsumerCredit;
use Illuminate\Database\Eloquent\Factories\Factory;

class PartnerConsumerCreditFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PartnerConsumerCredit::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'partnerId' => $this->faker->word,
        'credit' => $this->faker->word,
        'api_url' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
