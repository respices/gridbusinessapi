<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->increments('id');
            $table->string('stateName');
            $table->integer('countryId')->unsigned();
           
            $table->foreign('countryId')->references('id')->on('countries')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
        \DB::update("ALTER TABLE states AUTO_INCREMENT = 1001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('states');
    }
}
