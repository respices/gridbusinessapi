<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessAddressesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businessAddresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('grideCode');
            $table->string('gridCodeStatus');
            $table->integer('cityId')->unsigned();
            $table->integer('stateId')->unsigned();
            $table->integer('countryId')->unsigned();

            $table->uuid('partnerId');

            $table->string('address');
            $table->string('telephone');
            $table->string('telephone2')->nullable();
            $table->boolean('isTelephoneVerified')->default(0);
            $table->boolean('isTelephone2Verified')->default(0);

            $table->string('landmark');
            $table->foreign('cityId')->references('id')->on('cities')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('stateId')->references('id')->on('states')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('countryId')->references('id')->on('countries')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('partnerId')->references('partnerId')->on('businessRegistrations')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });

        \DB::update("ALTER TABLE businessAddresses AUTO_INCREMENT = 1001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('businessAddresses');
    }
}
