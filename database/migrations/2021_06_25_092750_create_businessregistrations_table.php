<?php

use App\Enums\BusinessRegistration;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessRegistrationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businessRegistrations', function (Blueprint $table) {
            $table->uuid('partnerId')->primary();
            $table->string('businessName');
            $table->string('businessEmail');
            $table->longText('description');
            $table->string('website');
            $table->string('logo');
            $table->enum('status', BusinessRegistration::$status)->default('Inactive');
            $table->boolean('isHeadQuarter');
            $table->string('headQuarterId');
            $table->integer('businessCategoryId')->unsigned();
            
            $table->unsignedBigInteger('userId');
            $table->integer('partnerCredits')->default(0);
            $table->foreign('businessCategoryId')->references('id')->on('businessCategories')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('userId')->references('id')->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('businessRegistrations');
    }
}
