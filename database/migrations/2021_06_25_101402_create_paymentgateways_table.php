<?php

use App\Enums\PaymentGateway;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentGatewaysTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paymentGateways', function (Blueprint $table) {
            $table->increments('id');
            $table->string('paymentGatewayName');
            $table->string('publicKey');
            $table->string('secretKey');
            $table->enum('status', PaymentGateway::$status)->default('Inactive');
            $table->date('lastUsedDate');
            $table->timestamps();
        });
        \DB::update("ALTER TABLE paymentGateways AUTO_INCREMENT = 1001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paymentGateways');
    }
}
