<?php

use App\Enums\Payment;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('amount');
            $table->enum('paymentStatus', Payment::$paymentStatus)->default('pending');
            $table->enum('paymentMethod', Payment::$paymentMethod);
            $table->string('paymentConfirmationId');
            $table->integer('currencyId')->unsigned();
            $table->integer('paymentGatewayId')->unsigned();
            $table->uuid('partnerId');
            $table->timestamps();

            $table->foreign('currencyId')->references('id')->on('currencies')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('paymentGatewayId')->references('id')->on('paymentGateways')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('partnerId')->references('partnerId')->on('businessRegistrations')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });

        \DB::update("ALTER TABLE payments AUTO_INCREMENT = 1001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments');
    }
}
