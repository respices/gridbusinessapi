<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessLogsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accessLogs', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('partnerId')->nullable();
            $table->string('searchedGridCode')->nullable();
            $table->string('ipAddress')->nullable();
            $table->text('sourceUrl')->nullable();
            $table->string('sourceCountry')->nullable();
            $table->dateTime('conStartDateTime');
            $table->dateTime('conEndDateTime');
            $table->timestamps();
           
        });

        \DB::update("ALTER TABLE accessLogs AUTO_INCREMENT = 1001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('AccessLogs');
    }
}
