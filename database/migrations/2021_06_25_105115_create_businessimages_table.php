<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessImagesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businessImages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imageUrl');
            $table->uuid('partnerId');
            $table->timestamps();
            $table->foreign('partnerId')->references('partnerId')->on('businessRegistrations')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
        \DB::update("ALTER TABLE businessImages AUTO_INCREMENT = 1001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('businessImages');
    }
}
