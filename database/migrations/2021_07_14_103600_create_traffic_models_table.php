<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrafficModelsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traffic_models', function (Blueprint $table) {
            $table->increments('id');
            $table->string('model');
            $table->boolean('default')->default(0);
            $table->timestamps();
        });
        \DB::update("ALTER TABLE traffic_models AUTO_INCREMENT = 1001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('traffic_models');
    }
}
