<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGridNavCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grid_codes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('gridCodeId');
            $table->string('gridCode');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('title');

            $table->timestamps();
        });
        \DB::update("ALTER TABLE grid_codes AUTO_INCREMENT = 1001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grid_nav_codes');
    }
}
