<?php

use App\Enums\PartnerSubscription;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnerSubscriptionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partnerSubscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('partnerId');
            $table->integer('subscriptionId')->unsigned();
            $table->integer('paymentId')->nullable()->unsigned();
            $table->dateTime('dateSubscribed');
            $table->dateTime('dateExpired');
            $table->integer('subscriptionQuantity');
            $table->string('subscriptionPeriod')->default('Month');
            $table->enum('status', PartnerSubscription::$status)->default('Active');
          
            $table->timestamps();
            $table->foreign('partnerId')->references('partnerId')->on('businessRegistrations')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('subscriptionId')->references('id')->on('subscriptions')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('paymentId')->references('id')->on('payments')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });

        \DB::update("ALTER TABLE partnerSubscriptions AUTO_INCREMENT = 1001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('partnerSubscriptions');
    }
}
