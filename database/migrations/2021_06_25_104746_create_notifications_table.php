<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('notificationTypeId')->unsigned();
            $table->uuid('partnerId');
            $table->longText('message');
            $table->timestamps();
            $table->foreign('notificationTypeId')->references('id')->on('notificationTypes')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('partnerId')->references('partnerId')->on('businessRegistrations')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });

        \DB::update("ALTER TABLE notifications AUTO_INCREMENT = 1001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notifications');
    }
}
