<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnerCreditsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_credits', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('partnerId');
            $table->integer('partnerSubscriptionId')->unsigned();
            $table->bigInteger('credits');

            $table->foreign('partnerId')->references('partnerId')->on('businessRegistrations')
            ->onDelete('cascade');

            $table->foreign('partnerSubscriptionId')->references('id')->on('partnerSubscriptions')
            ->onDelete('cascade')
            ->onUpdate('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('partner_credits');
    }
}
