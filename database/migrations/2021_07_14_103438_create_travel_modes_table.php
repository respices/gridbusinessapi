<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTravelModesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_modes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mode');
            $table->boolean('default')->default(0);
            $table->timestamps();
        });
        \DB::update("ALTER TABLE travel_modes AUTO_INCREMENT = 1001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('travel_modes');
    }
}
