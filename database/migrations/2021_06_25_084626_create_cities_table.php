<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cityName');
            $table->integer('stateId')->unsigned();
           
            $table->foreign('stateId')->references('id')
            ->on('states')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
        \DB::update("ALTER TABLE cities AUTO_INCREMENT = 1001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cities');
    }
}
