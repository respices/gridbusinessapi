<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrivaciesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('privacies', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('showEmailInDirectory');
            $table->boolean('showPhoneNumberInDirectory');
            $table->boolean('showBranches');
            $table->boolean('showInDirectory');
            $table->boolean('showContactPersonInDirectory');
            $table->uuid('partnerId');
            $table->timestamps();
            $table->foreign('partnerId')->references('partnerId')->on('businessRegistrations')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
        \DB::update("ALTER TABLE privacies AUTO_INCREMENT = 1001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('privacies');
    }
}
