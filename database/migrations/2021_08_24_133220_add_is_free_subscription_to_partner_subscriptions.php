<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsFreeSubscriptionToPartnerSubscriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partnerSubscriptions', function (Blueprint $table) {
            $table->boolean('isFreeSubscription')->default(0);
        });
    }

    /**
     *  Reverse the migrations.
     *
     *  @return void
     */

    public function down()
    {
        Schema::table('partnerSubscriptions', function (Blueprint $table) {
             $table->boolean('isFreeSubscription')->default(0);
        });   
    }
}
