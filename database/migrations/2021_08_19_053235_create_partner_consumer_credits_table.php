<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnerConsumerCreditsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_consumer_credits', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('partnerId');
            $table->bigInteger('credit');
            $table->string('api_url');

            $table->foreign('partnerId')->references('partnerId')->on('businessRegistrations')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('partner_consumer_credits');
    }
}
