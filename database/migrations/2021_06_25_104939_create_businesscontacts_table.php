<?php

use App\Enums\BusinessContact;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessContactsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businessContacts', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('businessContactId');
            $table->uuid('partnerId');
            $table->enum('status', BusinessContact::$status)->default('Pending');
            $table->dateTime('action_date')->nullable();
            $table->timestamps();
            $table->foreign('businessContactId')->references('partnerId')->on('businessRegistrations')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('partnerId')->references('partnerId')->on('businessRegistrations')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });

        \DB::update("ALTER TABLE businessContacts AUTO_INCREMENT = 1001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('businessContacts');
    }
}
