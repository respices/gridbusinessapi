<?php

namespace Database\Seeders;

use App\Models\PartnerSubscription;
use Illuminate\Database\Seeder;

class PartnerSubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PartnerSubscription::factory()
        ->count(10)->create();
    }
}
