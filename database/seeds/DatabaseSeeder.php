<?php

use Database\Seeders\PartnerSubscriptionSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(PartnerSubscriptionSeeder::class);
    }
}
