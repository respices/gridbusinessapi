# GRID BUSINESS PORTAL REST API

## Before using

-   Please make sure that you have:
-   Visual studio code(https://code.visualstudio.com/download) for editing and running the app
-   Postman(https://www.getpostman.com/downloads/) to test the Api endpoints
-   Composer installed (https://getcomposer.org/download/)
-   Make sure Mysql installed and running locally (latest stable version: 5.1.1)
    -   For Window: you can download xampp app(https://www.apachefriends.org/xampp-files/8.0.7/xampp-windows-x64-8.0.7-0-VS16-installer.exe) install it in your computer then after installation is completed, you can start mysql.
    -   For lunix: Install Mysql by following this link (https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-18-04)

## Installation

#### A. Clone the project

1. From your computer, open terminal
2. Run `git clone https://gitlab.com/phemmywales/gridbusiness.git` to clone the repository.
3. go to your project folder `cd gridbusiness`
4. run `composer install` using terminal to install all dependencies.
5. Make a Copy of `.env.example` the name it `.env` in root directory
6. In `.env` please replace environment variables with your value

## Run Project

    1. run php artisan key:generate
    2. run php artisan migrate
    3. run php artisan passport:install
    3. run php artisan serve

## Here is the description of common server responses:

1. 200 OK - the request was successful (some API calls may return 201 instead).
2. 201 Created - the request was successful and a resource was created.
3. 204 No Content - the request was successful but there is no representation to return
   (i.e. the response is empty).
4. 400 Bad Request - the request could not be understood or was missing required parameters.
5. 401 Unauthorized - authentication failed or user doesn't have permissions for requested operation.
6. 403 Forbidden - access denied.
7. 404 Not Found - resource was not found.
8. 405 Method Not Allowed - requested method is not supported for resource.
9. 406 Not Acceptable - the target resource does not have a current representation that would be acceptable to the user agent, according to the proactive negotiation header fields received in the request.
10. 409 Conflict - request could not be completed due to a conflict with the current state of the target resource.
11. 422 Unprocessable Entity - requested data contain invalid values.
12. 429 Too Many Requests - exceeded API limits.
    Pause requests, wait up to one minute, and try again (you can check rate limits in X-RATELIMIT-LIMIT and X-RATELIMIT-REMAINING headers).

API provides JSON request and response.

# Header can be set by:
1. Accept:application/json
2. Authorization:Bearer {{token}}


## API endpoints

      . Open this link ->
          https://www.getpostman.com/collections/68b65f3af8a2310aa2e3
        to access POSTMAN APIs

# to get machine details [IP, HOST DETAILS, HARDWARE DETAILS AND SOFTWARE DETAILS]
    https://github.com/matriphe/larinfo/tree/2.2

# A simple library to encode and decode JSON Web Tokens (JWT) in PHP, conforming to RFC 7519.
  https://github.com/firebase/php-jwt