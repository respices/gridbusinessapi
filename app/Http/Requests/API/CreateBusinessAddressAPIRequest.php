<?php

namespace App\Http\Requests\API;

use App\Models\BusinessAddress;
use InfyOm\Generator\Request\APIRequest;

class CreateBusinessAddressAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return BusinessAddress::$rules;
    }
    public function messages()
    {
        return [
            'partnerId.unique' => 'The partner already exists(Each partner belongs to one business address)',
        ];
    }
}
