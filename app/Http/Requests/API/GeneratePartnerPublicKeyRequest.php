<?php

namespace App\Http\Requests\API;

use InfyOm\Generator\Request\APIRequest;

class GeneratePartnerPublicKeyRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *  
     * @return array
     */
    public function rules()
    {
        return [
            'partnerId' => 'required|string|exists:businessRegistrations,partnerId',
            'partnerBusinessName' => 'required|string|exists:businessRegistrations,businessName',
            'userId' => 'required|integer|exists:users,id'
        ];
    }

    public function getAttributes() {
        return $this->validated();
    }
}