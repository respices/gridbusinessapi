<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'email' => $this->email,
            'isEmailVerified'=>$this->email_verified_at?true:false,
            'activeBusiness' => User::activeBusiness($this->id),
            'hasBusiness'=>User::activeBusiness($this->id)?true:false,
            'profileImageUrl' =>$this->avatar? Storage::disk('azure')->url('/profileImages/users/'.  $this->avatar):config('app.portalDomainUrl').'/avatar.png',
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
