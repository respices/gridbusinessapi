<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BusinessContactResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'businessContactId' => $this->businessContactId,
            'partnerId' => $this->partnerId,
            'status'=> $this->status,
            'action_date'=> $this->action_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
