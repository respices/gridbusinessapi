<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class BusinessImageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request, $visible = false)
    {
        $fields = [];

        if ($visible) {
            $fields = ['created_at' => $this->created_at,'updated_at' => $this->updated_at];
        }
        
        return  array_merge([
            'id' => $this->id,
            'imageUrl' => Storage::disk('azure')->url('/businessImages/' .  $this->partnerId . '/' . $this->imageUrl),
            'partnerId' => $this->partnerId
        ], $fields);
    }
}
