<?php

namespace App\Http\Resources;

use App\Models\Payment;
use Illuminate\Http\Resources\Json\JsonResource;

class PaymentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $fields = [];

     
        return  array_merge([
            'partner'=>$this->businessRegistration,
            'partnerAddress' =>count($this->businessRegistration->businessAddresses) > 0? BusinessAddressResource::collection($this->businessRegistration->businessAddresses)[0]:null,
            'id' => $this->id,
            'amount' => $this->amount,
            'currencyId' => $this->currencyId,
            'paymentStatus' => $this->paymentStatus,
            'paymentMethod' => $this->paymentMethod,
            'paymentConfirmationId' => $this->paymentConfirmationId,
            'paymentGatewayId' => $this->paymentGatewayId,
            'partnerId' => $this->partnerId,
            'subscription'=>Payment::subscription($this->id),
            'partnerSubscription'=>Payment::partnerSubscription($this->id),
            'created_at' => $this->created_at, 
            'updated_at' => $this->updated_at,
            'issuedDate'=>date("d/m/Y H:i:s", strtotime($this->updated_at)),
            'currency'=>$this->currency->currencySymbol
        ],$fields);
    }
}
