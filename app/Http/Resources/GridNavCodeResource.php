<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GridNavCodeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'gridCode'=> $this->gridCode,
            'gridCodeId'=> $this->gridCodeId,
            'latitude'=> $this->latitude,
            'longitude'=> $this->longitude,
            'title'=> $this->title,
            'googleMapURL'=>'http://maps.google.com?q='.$this->latitude.','.$this->longitude,
            'created_at'=> $this->created_at,
            'updated_at'=> $this->updated_at,
        ];
    }
}
