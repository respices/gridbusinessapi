<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BusinessAddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      

        return [
            'id' => $this->id,
            'grideCode' => $this->grideCode,
            'cityId' => $this->cityId,
            'stateId' => $this->stateId,
            'countryId' => $this->countryId,
            'city' => $this->city,
            'state' => $this->state,
            'country' => $this->country,
            'partnerId' => $this->partnerId,
            'landmark' => $this->landmark,
            'address' => $this->address,
            'telephone' => $this->telephone,
            'telephone2' => $this->telephone2,

            'isTelephoneVerified' => (bool)$this->isTelephoneVerified,
            'isTelephone2Verified' => (bool)$this->isTelephone2Verified,
        ];
    }
}
