<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CurrencyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'currencyName' => $this->currencyName,
            'currencySymbol' => $this->currencySymbol,
            'payments'=>$this->payments,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
