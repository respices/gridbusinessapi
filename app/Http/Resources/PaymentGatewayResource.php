<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PaymentGatewayResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'paymentGatewayName' => $this->paymentGatewayName,
            'publicKey' => $this->publicKey,
            'secretKey' => $this->secretKey,
            'status' => $this->status,
            'lastUsedDate' => $this->lastUsedDate,
            'payments'=>$this->payments,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
