<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CountryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'countryCode' => $this->countryCode,
            'countryName' => $this->countryName,
            'created_at' => $this->created_at,
            'states'=>$this->states,
            'businessAddresses'=>$this->businessAddresses,
            'updated_at' => $this->updated_at
        ];
    }
}
