<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PartnerConsumerCreditResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'partnerId' => $this->partnerId,
            'credit' => $this->credit,
            'api_url' => $this->api_url,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
