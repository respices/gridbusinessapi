<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AccessLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'partnerId' => $this->partnerId,
            'searchedGridCode' => $this->searchedGridCode,
            'ipAddress' => $this->ipAddress,
            'sourceUrl' => $this->sourceUrl,
            'sourceCountry'=> $this->sourceCountry,
            'conStartDateTime' => $this->conStartDateTime,
            'conEndDateTime' => $this->conEndDateTime,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
