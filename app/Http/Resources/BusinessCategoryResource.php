<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BusinessCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $fields = [];

        if ($request->has('viewHiddenColumns')) {
            $fields = ['businesses' => $this->businessRegistrations, 'created_at' => $this->created_at, 'updated_at' => $this->updated_at];
        }

        return  array_merge([
            'id' => $this->id,
            'description' => $this->description
        ], $fields);
    }
}
