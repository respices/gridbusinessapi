<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PrivacyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'partnerId' => $this->partnerId,
            'showEmailInDirectory' => $this->showEmailInDirectory,
            'showPhoneNumberInDirectory' => $this->showPhoneNumberInDirectory,
            'showBranches' => $this->showBranches,
            'showInDirectory' => $this->showInDirectory,
            'allowUnsusedCreditRollover' => $this->allowUnsusedCreditRollover,
            'allowUnsusedSubscriptionRollover' => $this->allowUnsusedSubscriptionRollover,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
