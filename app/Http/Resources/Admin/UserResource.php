<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;
use Cache;
use Illuminate\Support\Facades\Storage;

class UserResource  extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'email' => $this->email,
            'isEmailVerified'=>$this->email_verified_at?true:false,
            'profileImageUrl' =>$this->avatar? Storage::disk('azure')->url('/profileImages/users/'.  $this->avatar):config('app.portalDomainUrl').'/avatar.png',
            'lastLoginDate'=>$this->lastLoginDate,
            // 'userOnlineStatus'=>Cache::has('user-is-online-' . $this->id)?'IsOnline':'IsOffline',
            'lastLoginDateHumanReadable'=>\Carbon\Carbon::parse($this->lastLoginDate)->diffForHumans()
        ];
    }
}
