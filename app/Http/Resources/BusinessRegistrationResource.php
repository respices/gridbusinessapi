<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class BusinessRegistrationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $fields = [];
        

        if ($request->user() && $request->user()->id == $this->userId) {
             
            // $payments = $this->payments->map(function ($payment)  {
            //     return $payment->paymentStatus!=='pending';
            // });

            $fields = [
                'status' => $this->status,
                'subscriptions' => PartnerSubscriptionResource::collection($this->subscriptions),
                'payments'=> PaymentResource::collection($this->payments),
                'userId' => $this->userId, 
                'created_at' => $this->created_at,
                 'updated_at' => $this->updated_at
            ];
        }

        return  array_merge([
            'partnerId' => $this->partnerId,
            'businessName' => $this->businessName,
            'businessEmail' => $this->businessEmail,
            'description' => $this->description,
            'website' => $this->website,
            'logo' => Storage::disk('azure')->url('/businessLogo/' . $this->logo),
            'isHeadQuarter' => $this->isHeadQuarter,
            'headQuarterId' => $this->headQuarterId,
            'businessCategoryId' => $this->businessCategoryId,
            'partnerCategory' => new BusinessCategoryResource($this->businessCategory),
            'partnerAddresses' =>count($this->businessAddresses) > 0? BusinessAddressResource::collection($this->businessAddresses)[0]:null,
            'partnerImages' => BusinessImageResource::collection($this->businessImages),

        ], $fields);
    }
}
