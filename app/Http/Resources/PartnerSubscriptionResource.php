<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PartnerSubscriptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'partnerId' => $this->partnerId,
            'subscriptionId' => $this->subscriptionId,
            'subscription' => $this->subscription,
            'dateSubscribed' => $this->dateSubscribed,
            'dateExpired' => $this->dateExpired,
            'subscriptionQuantity' => $this->subscriptionQuantity,
            'subscriptionPeriod' => $this->subscriptionPeriod,
            'status' => $this->status,
            'paymentId' => $this->paymentId,
            'payment' => $this->payment?$this->payment:null,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
