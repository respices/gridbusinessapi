<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SubscriptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request, $visible = false)
    {
        $fields = [];

        if ($visible) {
            $fields = [
                'created_at' => $this->created_at, 'updated_at' => $this->updated_at
            ];
        }
        return  array_merge([
            'id' => $this->id,
            'price'=>$this->price,
            'title' => $this->title,
            'description' => $this->description,
            'credit' => $this->credit,
            'isDefault' => $this->isDefault,
            'currencyId' => $this->currencyId,
            'currency'=>$this->currency?$this->currency->currencySymbol:''
        ], $fields);
    }
}
