<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePartnerSubscriptionAPIRequest;
use App\Http\Requests\API\UpdatePartnerSubscriptionAPIRequest;
use App\Models\PartnerSubscription;
use App\Repositories\PartnerSubscriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\PartnerSubscriptionResource;
use App\Models\PartnerCredit;
use App\Models\Payment;
use App\Models\PaymentGateway;
use App\Models\Subscription;
use App\Models\User;
use App\Traits\GDateTime;
use Response;

/**
 * Class PartnerSubscriptionController
 * @package App\Http\Controllers\API
 */

class PartnerSubscriptionAPIController extends AppBaseController
{
    use GDateTime;
    /** @var  PartnerSubscriptionRepository */
    private $partnerSubscriptionRepository;

    public function __construct(PartnerSubscriptionRepository $partnerSubscriptionRepo)
    {
        $this->partnerSubscriptionRepository = $partnerSubscriptionRepo;
    }

    /**
     * Display a listing of the PartnerSubscription.
     * GET|HEAD /partnerSubscriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if (empty($request->get('partnerId'))) {
            return $this->sendError('PartnerId not found');
        }
        $request->merge(['partnerId' => $request->get('partnerId')]);
        $partnerSubscriptions = $this->partnerSubscriptionRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );
        $message = count($partnerSubscriptions) > 0?'Partner Subscriptions retrieved successfully':'No results found!';
        return $this->sendResponse(PartnerSubscriptionResource::collection($partnerSubscriptions), $message);
    }

    /**
     * Store a newly created PartnerSubscription in storage.
     * POST /partnerSubscriptions
     *
     * @param CreatePartnerSubscriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePartnerSubscriptionAPIRequest $request)
    {
        $input = $request->all();

        $partnerSubscription = $this->partnerSubscriptionRepository->create($input);

        return $this->sendResponse(new PartnerSubscriptionResource($partnerSubscription), 'Partner Subscription saved successfully');
    }

    public function storeFreeSubscription(Request $request)
    {
   
            $subscription = Subscription::find($request->get('subscriptionId'));
            $paymentGatway = PaymentGateway::activeGateway('Flutterwave');

           $payment = Payment::updateOrCreate([
                'partnerId' => $request->get('partnerId'),
                'paymentStatus' => 'Pending'
                ],[
                'amount'=>$subscription->price,
                'currencyId'=>$subscription->currencyId,
                'paymentStatus'=>'successful',
                'paymentMethod'=>'Free',
                'paymentConfirmationId'=>'None',
                'partnerId'=>$request->get('partnerId'),
                'paymentGatewayId'=>$paymentGatway->id
            ]);
       

            if($payment){
                    
                $partnerSubscription =PartnerSubscription::updateOrCreate([
                    'partnerId' => $request->get('partnerId'),
                    'status' => 'Pending'
                    ],
                    [
                    'partnerId'=>$request->get('partnerId'),
                    'subscriptionId'=>$subscription->id,
                    'dateSubscribed'=>$this->getTodayDate(),
                    'dateExpired'=>$this->getNextDate('month',1),
                    'subscriptionQuantity'=>1,
                    'subscriptionPeriod'=>'month',
                    'paymentId'=>$payment->id,
                    'isFreeSubscription'=>1,
                    'currentCredits'=>$subscription->credit,
                    'status'=>'Active'
                    ]
                );
                   
            }

        return $this->sendResponse(new PartnerSubscriptionResource($partnerSubscription), 'Partner Subscription saved successfully');
    }

    /**
     * Display the specified PartnerSubscription.
     * GET|HEAD /partnerSubscriptions/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PartnerSubscription $partnerSubscription */
        $partnerSubscription = $this->partnerSubscriptionRepository->find($id);

        if (empty($partnerSubscription)) {
            return $this->sendError('Partner Subscription not found');
        }

        return $this->sendResponse(new PartnerSubscriptionResource($partnerSubscription), 'Partner Subscription retrieved successfully');
    }

    /**
     * Update the specified PartnerSubscription in storage.
     * PUT/PATCH /partnerSubscriptions/{id}
     *
     * @param int $id
     * @param UpdatePartnerSubscriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var PartnerSubscription $partnerSubscription */
        $partnerSubscription = $this->partnerSubscriptionRepository->find($id);

        if (empty($partnerSubscription)) {
            return $this->sendError('Partner Subscription not found');
        }

        if($request->get('status')=='Active'){
            PartnerSubscription::makePartnerSubscriptionActive($partnerSubscription->partnerId, $id);
        }else{
            $partnerSubscription = $this->partnerSubscriptionRepository->update($input, $id);
        }

        

        return $this->sendResponse(new PartnerSubscriptionResource($partnerSubscription), 'PartnerSubscription updated successfully');
    }

    /**
     * Remove the specified PartnerSubscription from storage.
     * DELETE /partnerSubscriptions/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PartnerSubscription $partnerSubscription */
        $partnerSubscription = $this->partnerSubscriptionRepository->find($id);

        if (empty($partnerSubscription)) {
            return $this->sendError('Partner Subscription not found');
        }

        $partnerSubscription->delete();

        return $this->sendSuccess('Partner Subscription deleted successfully');
    }
}
