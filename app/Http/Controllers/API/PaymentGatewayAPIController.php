<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePaymentGatewayAPIRequest;
use App\Http\Requests\API\UpdatePaymentGatewayAPIRequest;
use App\Models\PaymentGateway;
use App\Repositories\PaymentGatewayRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\PaymentGatewayResource;
use Response;

/**
 * Class PaymentGatewayController
 * @package App\Http\Controllers\API
 */

class PaymentGatewayAPIController extends AppBaseController
{
    /** @var  PaymentGatewayRepository */
    private $paymentGatewayRepository;

    public function __construct(PaymentGatewayRepository $paymentGatewayRepo)
    {
        $this->middleware(['auth:admin','scope:admin'])->except(['index','show']);
        $this->paymentGatewayRepository = $paymentGatewayRepo;
    }

    /**
     * Display a listing of the PaymentGateway.
     * GET|HEAD /paymentGateways
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $paymentGateways = $this->paymentGatewayRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );
        $message = count($paymentGateways) > 0?'Payment Gateways retrieved successfully':'No results found!';
        return $this->sendResponse(PaymentGatewayResource::collection($paymentGateways), $message);
    }

    /**
     * Store a newly created PaymentGateway in storage.
     * POST /paymentGateways
     *
     * @param CreatePaymentGatewayAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePaymentGatewayAPIRequest $request)
    {
        $input = $request->all();

        $paymentGateway = $this->paymentGatewayRepository->create($input);

        return $this->sendResponse(new PaymentGatewayResource($paymentGateway), 'Payment Gateway saved successfully');
    }

    /**
     * Display the specified PaymentGateway.
     * GET|HEAD /paymentGateways/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PaymentGateway $paymentGateway */
        $paymentGateway = $this->paymentGatewayRepository->find($id);

        if (empty($paymentGateway)) {
            return $this->sendError('Payment Gateway not found');
        }

        return $this->sendResponse(new PaymentGatewayResource($paymentGateway), 'Payment Gateway retrieved successfully');
    }

    /**
     * Update the specified PaymentGateway in storage.
     * PUT/PATCH /paymentGateways/{id}
     *
     * @param int $id
     * @param UpdatePaymentGatewayAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaymentGatewayAPIRequest $request)
    {
        $input = $request->all();

        /** @var PaymentGateway $paymentGateway */
        $paymentGateway = $this->paymentGatewayRepository->find($id);

        if (empty($paymentGateway)) {
            return $this->sendError('Payment Gateway not found');
        }

        $paymentGateway = $this->paymentGatewayRepository->update($input, $id);

        return $this->sendResponse(new PaymentGatewayResource($paymentGateway), 'PaymentGateway updated successfully');
    }

    /**
     * Remove the specified PaymentGateway from storage.
     * DELETE /paymentGateways/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PaymentGateway $paymentGateway */
        $paymentGateway = $this->paymentGatewayRepository->find($id);

        if (empty($paymentGateway)) {
            return $this->sendError('Payment Gateway not found');
        }

        $paymentGateway->delete();

        return $this->sendSuccess('Payment Gateway deleted successfully');
    }
}
