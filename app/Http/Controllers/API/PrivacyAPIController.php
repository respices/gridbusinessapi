<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePrivacyAPIRequest;
use App\Http\Requests\API\UpdatePrivacyAPIRequest;
use App\Models\Privacy;
use App\Repositories\PrivacyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\PrivacyResource;
use App\Models\User;
use Response;

/**
 * Class PrivacyController
 * @package App\Http\Controllers\API
 */

class PrivacyAPIController extends AppBaseController
{
    /** @var  PrivacyRepository */
    private $privacyRepository;

    public function __construct(PrivacyRepository $privacyRepo)
    {
        $this->privacyRepository = $privacyRepo;
    }

    /**
     * Display a listing of the Privacy.
     * GET|HEAD /privacies
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $request->merge(['partnerId'=>User::activeBusiness()->partnerId]);
        $privacies = $this->privacyRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );
        $message = count($privacies) > 0?'Privacies retrieved successfully':'No results found!';
        return $this->sendResponse(PrivacyResource::collection($privacies), $message);
    }

    /**
     * Store a newly created Privacy in storage.
     * POST /privacies
     *
     * @param CreatePrivacyAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePrivacyAPIRequest $request)
    {
        $input = $request->all();

        $privacy = $this->privacyRepository->create($input);

        return $this->sendResponse(new PrivacyResource($privacy), 'Privacy saved successfully');
    }

    /**
     * Display the specified Privacy.
     * GET|HEAD /privacies/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Privacy $privacy */
        $privacy = $this->privacyRepository->find($id);

        if (empty($privacy)) {
            return $this->sendError('Privacy not found');
        }

        return $this->sendResponse(new PrivacyResource($privacy), 'Privacy retrieved successfully');
    }
    //getPartnerPrivacy

    public function getPartnerPrivacy($partnerId)
    {
        /** @var Privacy $privacy */

        $privacy = Privacy::where('partnerId',$partnerId)->first();

        if (empty($privacy)) {
            return $this->sendError('Privacy not found');
        }

        return $this->sendResponse(new PrivacyResource($privacy), 'Privacy retrieved successfully');
    }

    /**
     * Update the specified Privacy in storage.
     * PUT/PATCH /privacies/{id}
     *
     * @param int $id
     * @param UpdatePrivacyAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var Privacy $privacy */
        $privacy = $this->privacyRepository->find($id);

        if (empty($privacy)) {
            return $this->sendError('Privacy not found');
        }

        $privacy = $this->privacyRepository->update($input, $id);

        return $this->sendResponse(new PrivacyResource($privacy), 'Privacy updated successfully');
    }

    /**
     * Remove the specified Privacy from storage.
     * DELETE /privacies/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Privacy $privacy */
        $privacy = $this->privacyRepository->find($id);

        if (empty($privacy)) {
            return $this->sendError('Privacy not found');
        }

        $privacy->delete();

        return $this->sendSuccess('Privacy deleted successfully');
    }
}
