<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\BusinessAddressResource;
use App\Http\Resources\BusinessCategoryResource;
use App\Http\Resources\BusinessImageResource;
use App\Models\BusinessRegistration;
use App\Models\Configuration;
use App\Models\User;
use App\Traits\APIConnecter;
use Illuminate\Support\Facades\Storage;
use Response;

/**
 * Class NearByPartnersAPIController
 * @package App\Http\Controllers\API
 */

class NearByPartnersAPIController extends AppBaseController
{

    use APIConnecter;
    private $partnerBusinesses;
    /**
     * Return partner distance from user's current location.
     * GET|HEAD /nearByPartners
     *
     * @param Request $request
     * @return Response
     */

    public function __construct(BusinessRegistration $partnerBusinesses)
    {
        Configuration::getConfig();
        $this->partnerBusinesses = $partnerBusinesses;
    }


    public function nearByPartners(Request $request)
    {

        /**
         * distance From 5 km
         * ditstance To 10 km
         */

        $request->validate([
            'distanceFrom'  => 'integer',
            'ditstanceTo'  => 'integer',
        ]);


        $partners = [];
        $currentLocation = [];
        $isGeocodeFoundResult = false;

        $partnerBusinesses = $this->partnerBusinesses->join('businessAddresses', 'businessRegistrations.partnerId', '=', 'businessAddresses.partnerId')
            ->join('countries', 'businessAddresses.countryId', '=', 'countries.id')
            ->join('states', 'businessAddresses.stateId', '=', 'states.id')
            ->join('cities', 'businessAddresses.cityId', '=', 'cities.id')
            ->join('businessCategories', 'businessCategories.id', '=', 'businessRegistrations.businessCategoryId');



        if ($request->has('latitude') && $request->has('longitude')) {

            $geocodeResponse = $this->connectToGoogleMapGeocode($request->get('latitude'), $request->get('longitude'));

            if ($geocodeResponse['status'] != 'ZERO_RESULTS' && $geocodeResponse['status'] != 'INVALID_REQUEST') {

                $address_components = $geocodeResponse['results'][0]['address_components'];

                if (count((array)$address_components) > 0) {

                    $counts = count($address_components);
                    $country = $address_components[$counts - 1];
                    $state = $address_components[$counts - 2];
                    $city = $address_components[$counts - 3];

                    if (count($request->except(['latitude', 'longitude'])) == 0) {

                        if (isset($state['long_name'])) {
                            $partnerBusinesses->orwhere('states.stateName', $state['long_name']);
                        }
                        if (isset($city['long_name'])) {
                            $partnerBusinesses->orwhere('cities.cityName', $city['long_name']);
                        }
                        if (isset($country['long_name'])) {
                            $partnerBusinesses->orwhere('countries.countryName', $country['long_name']);
                        }
                    }

                    $currentLocation = ['latitude' => $request->get('latitude'), 'longitude' => $request->get('longitude')];

                    $isGeocodeFoundResult = true;
                }
            }
        }

        if (!$isGeocodeFoundResult) {

            $api_result = $this->connectToIPStack();

            if (count($request->except(['latitude', 'longitude'])) == 0) {

                if ($api_result['region_name']) {
                    $partnerBusinesses->orwhere('states.stateName', $api_result['region_name']);
                }
                if ($api_result['city']) {
                    $partnerBusinesses->orwhere('cities.cityName', $api_result['city']);
                }
                if ($api_result['country_name']) {
                    $partnerBusinesses->orwhere('countries.countryName', $api_result['country_name']);
                }
            }
            $currentLocation = ['latitude' => $api_result['latitude'], 'longitude' => $api_result['longitude']];
        }

        if($request->user()->tokenCan('gridBusiness')){
            $partnerBusinesses->where('businessRegistrations.partnerId', '!=', User::activeBusiness()->partnerId);
        }

        $partnerBusinesses =  $partnerBusinesses->orderBy('businessRegistrations.created_at', 'desc')
            ->groupBy('businessRegistrations.partnerId')
            ->get();


        $partnerNearByFrom = $request->has('distanceFrom') ? $request->get('distanceFrom') : 5;
        $partnerNearByTo = $request->has('distanceTo')  ? $request->get('distanceTo') : 10;

        $logType= $request->user()->tokenCan('gridBusiness')?'gridBusiness':'Admin';

        $partners = $this->partnerDetails($partnerBusinesses, $currentLocation, $partnerNearByFrom, $partnerNearByTo,$logType);


        $message = count($partners) > 0 ? 'NearBy Partners retrieved successfully' : 'No results found!';

        return $this->sendResponse($partners, $message);
    }



    private function partnerDetails($partnerBusinesses, $currentLocation, $partnerNearByFrom, $partnerNearByTo,$logType)
    {

        return collect($partnerBusinesses ?? [])->map(function ($partner) use ($currentLocation, $partnerNearByFrom, $partnerNearByTo,$logType) {


            $partnerBusinessAddress = User::activeBusiness() ? User::activeBusiness()->businessAddresses : [];

            $origins = $this->getGrideCodeDetails($partnerBusinessAddress,$logType);

            $destinations = $this->getGrideCodeDetails($partner->businessAddresses,$logType);

            $businessDistanceMatrix = $this->calculateDistanceMatrix($origins, $destinations);

            $currentDistanceMatrix = $this->calculateDistanceMatrix(['locations' => $currentLocation], $destinations, $partnerNearByTo, $partnerNearByFrom);

            if (!empty($currentDistanceMatrix)) {
                return [
                    'partnerId' => $partner->partnerId,
                    'businessName' => $partner->businessName,
                    'businessEmail' => $partner->businessEmail,
                    'description' => $partner->description,
                    'website' => $partner->website,
                    'logo' => Storage::disk('azure')->url('/businessLogo/' . $partner->logo),
                    'isHeadQuarter' => $partner->isHeadQuarter,
                    'headQuarterId' => $partner->headQuarterId,
                    'businessCategoryId' => $partner->businessCategoryId,
                    'partnerCategory' => new BusinessCategoryResource($partner->businessCategory),
                    'partnerAddresses' => BusinessAddressResource::collection($partner->businessAddresses),
                    'partnerImages' => BusinessImageResource::collection($partner->businessImages),
                    'currentDistanceMatrix' => $currentDistanceMatrix,
                    'businessDistanceMatrix' => $businessDistanceMatrix

                ];
            }
            return false;
        })->reject(function ($value) {
            return $value === false || $value === null;
        });
    }
}
