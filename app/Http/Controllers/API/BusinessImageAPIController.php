<?php

namespace App\Http\Controllers\API;

use App\Enums\ConfigKeyParameter;
use App\Http\Requests\API\CreateBusinessImageAPIRequest;
use App\Models\BusinessImage;
use App\Repositories\BusinessImageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\BusinessImageResource;
use App\Models\BusinessRegistration;
use App\Models\Configuration;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Response;

/**
 * Class BusinessImageController
 * @package App\Http\Controllers\API
 */

class BusinessImageAPIController extends AppBaseController
{
    /** @var  BusinessImageRepository */
    private $businessImageRepository;

    public function __construct(BusinessImageRepository $businessImageRepo)
    {
        Configuration::getConfig();

        $this->businessImageRepository = $businessImageRepo;
    }

    /**
     * Display a listing of the BusinessImage.
     * GET|HEAD /businessImages
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->has('partnerId')) {
            $request->merge(['partnerId' => $request->get('partnerId')]);
        } else {
            $request->merge(['partnerId' => User::activeBusiness()->partnerId]);
        }

        $businessImages = $this->businessImageRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );
        $message = count($businessImages) > 0 ? 'Partner Images retrieved successfully' : 'No results found!';
        return $this->sendResponse(BusinessImageResource::collection($businessImages), $message);
    }

    /**
     * Store a newly created BusinessImage in storage.
     * POST /businessImages
     *
     * @param CreateBusinessImageAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBusinessImageAPIRequest $request)
    {
        $businessRegistration = BusinessRegistration::find($request->get('partnerId'));
        $this->userHasAccess($businessRegistration->userId);

        $uploadedFiles =  $this->uploadImages($request->all());
        $businessImages = [];
        if (count($uploadedFiles) > 0) {
            foreach ($uploadedFiles as $input) {
                $businessImages[] = $this->businessImageRepository->create($input);
            }
        }

        return $this->sendResponse(BusinessImageResource::collection($businessImages), 'Partner Images saved successfully');
    }


    public function uploadImages($request)
    {


        $fileNameToStores = [];

        if (count($request['file']) > 0) {

            foreach ($request['file'] as $file) {

                $filenameWithExt = $file->getClientOriginalName();
                // Get Filename
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                // Get just Extension
                $extension = $file->getClientOriginalExtension();
                // Filename To store
                $fileNameToStore = strtolower(str_replace(' ', '_', $filename)) . '_' . time() . '.' . $extension;

                $file->storeAs('/businessImages/' . $request['partnerId'] . '/', $fileNameToStore);

                $fileNameToStores[] = ['imageUrl' => $fileNameToStore, 'partnerId' => $request['partnerId']];
            }
        }

        return $fileNameToStores;
    }

    /**
     * Display the specified BusinessImage.
     * GET|HEAD /businessImages/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BusinessImage $businessImage */
        $businessImage = $this->businessImageRepository->find($id);

        if (empty($businessImage)) {
            return $this->sendError('Partner Image not found');
        }
        $this->userHasAccess($businessImage->businessRegistration->userId);

        return $this->sendResponse(new BusinessImageResource($businessImage), 'Partner Image retrieved successfully');
    }



    /**
     * Remove the specified BusinessImage from storage.
     * DELETE /businessImages/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BusinessImage $businessImage */
        $businessImage = $this->businessImageRepository->find($id);

        if (empty($businessImage)) {
            return $this->sendError('Partner Image not found', 404);
        }

        $this->userHasAccess($businessImage->businessRegistration->userId);

        if (Storage::disk('azure')->exists('/businessImages/' .  $businessImage->partnerId . '/' . $businessImage->imageUrl)) {
            Storage::disk('azure')->delete('/businessImages/' .  $businessImage->partnerId . '/' . $businessImage->imageUrl);
        }

        $businessImage->delete();

        return $this->sendSuccess('Partner Image deleted successfully');
    }

    public function destroyAll(Request $request)
    {

        /** @var BusinessImage $businessImage */

        $request->merge(['partnerId' => $request->get('partnerId')]);

        $businessImages = $this->businessImageRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        if (count($businessImages) == 0) {
            return $this->sendError('Partner Images not found', 404);
        }

        foreach ($businessImages as $businessImage) {

            if (Storage::disk('azure')->exists('/businessImages/' .  $businessImage->partnerId . '/' . $businessImage->imageUrl)) {
                Storage::disk('azure')->delete('/businessImages/' .  $businessImage->partnerId . '/' . $businessImage->imageUrl);
            }

            $businessImage->delete();
        }

        return $this->sendResponse([], 'Partner Image deleted successfully');
    }
}
