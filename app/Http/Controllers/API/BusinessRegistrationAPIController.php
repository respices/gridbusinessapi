<?php

namespace App\Http\Controllers\API;

use App\Enums\BusinessRegistration as EnumsBusinessRegistration;
use App\Http\Requests\API\CreateBusinessRegistrationAPIRequest;
use App\Http\Requests\API\UpdateBusinessRegistrationAPIRequest;
use App\Models\BusinessRegistration;
use App\Repositories\BusinessRegistrationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\BusinessAddressResource;
use App\Http\Resources\BusinessRegistrationResource;
use App\Models\BusinessImage;
use App\Models\Configuration;
use App\Models\Privacy;
use App\Traits\APIConnecter;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Response;

/**
 * Class BusinessRegistrationController
 * @package App\Http\Controllers\API
 */

class BusinessRegistrationAPIController extends AppBaseController
{
    use APIConnecter;
    /** @var  BusinessRegistrationRepository */
    private $businessRegistrationRepository;

    public function __construct(BusinessRegistrationRepository $businessRegistrationRepo)
    {
        Configuration::getConfig();
        
        $this->businessRegistrationRepository = $businessRegistrationRepo;
    }

    /**
     * Display a listing of the BusinessRegistration.
     * GET|HEAD /businessRegistrations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        if (count($request->all()) > 0) {
            $businessRegistrations = $this->businessRegistrationRepository->businessQueries($request);
        } else {

            $request->merge(['userId' => $request->user()->id]);

            $businessRegistrations = $this->businessRegistrationRepository->all(
                $request->except(['skip', 'limit']),
                $request->get('skip'),
                $request->get('limit')
            );
        }


        return $this->sendResponse(BusinessRegistrationResource::collection($businessRegistrations), count($businessRegistrations) > 0 ? 'Business Partners retrieved successfully' : 'No results found!');
    }


    public function getBusinessLocations(Request $request,$partnerId)
    {
        $request->merge(['headQuarterId' => $partnerId]);

        $businessRegistrations = $this->businessRegistrationRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(BusinessRegistrationResource::collection($businessRegistrations), count($businessRegistrations) > 0 ? 'Business Partners retrieved successfully' : 'No results found!');
    }

    public function getBusinessLocationsCoordinates(Request $request,$partnerId)
    {
        $request->merge(['headQuarterId' => $partnerId]);
        $businessRegistrations=[];
        $businessRegistrations = $this->businessRegistrationRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );
        $businessRegistration=BusinessRegistration::find($partnerId);
       
        $collections = collect($businessRegistrations);
        $collections->push($businessRegistration);
        $collections->all();

        $response = $collections->map(function ($partner)  {
            $businessAddress=$partner->businessAddresses && count($partner->businessAddresses) > 0?$partner->businessAddresses[0]:null;
           if( $businessAddress ){

                $country =  $businessAddress->country;
                $countryCode = $country->countryCode;
                $gridCode= $businessAddress->grideCode;

                $gridCodeResult = $this->connectToFindGridcodeAPI(
                    isset($gridCode) ? $gridCode : null,
                    isset($countryCode) ? $countryCode : null,
                    $partner->partnerId,
                    url()->full(),
                    'gridBusiness'
                );
    
                if ($gridCodeResult['success']) {
                    return [
                        'partner' => new BusinessRegistrationResource($partner),
                        'businessAddress' => new BusinessAddressResource($businessAddress),
                        'gridCodeResult' => $gridCodeResult['response']
                    ];
                }
           }
           return false;
        })->reject(function ($value) {
            return $value === false || $value === null;
        });

       

        
        return $this->sendResponse($response, count($response) > 0 ? 'Business Partners retrieved successfully' : 'No results found!');
    }

    /**
     * Store a newly created BusinessRegistration in storage.
     * POST /businessRegistrations
     *
     * @param CreateBusinessRegistrationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBusinessRegistrationAPIRequest $request)
    {
        $input = $request->all();

        if ($request->get('isHeadQuarter') == 0 || $request->get('isHeadQuarter') == false) {
            $request->validate(['headQuarterId'  => 'required|string|exists:businessRegistrations,partnerId']);
        }
        if ($request->get('isHeadQuarter') == 1 || $request->get('isHeadQuarter') == true) {
              $request->validate(['logo' => 'required|file|mimes:jpeg,png,jpg|max:1048']);
        }

        

        $input['userId'] = $request->user()->id;
        $input['status'] = 'Active';  

        if ($request->get('isHeadQuarter') == 0 || $request->get('isHeadQuarter') == false) {
            $headQuarter=BusinessRegistration::find($request->get('headQuarterId'));
             $input['logo'] =  $headQuarter->logo;
        }else{
            $input['logo'] =  $this->uploadLogo($request);
           
        }

        $businessRegistration = $this->businessRegistrationRepository->create($input);

        if ($request->get('isHeadQuarter') == 1 || $request->get('isHeadQuarter') == true) {

                app(\App\Http\Controllers\API\GridNav\GeneratePublicKeyAPIController::class)
                ->savePublicKey([
                    'partnerId' => $businessRegistration->partnerId,
                    'partnerBusinessName' => $businessRegistration->businessName,
                    'userId'=>$businessRegistration->userId]);

                    Privacy::create([
                        'partnerId'=>$businessRegistration->partnerId,
                        'showEmailInDirectory'=>false,
                        'showPhoneNumberInDirectory'=>false,
                        'showBranches'=>false,
                        'showInDirectory'=>false,
                        'showContactPersonInDirectory'=>false,
                        'allowUnsusedCreditRollover'=>false,
                        'allowUnsusedSubscriptionRollover'=>false
                    ]);
            
        }

        return $this->sendResponse(new BusinessRegistrationResource($businessRegistration), 'Business Partner saved successfully');
    }



    public function uploadLogo($request)
    {


        if ($request->hasFile('logo')) {

            $filenameWithExt = $request->file('logo')->getClientOriginalName();
            // Get Filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just Extension
            $extension = $request->file('logo')->getClientOriginalExtension();
            // Filename To store
            $fileNameToStore = strtolower(str_replace(' ', '_', $filename)) . '_' . time() . '.' . $extension;

            $request->file('logo')->storeAs('/businessLogo/', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        return $fileNameToStore;
    }

    /**
     * Display the specified BusinessRegistration.
     * GET|HEAD /businessRegistrations/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(Request $request, $partnerId)
    {
        /** @var BusinessRegistration $businessRegistration */

        $businessRegistration = $this->businessRegistrationRepository->find($partnerId);

        if (empty($businessRegistration)) {
            return $this->sendError('Business Partner not found');
        }

        $this->userHasAccess($businessRegistration->userId);

        return $this->sendResponse(new BusinessRegistrationResource($businessRegistration), 'Business Partner retrieved successfully');
    }

    /**
     * Update the specified BusinessRegistration in storage.
     * PUT/PATCH /businessRegistrations/{id}
     *
     * @param int $id
     * @param UpdateBusinessRegistrationAPIRequest $request
     *
     * @return Response
     */
    public function update($partnerId, UpdateBusinessRegistrationAPIRequest $request)
    {
        $input = $request->except(['logo']);

        /** @var BusinessRegistration $businessRegistration */

        $businessRegistration = $this->businessRegistrationRepository->find($partnerId);

        if (empty($businessRegistration)) {
            return $this->sendError('Business Registration not found');
        }

        $this->userHasAccess($businessRegistration->userId);


        if ($request->get('isHeadQuarter') == 0 || $request->get('isHeadQuarter') == false) {
            $request->validate(['headQuarterId' => 'required|string|exists:businessRegistrations,partnerId']);
        }
        $businessRegistration = $this->businessRegistrationRepository->update($input, $partnerId);

        return $this->sendResponse(new BusinessRegistrationResource($businessRegistration), 'BusinessRegistration updated successfully');
    }

    //changePartnerBusinessStatus


    public function changePartnerBusinessStatus($partnerId, Request $request)
    {
        $input = $request->all();

        /** @var BusinessRegistration $businessRegistration */

        $businessRegistration = $this->businessRegistrationRepository->find($partnerId);

        if (empty($businessRegistration)) {
            return $this->sendError('Business Registration not found');
        }

        $this->userHasAccess($businessRegistration->userId);

        $request->validate(['status' => ['required',Rule::in(EnumsBusinessRegistration::$status)]]);

        $businessRegistration = $this->businessRegistrationRepository->update($input, $partnerId);

        return $this->sendResponse(new BusinessRegistrationResource($businessRegistration), 'Status updated successfully');
    }


    public function changePartnerBusinessLogo(Request $request)
    {
        $input = $request->all();

        $request->validate(['logo' => 'required|file|mimes:jpeg,png,jpg|max:1048']);

        $businessRegistration = $this->businessRegistrationRepository->find($request->get('partnerId'));

        if (empty($businessRegistration)) {
            return $this->sendError('Business Registration not found');
        }



        $this->userHasAccess($businessRegistration->userId);


        if (Storage::disk('azure')->exists('/businessLogo/' . $businessRegistration->logo)) {
            Storage::disk('azure')->delete('/businessLogo/' . $businessRegistration->logo);
        }

        $input['logo'] =  $this->uploadLogo($request);


        $businessRegistration = $this->businessRegistrationRepository->update($input, $request->get('partnerId'));

        return $this->sendResponse(Storage::disk('azure')->url('/businessLogo/' . $businessRegistration->logo), 'Logo updated successfully');
    }

    /**
     * Remove the specified BusinessRegistration from storage.
     * DELETE /businessRegistrations/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($partnerId)
    {
        /** @var BusinessRegistration $businessRegistration */

        $businessRegistration = $this->businessRegistrationRepository->find($partnerId);

        if (empty($businessRegistration)) {
            return $this->sendError('Business Registration not found');
        }

        $this->userHasAccess($businessRegistration->userId);
        
        if($businessRegistration->isHeadQuarter){
            if (Storage::disk('azure')->exists('/businessLogo/' . $businessRegistration->logo)) {
                Storage::disk('azure')->delete('/businessLogo/' . $businessRegistration->logo);
            }
        }
        

        foreach (BusinessImage::where('partnerId', $partnerId)->get() as $businessImage) {
            if (Storage::disk('azure')->exists('/businessImages/' .  $businessImage->partnerId . '/' . $businessImage->imageUrl)) {
                Storage::disk('azure')->delete('/businessImages/' .  $businessImage->partnerId . '/' . $businessImage->imageUrl);
            }
        }
       
        if($businessRegistration->isHeadQuarter){
           $this->deleteBranches($partnerId);
        }

        $businessRegistration->delete();


        return $this->sendSuccess('Business Partner deleted successfully');
    }

    function deleteBranches($partnerId){

        foreach(BusinessRegistration::where('headQuarterId',$partnerId)->get() as $branch ){
            if (Storage::disk('azure')->exists('/businessLogo/' . $branch->logo)) {
                Storage::disk('azure')->delete('/businessLogo/' . $branch->logo);
            }
    
            foreach (BusinessImage::where('partnerId', $partnerId)->get() as $businessImage) {
                if (Storage::disk('azure')->exists('/businessImages/' .  $businessImage->partnerId . '/' . $businessImage->imageUrl)) {
                    Storage::disk('azure')->delete('/businessImages/' .  $businessImage->partnerId . '/' . $businessImage->imageUrl);
                }
            }
        }
       
    }
}
