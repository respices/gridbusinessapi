<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\AccessLog;
use Illuminate\Support\Facades\DB;
use Response;

/**
 * Class GridCodeStatisticsAPIController 
 * @package App\Http\Controllers\API
 */

class GridCodeStatisticsAPIController  extends AppBaseController
{


    /**
     *  Get API statistics that has been made by a Partner from GridNav API
     * 
     * GET|HEAD /gridCodeStatistics
     *
     * @param Request $request
     * @return Response
     */


    public function gridCodeStatistics(Request $request)
    {

        /**
         * Total number of API calls
         * API calls per Gridcode
         * API Calls per Endpoint
         * Last API Call Datetime
         * Top GridCode Search
         * Top EndPoint Searched
         */
        $statistics = [
            'totalNumberApiCalls' => AccessLog::count(),
            'gridCodeStatistics' => $this->apiCallsPerGridCode(),
            'endPointStatistics' => $this->apiCallsPerEndPoint(),
            'lastApiCall' => $this->lastApiCallsDateTime(),
            'topGridCodeSearched' => $this->topGridCodeSearched(),
            'topEndPointSearched' => $this->topEndPointSearched()
        ];


        return $this->sendResponse($statistics, 'Statistics retrieved successfully');
    }

    private function apiCallsPerGridCode()
    {
        return  AccessLog::select('searchedGridCode', DB::raw('count(searchedGridCode) as totalCalls,searchedGridCode'))->whereNotNull('searchedGridCode')->whereNotNull('partnerId')->groupBy('searchedGridCode')->get();
    }

    private function apiCallsPerEndPoint()
    {
        return  AccessLog::select('sourceUrl', DB::raw('count(sourceUrl) as totalCalls,sourceUrl'))->whereNotNull('sourceUrl')->whereNotNull('partnerId')->groupBy('sourceUrl')->get();
    }

    private function lastApiCallsDateTime()
    {
        return  AccessLog::orderBy('conEndDateTime', 'desc')->whereNotNull('partnerId')->first();
    }


    private function topGridCodeSearched()
    {
        return   array_reduce($this->apiCallsPerGridCode()->toArray(), function($a, $b){
           
            return $a ? ($a['totalCalls'] > $b['totalCalls'] ? $a : $b) : $b;
          });
       
    }

    private function topEndPointSearched()
    {
        return   array_reduce($this->apiCallsPerEndPoint()->toArray(), function($a, $b){
           
            return $a ? ($a['totalCalls'] > $b['totalCalls'] ? $a : $b) : $b;
          });
    }
    //
}
