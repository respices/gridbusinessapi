<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAccessLogAPIRequest;
use App\Http\Requests\API\UpdateAccessLogAPIRequest;
use App\Models\AccessLog;
use App\Repositories\AccessLogRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\AccessLogResource;
use App\Traits\APIConnecter;
use Response;

/**
 * Class AccessLogController
 * @package App\Http\Controllers\API
 */

class AccessLogAPIController extends AppBaseController
{
    use APIConnecter;

    /** @var  AccessLogRepository */
    private $accessLogRepository;

    public function __construct(AccessLogRepository $accessLogRepo)
    {
        $this->accessLogRepository = $accessLogRepo;
    }

    /**
     * Display a listing of the AccessLog.
     * GET|HEAD /accessLogs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $accessLogs = $this->accessLogRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(AccessLogResource::collection($accessLogs), count($accessLogs) > 0?'Access Logs retrieved successfully':'No results found!');
    }

    /**
     * Store a newly created AccessLog in storage.
     * POST /accessLogs
     *
     * @param CreateAccessLogAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAccessLogAPIRequest $request)
    {
        $input = $request->all();

        $api_result = $this->connectToIPStack();

        $input['sourceCountry'] = $api_result && $api_result['country_name'] ? $api_result['country_name'] : '--';
        $input['ipAddress'] =  $api_result['ip'];

        $accessLog = $this->accessLogRepository->create($input);

        return $this->sendResponse(new AccessLogResource($accessLog), 'Access Log saved successfully');
    }


    public function storeOrUpdate($input)
    {

        $api_result = $this->connectToIPStack();

       

        $input['sourceCountry'] = $api_result && $api_result['country_name'] ? $api_result['country_name'] : '--';
        $input['ipAddress'] =  $api_result['ip'];

        if (isset($input['id'])) {
            $accessLog = $this->accessLogRepository->update($input->toArray(), $input['id']);
        } else {
            $accessLog = $this->accessLogRepository->create($input);
        }

        return $accessLog;
    }


    /**
     * Display the specified AccessLog.
     * GET|HEAD /accessLogs/{id}
     *
     * @param int $id
     *
     * @return Response
     */

    public function show($id)
    {
        /** @var AccessLog $accessLog */
        $accessLog = $this->accessLogRepository->find($id);

        if (empty($accessLog)) {
            return $this->sendError('Access Log not found');
        }

        return $this->sendResponse(new AccessLogResource($accessLog), 'Access Log retrieved successfully' );
    }

    /**
     * Update the specified AccessLog in storage.
     * PUT/PATCH /accessLogs/{id}
     *
     * @param int $id
     * @param UpdateAccessLogAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAccessLogAPIRequest $request)
    {
        $input = $request->all();

        /** @var AccessLog $accessLog */
        $accessLog = $this->accessLogRepository->find($id);

        if (empty($accessLog)) {
            return $this->sendError('Access Log not found');
        }

        $api_result = $this->connectToIPStack();

         $input['sourceCountry'] = $api_result && $api_result['country_name'] ? $api_result['country_name'] : '--';
        $input['ipAddress'] =  $api_result['ip'];

        $accessLog = $this->accessLogRepository->update($input, $id);

        return $this->sendResponse(new AccessLogResource($accessLog), 'AccessLog updated successfully');
    }

    /**
     * Remove the specified AccessLog from storage.
     * DELETE /accessLogs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var AccessLog $accessLog */
        $accessLog = $this->accessLogRepository->find($id);

        if (empty($accessLog)) {
            return $this->sendError('Access Log not found');
        }

        $accessLog->delete();

        return $this->sendSuccess('Access Log deleted successfully');
    }
}
