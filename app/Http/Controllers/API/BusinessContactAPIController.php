<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBusinessContactAPIRequest;
use App\Http\Requests\API\UpdateBusinessContactAPIRequest;
use App\Models\BusinessContact;
use App\Repositories\BusinessContactRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\BusinessContactResource;
use App\Http\Resources\BusinessRegistrationResource;
use App\Models\BusinessRegistration;
use App\Models\Configuration;
use App\Models\User;
use Response;

/**
 * Class BusinessContactController
 * @package App\Http\Controllers\API
 */

class BusinessContactAPIController extends AppBaseController
{
    /** @var  BusinessContactRepository */
    private $businessContactRepository;

    public function __construct(BusinessContactRepository $businessContactRepo)
    {
        Configuration::getConfig();
        $this->businessContactRepository = $businessContactRepo;
    }

    /**
     * Display a listing of the BusinessContact.
     * GET|HEAD /businessContacts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $request->merge(['partnerId' => User::activeBusiness()->partnerId,'hasBusinessPartner'=>true]);

        $businessContacts = $this->businessContactRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        $message = count($businessContacts) > 0?'Partner Contacts retrieved successfully':'No results found!';

        return $this->sendResponse(BusinessContactResource::collection($businessContacts), $message);
    }

    public function getBusinessContactProfile(Request $request)
    {
        $request->merge(['hasBusinessPartner'=>true]);
      
        $businessContacts =  BusinessRegistration::join('businessContacts', 'businessContacts.businessContactId', '=', 'businessRegistrations.partnerId')
        ->where('businessContacts.partnerId', User::activeBusiness()->partnerId)
        ->orderBy('businessRegistrations.created_at', 'desc')
    
                ->groupBy('businessRegistrations.partnerId')
        ->get();
        $message = count($businessContacts) > 0?'Partner Contacts retrieved successfully':'No results found!';
        return $this->sendResponse(BusinessRegistrationResource::collection($businessContacts), $message);
    }


    public function getPartnerRequests(Request $request)
    {
        $request->merge(['hasBusinessPartner'=>true]);
      
        $businessContacts =  BusinessRegistration::join('businessContacts', 'businessContacts.businessContactId', '=', 'businessRegistrations.partnerId')
        ->where('businessContacts.businessContactId', User::activeBusiness()->partnerId)
        ->orderBy('businessRegistrations.created_at', 'desc')
    
                ->groupBy('businessRegistrations.partnerId')
        ->get();
        $message = count($businessContacts) > 0?'Partner Contacts retrieved successfully':'No results found!';
        return $this->sendResponse(BusinessRegistrationResource::collection($businessContacts), $message);
    }

    /**
     * Store a newly created BusinessContact in storage.
     * POST /businessContacts
     *
     * @param CreateBusinessContactAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBusinessContactAPIRequest $request)
    {
        $input = $request->all();
        $businessRegistration = BusinessRegistration::find($request->get('partnerId'));
        $this->userHasAccess($businessRegistration->userId);

        $businessContact = $this->businessContactRepository->create($input);

        return $this->sendResponse(new BusinessContactResource($businessContact), 'Partner Contact saved successfully');
    }

    /**
     * Display the specified BusinessContact.
     * GET|HEAD /businessContacts/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BusinessContact $businessContact */
        $businessContact = $this->businessContactRepository->find($id);

        if (empty($businessContact)) {
            return $this->sendError('Partner Contact not found');
        }

        $this->userHasAccess($businessContact->businessRegistration->userId);

        return $this->sendResponse(new BusinessContactResource($businessContact), 'Partner Contact retrieved successfully');
    }

    /**
     * Update the specified BusinessContact in storage.
     * PUT/PATCH /businessContacts/{id}
     *
     * @param int $id
     * @param UpdateBusinessContactAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBusinessContactAPIRequest $request)
    {
        $input = $request->all();

        /** @var BusinessContact $businessContact */
        $businessContact = $this->businessContactRepository->find($id);

        if (empty($businessContact)) {
            return $this->sendError('Partner Contact not found');
        }

        $this->userHasAccess($businessContact->businessRegistration->userId);

        $businessContact = $this->businessContactRepository->update($input, $id);

        return $this->sendResponse(new BusinessContactResource($businessContact), 'Partner Contact updated successfully');
    }

    /**
     * Remove the specified BusinessContact from storage.
     * DELETE /businessContacts/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BusinessContact $businessContact */
        $businessContact = $this->businessContactRepository->find($id);

        if (empty($businessContact)) {
            return $this->sendError('Partner Contact not found');
        }
        $this->userHasAccess($businessContact->businessRegistration->userId);

        $businessContact->delete();

        return $this->sendSuccess('Partner Contact deleted successfully');
    }


     //acceptBusinessContactRequest

     public function acceptBusinessContactRequest($id)
     {
         /** @var BusinessAddress $businessAddress */
         $businessContact = $this->businessContactRepository->find($id);
 
         if (empty($businessContact)) {
            return $this->sendError('Contact Request not found');
        }

        if (empty(BusinessContact::where('id',$id)->where('businessContactId',User::activeBusiness()->partnerId)->first())) {
            return $this->sendError("Please make sure request belongs to you, otherwise you're not allowed to continue!",406);
        }
        
 
         $this->userHasAccess($businessContact->businessContact->userId);

 
          $this->businessContactRepository->update(['status'=>'Accepted','action_date'=>date('Y-m-d H:i:s')], $id);
        
         return $this->sendSuccess('Request accepted successfully',201);
     }

     public function rejectBusinessContactRequest($id)
     {
         /** @var BusinessAddress $businessAddress */
         $businessContact = $this->businessContactRepository->find($id);
 
         if (empty($businessContact)) {
            return $this->sendError('Contact Request not found');
        }

        if (empty(BusinessContact::where('id',$id)->where('businessContactId',User::activeBusiness()->partnerId)->first())) {
            return $this->sendError("Please make sure request belongs to you, otherwise you're not allowed to continue!",406);
        }
        
 
         $this->userHasAccess($businessContact->businessContact->userId);

 
          $this->businessContactRepository->update(['status'=>'Rejected','action_date'=>date('Y-m-d H:i:s')], $id);
        
         return $this->sendSuccess('Request rejected!',201);
     }
}


