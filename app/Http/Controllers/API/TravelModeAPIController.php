<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTravelModeAPIRequest;
use App\Http\Requests\API\UpdateTravelModeAPIRequest;
use App\Models\TravelMode;
use App\Repositories\TravelModeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\TravelModeResource;
use Response;

/**
 * Class TravelModeController
 * @package App\Http\Controllers\API
 */

class TravelModeAPIController extends AppBaseController
{
    /** @var  TravelModeRepository */
    private $travelModeRepository;

    public function __construct(TravelModeRepository $travelModeRepo)
    {
        $this->middleware(['auth:admin','scope:admin'])->except(['index','show']);
        $this->travelModeRepository = $travelModeRepo;
    }

    /**
     * Display a listing of the TravelMode.
     * GET|HEAD /travelModes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $travelModes = $this->travelModeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );
        $message = count($travelModes) > 0?'TTravel Modes retrieved successfully':'No results found!';
        return $this->sendResponse(TravelModeResource::collection($travelModes), $message);
    }

    /**
     * Store a newly created TravelMode in storage.
     * POST /travelModes
     *
     * @param CreateTravelModeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTravelModeAPIRequest $request)
    {
        $input = $request->all();

        $travelMode = $this->travelModeRepository->create($input);

        return $this->sendResponse(new TravelModeResource($travelMode), 'Travel Mode saved successfully');
    }

    /**
     * Display the specified TravelMode.
     * GET|HEAD /travelModes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TravelMode $travelMode */
        $travelMode = $this->travelModeRepository->find($id);

        if (empty($travelMode)) {
            return $this->sendError('Travel Mode not found');
        }

        return $this->sendResponse(new TravelModeResource($travelMode), 'Travel Mode retrieved successfully');
    }

    /**
     * Update the specified TravelMode in storage.
     * PUT/PATCH /travelModes/{id}
     *
     * @param int $id
     * @param UpdateTravelModeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTravelModeAPIRequest $request)
    {
        $input = $request->all();

        /** @var TravelMode $travelMode */
        $travelMode = $this->travelModeRepository->find($id);

        if (empty($travelMode)) {
            return $this->sendError('Travel Mode not found');
        }

        $travelMode = $this->travelModeRepository->update($input, $id);

        return $this->sendResponse(new TravelModeResource($travelMode), 'TravelMode updated successfully');
    }

    /**
     * Remove the specified TravelMode from storage.
     * DELETE /travelModes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TravelMode $travelMode */
        $travelMode = $this->travelModeRepository->find($id);

        if (empty($travelMode)) {
            return $this->sendError('Travel Mode not found');
        }

        $travelMode->delete();

        return $this->sendSuccess('Travel Mode deleted successfully');
    }
}
