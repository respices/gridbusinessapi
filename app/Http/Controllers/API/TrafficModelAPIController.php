<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTrafficModelAPIRequest;
use App\Http\Requests\API\UpdateTrafficModelAPIRequest;
use App\Models\TrafficModel;
use App\Repositories\TrafficModelRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\TrafficModelResource;
use Response;

/**
 * Class TrafficModelController
 * @package App\Http\Controllers\API
 */

class TrafficModelAPIController extends AppBaseController
{
    /** @var  TrafficModelRepository */
    private $trafficModelRepository;

    public function __construct(TrafficModelRepository $trafficModelRepo)
    {
        $this->middleware(['auth:admin','scope:admin'])->except(['index','show']);
        $this->trafficModelRepository = $trafficModelRepo;
    }

    /**
     * Display a listing of the TrafficModel.
     * GET|HEAD /trafficModels
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $trafficModels = $this->trafficModelRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );
        $message = count($trafficModels) > 0?'Traffic Models retrieved successfully':'No results found!';
        return $this->sendResponse(TrafficModelResource::collection($trafficModels), $message);
    }

    /**
     * Store a newly created TrafficModel in storage.
     * POST /trafficModels
     *
     * @param CreateTrafficModelAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTrafficModelAPIRequest $request)
    {
        $this->middleware('auth:admin');
        $input = $request->all();

        $trafficModel = $this->trafficModelRepository->create($input);

        return $this->sendResponse(new TrafficModelResource($trafficModel), 'Traffic Model saved successfully');
    }

    /**
     * Display the specified TrafficModel.
     * GET|HEAD /trafficModels/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TrafficModel $trafficModel */
        $trafficModel = $this->trafficModelRepository->find($id);

        if (empty($trafficModel)) {
            return $this->sendError('Traffic Model not found');
        }

        return $this->sendResponse(new TrafficModelResource($trafficModel), 'Traffic Model retrieved successfully');
    }

    /**
     * Update the specified TrafficModel in storage.
     * PUT/PATCH /trafficModels/{id}
     *
     * @param int $id
     * @param UpdateTrafficModelAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTrafficModelAPIRequest $request)
    {
        $input = $request->all();

        /** @var TrafficModel $trafficModel */
        $trafficModel = $this->trafficModelRepository->find($id);

        if (empty($trafficModel)) {
            return $this->sendError('Traffic Model not found');
        }

        $trafficModel = $this->trafficModelRepository->update($input, $id);

        return $this->sendResponse(new TrafficModelResource($trafficModel), 'TrafficModel updated successfully');
    }

    /**
     * Remove the specified TrafficModel from storage.
     * DELETE /trafficModels/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->middleware('auth:admin');
        /** @var TrafficModel $trafficModel */
        $trafficModel = $this->trafficModelRepository->find($id);

        if (empty($trafficModel)) {
            return $this->sendError('Traffic Model not found');
        }

        $trafficModel->delete();

        return $this->sendSuccess('Traffic Model deleted successfully');
    }
}
