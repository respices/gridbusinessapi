<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePartnerCreditAPIRequest;
use App\Http\Requests\API\UpdatePartnerCreditAPIRequest;
use App\Models\PartnerCredit;
use App\Repositories\PartnerCreditRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\PartnerCreditResource;
use Response;

/**
 * Class PartnerCreditController
 * @package App\Http\Controllers\API
 */

class PartnerCreditAPIController extends AppBaseController
{
    /** @var  PartnerCreditRepository */
    private $partnerCreditRepository;

    public function __construct(PartnerCreditRepository $partnerCreditRepo)
    {
        $this->partnerCreditRepository = $partnerCreditRepo;
    }

    /**
     * Display a listing of the PartnerCredit.
     * GET|HEAD /partnerCredits
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $partnerCredits = $this->partnerCreditRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(PartnerCreditResource::collection($partnerCredits), 'Partner Credits retrieved successfully');
    }

    /**
     * Store a newly created PartnerCredit in storage.
     * POST /partnerCredits
     *
     * @param CreatePartnerCreditAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePartnerCreditAPIRequest $request)
    {
        $input = $request->all();

        $partnerCredit = $this->partnerCreditRepository->create($input);

        return $this->sendResponse(new PartnerCreditResource($partnerCredit), 'Partner Credit saved successfully');
    }

    /**
     * Display the specified PartnerCredit.
     * GET|HEAD /partnerCredits/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PartnerCredit $partnerCredit */
        $partnerCredit = $this->partnerCreditRepository->find($id);

        if (empty($partnerCredit)) {
            return $this->sendError('Partner Credit not found');
        }

        return $this->sendResponse(new PartnerCreditResource($partnerCredit), 'Partner Credit retrieved successfully');
    }

    /**
     * Update the specified PartnerCredit in storage.
     * PUT/PATCH /partnerCredits/{id}
     *
     * @param int $id
     * @param UpdatePartnerCreditAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartnerCreditAPIRequest $request)
    {
        $input = $request->all();

        /** @var PartnerCredit $partnerCredit */
        $partnerCredit = $this->partnerCreditRepository->find($id);

        if (empty($partnerCredit)) {
            return $this->sendError('Partner Credit not found');
        }

        $partnerCredit = $this->partnerCreditRepository->update($input, $id);

        return $this->sendResponse(new PartnerCreditResource($partnerCredit), 'PartnerCredit updated successfully');
    }

    /**
     * Remove the specified PartnerCredit from storage.
     * DELETE /partnerCredits/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PartnerCredit $partnerCredit */
        $partnerCredit = $this->partnerCreditRepository->find($id);

        if (empty($partnerCredit)) {
            return $this->sendError('Partner Credit not found');
        }

        $partnerCredit->delete();

        return $this->sendSuccess('Partner Credit deleted successfully');
    }
}
