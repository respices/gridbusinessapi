<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePartnerConsumerCreditAPIRequest;
use App\Http\Requests\API\UpdatePartnerConsumerCreditAPIRequest;
use App\Models\PartnerConsumerCredit;
use App\Repositories\PartnerConsumerCreditRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\PartnerConsumerCreditResource;
use Response;

/**
 * Class PartnerConsumerCreditController
 * @package App\Http\Controllers\API
 */

class PartnerConsumerCreditAPIController extends AppBaseController
{
    /** @var  PartnerConsumerCreditRepository */
    private $partnerConsumerCreditRepository;

    public function __construct(PartnerConsumerCreditRepository $partnerConsumerCreditRepo)
    {
        $this->partnerConsumerCreditRepository = $partnerConsumerCreditRepo;
    }

    /**
     * Display a listing of the PartnerConsumerCredit.
     * GET|HEAD /partnerConsumerCredits
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $partnerConsumerCredits = $this->partnerConsumerCreditRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(PartnerConsumerCreditResource::collection($partnerConsumerCredits), 'Partner Consumer Credits retrieved successfully');
    }

    /**
     * Store a newly created PartnerConsumerCredit in storage.
     * POST /partnerConsumerCredits
     *
     * @param CreatePartnerConsumerCreditAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePartnerConsumerCreditAPIRequest $request)
    {
        $input = $request->all();

        $partnerConsumerCredit = $this->partnerConsumerCreditRepository->create($input);

        return $this->sendResponse(new PartnerConsumerCreditResource($partnerConsumerCredit), 'Partner Consumer Credit saved successfully');
    }

    /**
     * Display the specified PartnerConsumerCredit.
     * GET|HEAD /partnerConsumerCredits/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PartnerConsumerCredit $partnerConsumerCredit */
        $partnerConsumerCredit = $this->partnerConsumerCreditRepository->find($id);

        if (empty($partnerConsumerCredit)) {
            return $this->sendError('Partner Consumer Credit not found');
        }

        return $this->sendResponse(new PartnerConsumerCreditResource($partnerConsumerCredit), 'Partner Consumer Credit retrieved successfully');
    }

    /**
     * Update the specified PartnerConsumerCredit in storage.
     * PUT/PATCH /partnerConsumerCredits/{id}
     *
     * @param int $id
     * @param UpdatePartnerConsumerCreditAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartnerConsumerCreditAPIRequest $request)
    {
        $input = $request->all();

        /** @var PartnerConsumerCredit $partnerConsumerCredit */
        $partnerConsumerCredit = $this->partnerConsumerCreditRepository->find($id);

        if (empty($partnerConsumerCredit)) {
            return $this->sendError('Partner Consumer Credit not found');
        }

        $partnerConsumerCredit = $this->partnerConsumerCreditRepository->update($input, $id);

        return $this->sendResponse(new PartnerConsumerCreditResource($partnerConsumerCredit), 'PartnerConsumerCredit updated successfully');
    }

    /**
     * Remove the specified PartnerConsumerCredit from storage.
     * DELETE /partnerConsumerCredits/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PartnerConsumerCredit $partnerConsumerCredit */
        $partnerConsumerCredit = $this->partnerConsumerCreditRepository->find($id);

        if (empty($partnerConsumerCredit)) {
            return $this->sendError('Partner Consumer Credit not found');
        }

        $partnerConsumerCredit->delete();

        return $this->sendSuccess('Partner Consumer Credit deleted successfully');
    }
}
