<?php

namespace App\Http\Controllers\API\Admin\Auth;

use App\Http\Controllers\AppBaseController;
use App\Http\Resources\Admin\AdminUserResource;
use App\Http\Resources\Admin\UserResource;
use App\Models\Configuration;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UserController extends AppBaseController
{

    public function __construct()
    {
        Configuration::getConfig();
    }

    //allUsers
    public function allUsers(Request $request)
    {

        $users = User::get();

        return $this->sendResponse(UserResource::collection($users), 'Users', 200);
    }
    //totalUsers

    public function totalUsers(Request $request)
    {
        return $this->sendResponse(['totalUsers' => User::count()], 'Total Users', 200);
    }

    //userLastLogin
    public function userLastLogin(Request $request)
    {
        return $this->sendResponse(new UserResource(User::orderBy('lastLoginDate','desc')->first()), 'User last login date', 200);
    }
    
    protected function me(Request $request)
    {

        $user = $request->user();

        return $this->sendResponse(new AdminUserResource($user), 'Admin details', 200);
    }


    public function logout(Request $request)
    {
        $user = $request->user();
        if ($user) {
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                    'revoked' => true
                ]);

            $accessToken->revoke();
        }
        return $this->sendResponse('You are signed out!', 204);
    }


    public function addProfileImage(Request $request)
    {

        $request->validate(['profileImage' => 'required|file|mimes:jpeg,png,jpg|max:1048']);

        $user = $request->user();

        if ($request->hasFile('profileImage')) {

         
            if ( Storage::disk('azure')->exists('/profileImages/admin/' .   $user->avatar) ) {
                Storage::disk('azure')->delete('/profileImages/admin/' .   $user->avatar);
            }

            $filenameWithExt = $request->file('profileImage')->getClientOriginalName();
            // Get Filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just Extension
            $extension = $request->file('profileImage')->getClientOriginalExtension();
            // Filename To store
            $fileNameToStore = strtolower(str_replace(' ', '_', $filename)) . '_' . time() . '.' . $extension;

            $request->file('profileImage')->storeAs('/profileImages/admin/', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

      
        $user->avatar = $fileNameToStore;
        $user->save();

        return $this->sendResponse(Storage::disk('azure')->url('/profileImages/admin/' .  $user->avatar), 'Profile image changed successfully', 200);
    }
}
