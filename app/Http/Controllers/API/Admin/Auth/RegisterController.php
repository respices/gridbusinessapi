<?php

namespace App\Http\Controllers\API\Admin\Auth;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\RegistrationAdminRequest;

use App\Models\Admin;
use Illuminate\Support\Facades\Auth;

class RegisterController extends AppBaseController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */



    /**
     * Register User
     *
     * @param RegistrationAdminRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */

    protected function register(RegistrationAdminRequest $request)
    {

        $user = Admin::create($request->getAttributes());
        try {
            Auth::loginUsingId($user->id);

            return $this->sendResponse('Admin Registered Successful', 200);
        } catch (\Exception $e) {
            return $this->sendError($e, 500);
        }
    }
}
