<?php

namespace App\Http\Controllers\API\Admin\Auth;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\LoginRequest;
use App\Models\Admin;

class LoginController extends AppBaseController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */





    protected function login(LoginRequest $request)
    {

        if (!$admin = Admin::where('email', $request->get('email'))->first()) return $this->sendError('Email does not exist', 400);


        if (password_verify($request->get('password'), $admin->password)) {

            $accessToken =  $admin->createToken(date('Y-m-d g:i:s'),['admin'])->accessToken;

            return $this->sendResponse(['accessToken' => $accessToken], 'You are Logged in!', 200);
        } else {
            return $this->sendError('These credentials do not match our records.', 401);
        }
    }
}
