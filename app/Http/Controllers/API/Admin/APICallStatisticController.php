<?php

namespace App\Http\Controllers\API\Admin;

use App\Repositories\AccessLogRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\AccessLogResource;
use App\Models\AccessLog;
use Carbon\CarbonInterval;
use Illuminate\Support\Facades\DB;
use Response;

/**
 * Class AccessLogController
 * @package App\Http\Controllers\API
 */

class APICallStatisticController extends AppBaseController
{
    /** @var  AccessLogRepository */
    private $accessLogRepository;

    public function __construct(AccessLogRepository $accessLogRepo)
    {
        $this->accessLogRepository = $accessLogRepo;
    }

    /**
     * Display a listing of the AccessLog.
     * GET|HEAD /accessLogs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $accessLogs = $this->accessLogRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );
        $message = count($accessLogs) > 0?'Access Logs retrieved successfully':'No results found!';
        return $this->sendResponse(AccessLogResource::collection($accessLogs), $message);
    }

    //apiCallsStatistics

    public function apiCallsStatistics(Request $request)
    {


        $statistics = [
            'totalApiCalls' => AccessLog::count(),
            'apiCallsbyPartners' => $this->apiCallsbyPartners(),
            'apiCallsByGridCode' => $this->apiCallsByGridCode(),
            'averageAPICallResponseTime' => $this->averageAPICallResponseTime()
        ];


        return $this->sendResponse($statistics, 'Statistics retrieved successfully');
    }

    private function apiCallsbyPartners()
    {
        return  AccessLog::select('partnerId', DB::raw('count(partnerId) as apiCallsbyPartners'))->whereNotNull('partnerId')->groupBy('partnerId')->get();
    }

    private function apiCallsByGridCode()
    {
        return  AccessLog::select('searchedGridCode', DB::raw('count(searchedGridCode) as apiCallsByGridCode'))->whereNotNull('searchedGridCode')->groupBy('searchedGridCode')->get();
    }

    private function averageAPICallResponseTime()
    {
        $getAverageResponseTime = AccessLog::select(DB::raw("AVG(TIME_TO_SEC(TIMEDIFF(conStartDateTime, conEndDateTime))) AS timediff"))
            ->whereNotNull('conStartDateTime')
            ->whereNotNull('conEndDateTime')
            ->get();

        return CarbonInterval::seconds((int)$getAverageResponseTime[0]->timediff)
            ->cascade()
            ->forHumans();
    }
}
