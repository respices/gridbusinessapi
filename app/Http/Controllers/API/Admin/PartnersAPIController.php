<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\AppBaseController;
use App\Http\Resources\BusinessRegistrationResource;
use App\Http\Resources\PartnerSubscriptionResource;
use App\Models\BusinessRegistration;
use App\Models\Configuration;
use App\Models\PartnerSubscription;
use App\Repositories\BusinessRegistrationRepository;
use Illuminate\Http\Request;

class PartnersAPIController extends AppBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /** @var  BusinessRegistrationRepository */
    private $businessRegistrationRepository;

    public function __construct(BusinessRegistrationRepository $businessRegistrationRepo)
    {
        Configuration::getConfig();
        $this->businessRegistrationRepository = $businessRegistrationRepo;
    }

    public function index(Request $request)
    {

        if (count($request->all()) > 0) {
            $businessRegistrations = $this->businessRegistrationRepository->businessQueries($request);
        } else {

            $businessRegistrations = $this->businessRegistrationRepository->all(
                $request->except(['skip', 'limit']),
                $request->get('skip'),
                $request->get('limit')
            );
        }

        $message = count($businessRegistrations) > 0 ? 'Partners retrieved successfully' : 'No results found!';
        return $this->sendResponse(BusinessRegistrationResource::collection($businessRegistrations), $message);
    }


    public function partnerCounts()
    {
        $counts = BusinessRegistration::count();
        return $this->sendResponse(['partnerCounts' => $counts], $counts > 0 ? 'Partner counts retrieved successfully' : 'No results found!');
    }

    public function partnerSubscriptions()
    {
        $businessRegistrations = PartnerSubscription::join('businessRegistrations', 'businessRegistrations.partnerId', '=', 'partnerSubscriptions.partnerId');
        $businessRegistrations = $businessRegistrations->get();
        $message = count($businessRegistrations) > 0 ? 'Partner subscriptions retrieved successfully' : 'No results found!';
        return $this->sendResponse(PartnerSubscriptionResource::collection($businessRegistrations), $message);
    }
}
