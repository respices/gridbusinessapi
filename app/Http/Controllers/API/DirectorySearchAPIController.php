<?php

namespace App\Http\Controllers\API;

use App\Common\Distance;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\BusinessAddressResource;
use App\Http\Resources\BusinessCategoryResource;
use App\Http\Resources\BusinessImageResource;
use App\Models\BusinessAddress;
use App\Models\BusinessContact;
use App\Models\BusinessRegistration;
use App\Models\Configuration;
use App\Models\GridNavCode;
use App\Models\User;
use App\Traits\APIConnecter;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Response;

/**
 * Class DirectorySearchAPIController
 * @package App\Http\Controllers\API
 */

class DirectorySearchAPIController extends AppBaseController
{

    use APIConnecter;
    private $partnerBusinesses;
    /**
     * Return partner distance from user's current location.
     *  We will call GridNav API to get lat long when the directorysearch api is executed.
     * GET|HEAD /directorySearch
     *
     * @param Request $request
     * @return Response
     */

    public function __construct(BusinessRegistration $partnerBusinesses)
    {
        Configuration::getConfig();
        $this->partnerBusinesses = $partnerBusinesses;
    }


    public function directorySearch(Request $request)
    {

        /**
         * Search by Country
         * Search by City
         * Search by GridCode
         * Search by Category
         * Search by PlaceName
         * Search by partnername
         * Search by Distance (Km) from current location
         */

        $request->validate([
            'businessCategoryId' => 'integer|exists:businessCategories,id',
            'grideCode' => 'string|max:9',
            'cityId' => 'integer|exists:cities,id',
            'stateId' => 'integer|exists:states,id',
            'countryId' => 'integer|exists:countries,id',
            'partnerId' => 'string|exists:businessRegistrations,partnerId',
        ]);


        $partners = [];
        $currentLocation = [];
        $isGeocodeFoundResult = false;

        $partnerBusinesses = $this->partnerBusinesses->join('businessAddresses', 'businessRegistrations.partnerId', '=', 'businessAddresses.partnerId')
            ->join('countries', 'businessAddresses.countryId', '=', 'countries.id')
            ->join('states', 'businessAddresses.stateId', '=', 'states.id')
            ->join('cities', 'businessAddresses.cityId', '=', 'cities.id')
            ->join('businessCategories', 'businessCategories.id', '=', 'businessRegistrations.businessCategoryId');

        if ($request->has('latitude') && $request->has('longitude')) {

            $geocodeResponse = $this->connectToGoogleMapGeocode($request->get('latitude'), $request->get('longitude'));

            if ($geocodeResponse['status'] != 'ZERO_RESULTS' && $geocodeResponse['status'] != 'INVALID_REQUEST') {

                $address_components = $geocodeResponse['results'][0]['address_components'];

                if (count((array)$address_components) > 0) {

                    $counts = count($address_components);
                    $country = $address_components[$counts - 1];
                    $state = $address_components[$counts - 2];
                    $city = $address_components[$counts - 3];

                    if (count($request->except(['latitude', 'longitude'])) == 0) {

                            if (isset($state['long_name'])) {
                                $partnerBusinesses->orwhere('states.stateName', $state['long_name']);
                            }
                            if (isset($city['long_name'])) {
                                $partnerBusinesses->orwhere('cities.cityName', $city['long_name']);
                            }
                            if (isset($country['long_name'])) {
                                $partnerBusinesses->orwhere('countries.countryName', $country['long_name']);
                            }
                    }

                    $currentLocation = ['latitude' => $request->get('latitude'), 'longitude' => $request->get('longitude')];

                    $isGeocodeFoundResult = true;
                }
            }
        }

        if (!$isGeocodeFoundResult) {

            $api_result = $this->connectToIPStack();

                if (count($request->except(['latitude', 'longitude'])) == 0) {

                            if ($api_result['region_name']) {
                                $partnerBusinesses->orwhere('states.stateName', $api_result['region_name']);
                            }
                            if ($api_result['city']) {
                                $partnerBusinesses->orwhere('cities.cityName', $api_result['city']);
                            }
                            if ($api_result['country_name']) {
                                $partnerBusinesses->orwhere('countries.countryName', $api_result['country_name']);
                            }
                }
            $currentLocation = ['latitude' => $api_result['latitude'], 'longitude' => $api_result['longitude']];
        }



        if (count($request->except(['latitude', 'longitude'])) > 0) {


            if ($request->has('partnerId')) {
                $partnerBusinesses->where('businessRegistrations.partnerId', $request->get('partnerId'));
            }

            if ($request->has('countryId')) {
                $partnerBusinesses->where('businessAddresses.countryId', $request->get('countryId'));
            }

            if ($request->has('stateId')) {
                $partnerBusinesses->where('businessAddresses.stateId', $request->get('stateId'));
            }

            if ($request->has('cityId')) {
                $partnerBusinesses->where('businessAddresses.cityId', $request->get('cityId'));
            }
            if ($request->has('grideCode')) {
                $partnerBusinesses->where('businessAddresses.grideCode', $request->get('grideCode'));
            }

            if ($request->has('businessCategoryId')) {
                $partnerBusinesses->where('businessRegistrations.businessCategoryId', $request->get('businessCategoryId'));
            }

            if ($request->has('businessName')) {
                $partnerBusinesses->where('businessRegistrations.businessName', $request->get('businessName'));
            }

            if ($request->has('countryCode')) {
                $partnerBusinesses->where('countries.countryCode', $request->get('countryCode'));
            }

            if ($request->has('countryName')) {
                $partnerBusinesses->where('countries.countryName', $request->get('countryName'));
            }

            if ($request->has('stateName')) {
                $partnerBusinesses->where('states.stateName', $request->get('stateName'));
            }

            if ($request->has('cityName')) {
                $partnerBusinesses->where('cities.cityName', $request->get('cityName'));
            }

            if ($request->has('address')) {
                $partnerBusinesses->where('businessAddresses.address', $request->get('address'));
            }
        }
        
        if($request->user()->tokenCan('gridBusiness')){
            $partnerBusinesses->where('businessRegistrations.partnerId', '!=', User::activeBusiness()->partnerId);
        }
       

        $partnerBusinesses =  $partnerBusinesses->orderBy('businessRegistrations.created_at', 'desc')
            ->groupBy('businessRegistrations.partnerId')
            ->get();

        $logType= $request->user()->tokenCan('gridBusiness')?'gridBusiness':'Admin';
        $partners = $this->partnerDetails($partnerBusinesses, $currentLocation,$logType);


        $message = count($partners) > 0 ? 'Partners retrieved successfully' : 'No results found!';

        return $this->sendResponse($partners, $message);
    }




    private function partnerDetails($partnerBusinesses, $currentLocation,$logType)
    {

        return collect($partnerBusinesses ?? [])->map(function ($partner) use ($currentLocation,$logType) {

            $partnerBusinessAddress = User::activeBusiness() ? User::activeBusiness()->businessAddresses : [];

            $origins = $this->getGrideCodeDetails($partnerBusinessAddress,$logType);

            $destinations = $this->getGrideCodeDetails($partner->businessAddresses,$logType);

            $businessDistanceMatrix = $this->calculateDistanceMatrix($origins, $destinations);
            $currentDistanceMatrix = $this->calculateDistanceMatrix(['locations' => $currentLocation], $destinations);


            return [
                'partnerId' => $partner->partnerId,
                'businessName' => $partner->businessName,
                'businessEmail' => $partner->businessEmail,
                'description' => $partner->description,
                'website' => $partner->website,
                'logo' => Storage::disk('azure')->url('/businessLogo/' . $partner->logo),
                'isHeadQuarter' => $partner->isHeadQuarter,
                'headQuarterId' => $partner->headQuarterId,
                'businessCategoryId' => $partner->businessCategoryId,
                'partnerCategory' => new BusinessCategoryResource($partner->businessCategory),
                'partnerAddresses' => BusinessAddressResource::collection($partner->businessAddresses),
                'partnerImages' => BusinessImageResource::collection($partner->businessImages),
                'currentDistanceMatrix' => $currentDistanceMatrix,
                'businessDistanceMatrix' => $businessDistanceMatrix

            ];
        })->reject(function ($value) {
            return $value === false || $value === null;
        });
    }

  
}
