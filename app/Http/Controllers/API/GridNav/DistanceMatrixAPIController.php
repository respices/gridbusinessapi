<?php

namespace App\Http\Controllers\API\GridNav;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Traits\APIConnecter;
use App\Models\TrafficModel;
use App\Models\TravelMode;

/**
 * Class DistanceMatrixAPIController
 * @package App\Http\Controllers\API\GridNav
 */

class DistanceMatrixAPIController extends AppBaseController
{
    use APIConnecter;

    /**
     * @OA\Get(
     * path="/api/gridNav/distanceMatrix",
     * summary="Endpoint for Grid Nav Distance Matrix",
     * description="Calculate Distance between a given source and Destination Gridcode (sourceGridCode, sourceCountryCode, destinationGridCode, destinationCountryCode,trafficModelId,travelModeId,departureTime)",
     * operationId="distanceMatrix",
     * tags={"GridNav APIs"},
     * security={{"token": {}}},
     * @OA\Response(
     *    response=200,
     *    description="Ok",
     *    @OA\JsonContent(
     *       @OA\Property(property="success", type="string", format="boolean", example="true"),
     *       @OA\Property(property="data", type="object"),
     *       @OA\Property(property="message", type="string", example="Distance Matrix retrieved successfully")
     *        )
     *  ),
     *  @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *   @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *      response=500,
     *      description="Internal server error"
     *   ),
     * 
     *     @OA\Parameter(
     *        name="sourceGridCode",
     *         in="query",
     *         required=true,
     *        @OA\Schema(
     *            type="string",
     *            default="aaaa-acwg",
     *         )
     *     ),    
     *     @OA\Parameter(
     *         name="sourceCountryCode",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             default="rw",
     *         ),
     *      ), 
     * @OA\Parameter(
     *         name="destinationGridCode",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             default="aaaa-aavr",
     *         ),
     *      ), 
     * @OA\Parameter(
     *         name="destinationCountryCode",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             default="rw",
     *         ),
     *      ), 
     * @OA\Parameter(
     *         name="trafficModelId",
     *         in="query",
     *         @OA\Schema(
     *             type="integer",
     *             default="1002",
     *         ),
     *      ), 
     *  @OA\Parameter(
     *         name="travelModeId",
     *         in="query",
     *         @OA\Schema(
     *             type="integer",
     *             default="1002",
     *         ),
     *      ), 
     *    * @OA\Parameter(
     *         name="departureTime",
     *         in="query",
     *         @OA\Schema(
     *             type="string",
     *             default="",
     *         ),
     *      ), 
     * )   
     */

    public function distanceMatrix(Request $request)
    {
        $request->validate([
            'sourceGridCode' => 'required|string',
            'sourceCountryCode' => 'required|string',
            'destinationGridCode' => 'required|string',
            'destinationCountryCode' => 'required|string',
            'trafficModelId' => 'integer|exists:traffic_models,id',
            'travelModeId' => 'integer|exists:travel_modes,id',
            'departureTime' => 'nullable|date_format:h:i A',
        ]);

        try {

            $logType=$request->has('logType')?$request->get('logType'):'gridBusiness';
            $source = $this->connectToFindGridcodeAPI($request->get('sourceGridCode'), $request->get('sourceCountryCode'), $request->get('partnerId'), url()->full(),$logType);

            if ($source['success']) {

                $origin_address = $source['response']['latitude'] . ',' . $source['response']['longitude'];
            }

            $destination = $this->connectToFindGridcodeAPI($request->get('destinationGridCode'), $request->get('destinationCountryCode'), $request->get('partnerId'), url()->full(),$logType);

            if ($destination['success']) {
                $destination_address = $destination['response']['latitude'] . ',' . $destination['response']['longitude'];
            }

            if (!$origin_address) {
                return $this->sendResponse('Source Grid Code is invalid', 404);
            }
            if (!$destination_address) {
                return $this->sendResponse('Destination Grid Code is invalid', 404);
            }

            $travelmode = TravelMode::viewMode($request->has('travelModeId') ? $request->get('travelModeId') : null);
            $traffic_model = TrafficModel::viewModel($request->has('trafficModelId') ? $request->get('trafficModelId') : null);
            $departure_time = $request->get('departureTime') != NULL ? strtotime($request->get('departureTime')) : 'now';

            $distance_data = $this->connectToGoogleMapDistanceMatrix($origin_address, $destination_address, $travelmode, $traffic_model, $departure_time);

            if ($distance_data['status'] == 'OK') {
                
                $sourceData=['title'=>$source['response']['title'],
                            'grid_code'=>$source['response']['grid_code'],
                            'country_code'=>$source['response']['country_code'],
                            'latitude'=>$source['response']['latitude'],
                            'longitude'=>$source['response']['longitude']
                             ];

                      $destinationData=['title'=>$destination['response']['title'],
                            'grid_code'=>$destination['response']['grid_code'],
                            'country_code'=>$destination['response']['country_code'],
                            'latitude'=>$destination['response']['latitude'],
                            'longitude'=>$destination['response']['longitude']
                            ];

                return $this->sendResponse(
                    $this->transformGoogleMapDistanceMatrix($distance_data, $travelmode, $traffic_model,$sourceData,$destinationData),
                    'Distance Matrix retrieved successfully'
                );
            } else {

                return $this->sendError($distance_data['status'] . ': ' . $distance_data['error_message'], 400);
            }
        } catch (\Exception $e) {

            return $this->sendError($e->getMessage(), 500);
        }
    }
}
