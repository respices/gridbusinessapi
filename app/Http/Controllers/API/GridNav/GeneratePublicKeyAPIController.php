<?php

namespace App\Http\Controllers\API\GridNav;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\GeneratePartnerPublicKeyRequest;
use App\Models\Configuration;
use App\Models\GridNavAccessToken;
use App\Repositories\ConfigurationRepository;
use App\Traits\Encryption;

/**
 * Class BusinessAddressController
 * @package App\Http\Controllers\API
 */

class GeneratePublicKeyAPIController extends AppBaseController
{
    use Encryption;
    public function __construct(ConfigurationRepository $configurationRepo)
    {
        $this->configurationRepository = $configurationRepo;
    }


    /**
     * @OA\Post(
     * path="/api/gridNav/generatePublickey",
     * summary="Endpoint for Generate GridNav Public key",
     * description="Generate GridNav Public key by partnerId, userId, partnerBusinessName",
     * operationId="gridNavGeneratePublickey",
     * tags={"GridNav APIs"},
     * @OA\Response(
     *    response=422,
     *    description="Unprocessable Entity - requested data contain invalid values",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="The given data was invalid."),
     *       @OA\Property(property="errors", type="object")
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Ok",
     *    @OA\JsonContent(
     *       @OA\Property(property="success", type="string", format="boolean", example="true"),
     *      @OA\Property(property="data", type="string"),
     *       @OA\Property(property="message", type="string", example="Public Key generated successfully")
     *        )
     *  ),
     * @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="partnerId",
     *                     type="string",
     *                     default="57386a1f-cc82-4398-aa00-d1d175b3f1f0"
     *                 ),
     *                @OA\Property(
     *                     property="userId",
     *                     type="integer",
     *                     default="1"
     *                 ),
     *               @OA\Property(
     *                     property="partnerBusinessName",
     *                     type="string",
     *                     default="Plectrum"
     *                 ),
     *             )
     *         )
     *     ),
     * )   
     */

    public function index($partnerId)
    {
        $gridNavAccessToken = GridNavAccessToken::where('partnerId',$partnerId)->where('revoked',1)->first();
     
        return $this->sendResponse($gridNavAccessToken?base64_encode($gridNavAccessToken->tokenId.'|'.rand(3000,5000).'|'.rand(1000,3000)):null, 'Public Key generated successfully');
    }
    public function store(GeneratePartnerPublicKeyRequest $request)
    {


      $signature = $this->savePublicKey([
          'partnerId'=>$request->get('partnerId'),
          'userId'=>$request->get('userId'),
          'partnerBusinessName'=>$request->get('partnerBusinessName'),

        ]);
        
        return $this->sendResponse($signature, 'Public Key generated successfully');
    }

    public function savePublicKey($request){
        $hMacKey = $this->getMachineId();

        Configuration::updateOrCreate([
            'value' => $hMacKey,
            'parameter' => 'hMacKey',
        ], [
            'parameter' => 'hMacKey',
            'value' => $hMacKey,
            'status' => 'Active'
        ]);

        $time = time();

        GridNavAccessToken::where('partnerId',$request['partnerId'])->where('revoked',1)->update(['revoked'=>0]);
        
        $gridNavAccessToken = GridNavAccessToken::create(['partnerId' => $request['partnerId'],'revoked'=>1]);

        $payload = array(
            'partnerId' => $request['partnerId'],
            'userId' => $request['userId'],
            'tokenId'=>$gridNavAccessToken->tokenId,
            'partnerBusinessName' => $request['partnerBusinessName'],
            "currentTimeStamp" => $time,
            "expireTime" => $time+86400,
            "enableExpireTime"=>true
        );
        $signature  =   $this->encrypt(urldecode(http_build_query($payload)),$hMacKey);

        $gridNavAccessToken->token= base64_encode($hMacKey.'|'.$signature);

        $gridNavAccessToken->save();

        return $gridNavAccessToken->token;
    }
}
