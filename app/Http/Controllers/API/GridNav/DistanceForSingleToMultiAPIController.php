<?php

namespace App\Http\Controllers\API\GridNav;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Traits\APIConnecter;
use App\Models\TrafficModel;
use App\Models\TravelMode;
/**
 * Class DistanceForSingleToMultiAPIController
 * @package App\Http\Controllers\API\GridNav
 */

class DistanceForSingleToMultiAPIController extends AppBaseController
{
    use APIConnecter;

/**
     * @OA\Get(
     * path="/api/gridNav/distanceSingleToMulti",
     * summary="Endpoint for Grid Nav Distance Calculate Single to Multi",
     * description="Calculate Distance between a given source and Destination Gridcode (sourceGridCode, sourceCountryCode, destinationGridCode(n), destinationCountryCode(n),trafficModelId,travelModeId,departureTime)",
     * operationId="distanceSingleToMulti",
     * tags={"GridNav APIs"},
     * security={{"token": {}}},
     * @OA\Response(
     *    response=200,
     *    description="Ok",
     *    @OA\JsonContent(
     *       @OA\Property(property="success", type="string", format="boolean", example="true"),
     *       @OA\Property(property="data", type="object"),
     *       @OA\Property(property="message", type="string", example="Distance Calc Single to Multi retrieved successfully")
     *        )
     *  ),
     *  @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *   @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="Not Found"
     *   ),
     *   @OA\Response(
     *      response=500,
     *      description="Internal server error"
     *   ),
     * 
     *     @OA\Parameter(
     *        name="sourceGridCode",
     *         in="query",
     *         required=true,
     *        @OA\Schema(
     *            type="string",
     *            default="aaaa-acwg",
     *         )
     *     ),    
     *     @OA\Parameter(
     *         name="sourceCountryCode",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             default="rw",
     *         ),
     *      ), 
     * 
     *  @OA\Parameter(
     *         name="destinationGridCodes[0]",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             default="aaaa-aavr",
     *         ),
     *      ), 
     * @OA\Parameter(
     *         name="destinationCountryCodes[0]",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             default="rw",
     *         ),
     *      ), 
     *  @OA\Parameter(
     *         name="destinationGridCodes[1]",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             default="aaaa-aati",
     *         ),
     *      ), 
     * @OA\Parameter(
     *         name="destinationCountryCodes[1]",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             default="rw",
     *         ),
     *      ), 
     * @OA\Parameter(
     *         name="trafficModelId",
     *         in="query",
     *         @OA\Schema(
     *             type="integer",
     *             default="1002",
     *         ),
     *      ), 
     *  @OA\Parameter(
     *         name="travelModeId",
     *         in="query",
     *         @OA\Schema(
     *             type="integer",
     *             default="1002",
     *         ),
     *      ), 
     *    * @OA\Parameter(
     *         name="departureTime",
     *         in="query",
     *         @OA\Schema(
     *             type="string",
     *             default="",
     *         ),
     *      ), 
     * )   
     */

    public function distanceSingleToMulti(Request $request)
    {
        $request->validate([
            'sourceGridCode' => ['required'],
            'sourceCountryCode' => ['required'],
            'destinationGridCodes' => 'required|array',
            'destinationCountryCodes' => 'required|array',
            'trafficModelId' => 'integer|exists:traffic_models,id',
            'travelModeId' => 'integer|exists:travel_modes,id',
            'departure_time' => 'nullable|date_format:h:i A',
        ]);

        $destination_addresses=[];
        $destinations=[];
        try {
            $logType=$request->has('logType')?$request->get('logType'):'gridBusiness';
            $source = $this->connectToFindGridcodeAPI($request->get('sourceGridCode'), $request->get('sourceCountryCode'), $request->get('partnerId'), url()->full(),$logType);

            if ($source['success']) {

                $origin_address = $source['response']['latitude'] . ',' . $source['response']['longitude'];
            }

            if (count($request->get('destinationGridCodes')) > 0  && count($request->get('destinationCountryCodes'))) {
                $i = 0;
                $destination =[];
                while ($i < count($request->get('destinationCountryCodes'))) {

                    $destination[$i] = $this->connectToFindGridcodeAPI(
                        isset($request->get('destinationGridCodes')[$i]) ? $request->get('destinationGridCodes')[$i] : null,
                        isset($request->get('destinationCountryCodes')[$i]) ? $request->get('destinationCountryCodes')[$i] : null,
                        $request->get('partnerId'),
                        url()->full(),
                        $logType
                    );

                    $destinationData[$i]=['title'=>$destination[$i]['response']['title'],
                        'grid_code'=>$destination[$i]['response']['grid_code'],
                        'country_code'=>$destination[$i]['response']['country_code'],
                        'latitude'=>$destination[$i]['response']['latitude'],
                        'longitude'=>$destination[$i]['response']['longitude']
                        ];

                    if ($destination[$i]['success']) {
                        $destinations[]=  $destinationData[$i];
                        $destination_addresses[]          = $destination[$i]['response']['latitude'] . ',' . $destination[$i]['response']['longitude'];
                    }

                    $i++;
                }

            }


            if (!$origin_address) {
                return $this->sendResponse('Source Grid Code is invalid', 404);
            }
            if (count($destination_addresses) == 0) {
                return $this->sendResponse('Destination Grid Code is invalid', 404);
            }

            $destinationsImploded =  implode("|", $destination_addresses);

            $travelmode=TravelMode::viewMode($request->has('travelModeId')?$request->get('travelModeId'):null);
            $traffic_model = TrafficModel::viewModel($request->has('trafficModelId')?$request->get('trafficModelId'):null);
            $departure_time=$request->get('departure_time')!=NULL?strtotime($request->get('departure_time')):'now';
    
            $distance_data = $this->connectToGoogleMapDistanceMatrix($origin_address, $destinationsImploded,$travelmode,$traffic_model,$departure_time);

            if ($distance_data['status'] == 'OK') {

                $sourceData=['title'=>$source['response']['title'],
                'grid_code'=>$source['response']['grid_code'],
                'country_code'=>$source['response']['country_code'],
                'latitude'=>$source['response']['latitude'],
                'longitude'=>$source['response']['longitude']
                 ];

         

                return $this->sendResponse(
                    $this->transformGoogleMapSingleToMultDistanceMatrix($distance_data,$travelmode,$traffic_model,$sourceData,$destinations),
                    'Distance Calc Single to Multi retrieved successfully'
                );
            } else {

                return $this->sendError($distance_data['status'].': '.$distance_data['error_message'], 400);
            }
        } catch (\Exception $e) {

            return $this->sendError($e->getMessage(), 500);
        }
    }
}
