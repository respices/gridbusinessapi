<?php

namespace App\Http\Controllers\API\GridNav;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Traits\APIConnecter;

/**
 * Class BusinessAddressController
 * @package App\Http\Controllers\API
 */

class PartnerGridSearchAPIController extends AppBaseController
{
    use APIConnecter;


    /**
     * @OA\Get(
     * path="/api/gridNav/partnerGridSearch",
     * summary="Endpoint for Partner Grid Search (by GridCode and Countrycode)",
     * description="Find Gridcode details by GridCode and Countrycode",
     * operationId="partnerGridSearch",
     * tags={"GridNav APIs"},
     * security={{"token": {}}},
     * @OA\Response(
     *    response=200,
     *    description="Ok",
     *    @OA\JsonContent(
     *       @OA\Property(property="success", type="string", format="boolean", example="true"),
     *       @OA\Property(property="data", type="object"),
     *       @OA\Property(property="message", type="string", example="Grid Code retrieved successfully")
     *        )
     *  ),
     *  @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *   @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="Grid Code Not Found"
     *   ),
     *   @OA\Response(
     *      response=500,
     *      description="Internal server error"
     *   ),
     * 
     *     @OA\Parameter(
     *        name="gridCode",
     *         in="query",
     *         required=true,
     *        @OA\Schema(
     *            type="string",
     *            default="aaaa-ad18",
     *         )
     *     ),    
     *     @OA\Parameter(
     *         name="countryCode",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             default="ng",
     *         ),
     *      ), 
     * )   
     */
    public function partnerGridSearch(Request $request)
    {
        $request->validate([
            'gridCode' => ['required'],
            'countryCode' => ['required'],
        ]);

        $logType=$request->has('logType')?$request->get('logType'):'gridBusiness';
        $response = $this->connectToFindGridcodeAPI($request->get('gridCode'), $request->get('countryCode'), $request->get('partnerId'), url()->full(),$logType);

        if ($response['success']) {

            return $this->sendResponse($response['response'], 'Grid Code retrieved successfully');
        } else {
            return $this->sendError($response['response'], 404);
        }
    }
}
