<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\RegistrationRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
class RegisterController extends AppBaseController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */



    /**
     * Register User
     *
     * @param RegistrationRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */

    protected function register(RegistrationRequest $request)
    {

        $user = User::create($request->getAttributes());
        try {

            $this->sendVerificationEmail($user);
            
            $accessToken =  $user->createToken(date('Y-m-d H:i:s'), ['gridBusiness'])->accessToken;
            $user->lastLoginDate = date('Y-m-d H:i:s');
            $user->save();

            return $this->sendResponse(['user'=>new UserResource($user),'accessToken' => $accessToken], 'Please confirm yourself by clicking on verify user button sent to you on your email', 200);

        } catch (\Exception $e) {
            return $this->sendError($e, 500);
        }
    }
}
