<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\LoginRequest;
use App\Http\Resources\UserResource;
use App\Models\Configuration;
use App\Models\User;

class LoginController extends AppBaseController
{


    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * 
     * @OA\Post(
     * path="/login",
     * summary="Sign in",
     * description="Login by email, password",
     * operationId="authLogin",
     * tags={"Grid Business Portal APIs"},
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"email","password"},
     *       @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
     *       @OA\Property(property="password", type="string", format="password", example="PassWord@12345"),
     *    ),
     * ),
     * @OA\Response(
     *    response=422,
     *    description="Wrong email response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, Email does not exist. Please try again or create an account")
     *    ),
     * ),
     * @OA\Response(
     *    response=401,
     *    description="Unauthenticated",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
     *    ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Ok",
     *    @OA\JsonContent(
     *       @OA\Property(property="success", type="string", format="boolean", example="false"),
     *      @OA\Property(property="data", type="object", format="object"),
     *       @OA\Property(property="message", type="string", example="You are Logged in!")
     *        )
     *     )
     * )
     */
    public function __construct()
    {
        Configuration::getConfig();
    }

    protected function login(LoginRequest $request)
    {


        if (!$user = User::where('email', $request->get('email'))->first()) return $this->sendError("Sorry, Couldn't find your account!", 422);


        if (password_verify($request->get('password'), $user->password)) {

            $accessToken =  $user->createToken(date('Y-m-d H:i:s'), ['gridBusiness'])->accessToken;
            $user->lastLoginDate = date('Y-m-d H:i:s');
            $user->save();

            return $this->sendResponse(['user'=>new UserResource($user),'accessToken' => $accessToken], 'You are Logged in!', 200);
        } else {
            return $this->sendError('Sorry, wrong email address or password', 401);
        }
    }
}
