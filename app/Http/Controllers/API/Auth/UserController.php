<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\AppBaseController;
use App\Http\Resources\UserResource;
use App\Models\BusinessImage;
use App\Models\BusinessRegistration;
use App\Models\Configuration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;

class UserController extends AppBaseController
{

    public function __construct()
    {
        Configuration::getConfig();
    }

    protected function me(Request $request)
    {


        $user = $request->user();

        return $this->sendResponse(new UserResource($user), 'User details', 200);
    }


    public function logout(Request $request)
    {
        $user = $request->user();
        if ($user) {
            $accessToken = $user->token();
            DB::table('oauth_refresh_tokens')
                ->where('access_token_id', $accessToken->id)
                ->update([
                    'revoked' => true
                ]);

            $accessToken->revoke();
            $user->lastLogoutDate = date('Y-m-d g:i:s');
            $user->save();
        }
        return $this->sendResponse('You are signed out!', 204);
    }


    public function updateProfile(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            return $this->sendError('Profile not found', 404);
        }
        
        $this->validate($request, [
            'firstName' => ['required', 'string', 'max:255'],
            'lastName' => ['required', 'string', 'max:255'],
            'email' => [
                'required', 'string', 'email:rfc,dns', 'max:255',
                Rule::unique('users')->ignore($user->id)
            ]
        ]);

        $user->update($request->all());
        try {

            return $this->sendResponse(new UserResource($user), 'Profile updated successfully', 200);
        } catch (\Exception $e) {
            return $this->sendError($e, 500);
        }
    }

    public function deleteAccount(Request $request)
    {
        $user = $request->user();
        if (!$user) {
            return $this->sendError('Profile not found', 404);
        }

        foreach (BusinessRegistration::where('userId', $user->id)->get() as $businessRegistration) {
            
            if($businessRegistration->isHeadQuarter){
                    if (Storage::disk('azure')->exists('/businessLogo/' . $businessRegistration->logo)) {
                        Storage::disk('azure')->delete('/businessLogo/' . $businessRegistration->logo);
                    }
             }

            foreach (BusinessImage::where('partnerId', $businessRegistration->partnerId)->get() as $businessImage) {
                if (Storage::disk('azure')->exists('/businessImages/' .  $businessImage->partnerId . '/' . $businessImage->imageUrl)) {
                    Storage::disk('azure')->delete('/businessImages/' .  $businessImage->partnerId . '/' . $businessImage->imageUrl);
                }
            }


            $businessRegistration->delete();
        }
        $accessToken = $user->token();
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();
        $user->delete();

        try {
            return $this->sendResponse([], 'Account  deleted successfully', 200);
        } catch (\Exception $e) {
            return $this->sendError($e, 500);
        }
    }
    //changePassword

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'current_password' => 'required',
            'password' => [
                'required',
                'confirmed',
                Password::min(8)
                    ->mixedCase()
                    ->letters()
                    ->numbers()
                    ->symbols()
                    ->uncompromised()
            ],
        ]);

        $user = $request->user();
        if (!$user) {
            return $this->sendError('Profile not found', 404);
        }
        if (!password_verify($request->get('current_password'), $user->password)) {
            return $this->sendError('Incorrect current password!', 422);
        }

        $input = $request->all();
        $input['password'] = Hash::make($input['password']); // 
        $user->update($input);
        try {

            return $this->sendResponse(new UserResource($user), 'Password changed successfully', 200);
        } catch (\Exception $e) {
            return $this->sendError($e, 500);
        }
    }

    public function addProfileImage(Request $request)
    {

        $request->validate(['profileImage' => 'required|file|mimes:jpeg,png,jpg|max:1048']);

        $user = $request->user();

        if ($request->hasFile('profileImage')) {


            if (Storage::disk('azure')->exists('/profileImages/users/' .   $user->avatar)) {
                Storage::disk('azure')->delete('/profileImages/users/' .   $user->avatar);
            }

            $filenameWithExt = $request->file('profileImage')->getClientOriginalName();
            // Get Filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just Extension
            $extension = $request->file('profileImage')->getClientOriginalExtension();
            // Filename To store
            $fileNameToStore = strtolower(str_replace(' ', '_', $filename)) . '_' . time() . '.' . $extension;

            $request->file('profileImage')->storeAs('/profileImages/users/', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }


        $user->avatar = $fileNameToStore;
        $user->save();

        return $this->sendResponse(Storage::disk('azure')->url('/profileImages/users/' .  $user->avatar), 'Profile image changed successfully', 200);
    }
}
