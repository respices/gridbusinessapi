<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\AppBaseController;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Flash;

class VerificationController extends AppBaseController
{


    /**
     * Verify email
     *
     * @param $user_id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function verify(Request $request)
    {
        $user = $request->user();

        $code = $request->get('code');

        if ($user->emailVerifyToken != $code) {
            return $this->sendError('Invalid verification code', 422);
        }

            $date = date('Y-m-d g:i:s');
            $user->email_verified_at = $date; // to enable the “email_verified_at field of that user be a current time stamp by mimicing the must verify email feature
            $user->save();


            return $this->sendResponse(new UserResource($user), 'Email Verified successfully', 200);
        
    }


    public function verifyFromWeb(Request $request)
    {

        $token = $request->get('token');
        $user = null;
        if ($token) {

            $raw = base64_decode($token);

            $userID = $raw;
            $user = User::find($userID);


            if (!$user) {
                Flash::error('User not Found');
            }

            if (!$user->email_verified_at) {
                $date = date('Y-m-d g:i:s');
                $user->email_verified_at = $date;
                $user->save();
            }
            if ($user->email_verified_at) {
                Auth::loginUsingId($user->id);
                Flash::success('Email Verified successfully');
            }
        } else {
            Flash::error('Token not Found');
        }
        return view('emailVerification')->with('user', $user);
    }

    /**
     * Resend email verification link
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function resend(Request $request)
    {
        $user = $request->user();

        try {
            $this->sendVerificationEmail($user);

            return $this->sendResponse('Please confirm yourself by clicking on verify user button sent to you on your email', 200);
        } catch (\Exception $e) {
            return $this->sendError($e, 500);
        }
    }
}
