<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\BusinessAddressResource;
use App\Http\Resources\BusinessCategoryResource;
use App\Http\Resources\BusinessImageResource;
use App\Models\BusinessRegistration;
use App\Models\Configuration;
use App\Models\User;
use App\Traits\APIConnecter;
use Illuminate\Support\Facades\Storage;
use Response;

/**
 * Class NearByPartnersAPIController
 * @package App\Http\Controllers\API
 */

class NearByRelatedPartnersAPIController extends AppBaseController
{

    use APIConnecter;
    private $partnerBusinesses;
    /**
     * Return partner distance from user's current location.
     * GET|HEAD /nearByPartners
     *
     * @param Request $request
     * @return Response
     */

    public function __construct(BusinessRegistration $partnerBusinesses)
    {
        Configuration::getConfig();
        $this->partnerBusinesses = $partnerBusinesses;
    }


    public function nearByRelatedPartners(Request $request)
    {
        $partners = [];
        $partnerBusinessAddress = [];
        $partnerBusinesses = [];

        $request->validate([
            'partnerId' => 'string|exists:businessRegistrations,partnerId',
        ]);



        if ($request->has('partnerId')) {
            $partnerBusinesses = $this->partnerBusinesses->join('businessAddresses', 'businessRegistrations.partnerId', '=', 'businessAddresses.partnerId')
                ->join('businessCategories', 'businessCategories.id', '=', 'businessRegistrations.businessCategoryId');

            $partner = BusinessRegistration::where('partnerId', $request->get('partnerId'))->with('businessAddresses')->first();

            $partnerBusinessAddress = $partner ? $partner->businessAddresses : [];

            $partnerBusinesses->where('businessRegistrations.partnerId', '!=', $partner ? $partner->partnerId : 0);
            $partnerBusinesses->where('businessRegistrations.businessCategoryId', $partner ? $partner->businessCategoryId : 0);

            $partnerBusinesses =  $partnerBusinesses->orderBy('businessRegistrations.created_at', 'desc')
                ->groupBy('businessRegistrations.partnerId')
                ->get();
        }

        // for current user logged in active business

        if ($request->user()->tokenCan('gridBusiness')) {

            $partnerBusinesses = $this->partnerBusinesses->join('businessAddresses', 'businessRegistrations.partnerId', '=', 'businessAddresses.partnerId')
                ->join('businessCategories', 'businessCategories.id', '=', 'businessRegistrations.businessCategoryId');

            $partnerBusinessAddress = User::activeBusiness() ? User::activeBusiness()->businessAddresses : [];

            $partnerBusinesses->where('businessRegistrations.partnerId', '!=', User::activeBusiness() ? User::activeBusiness()->partnerId : 0);

            $partnerBusinesses->where('businessRegistrations.businessCategoryId',  User::activeBusiness() ? User::activeBusiness()->businessCategoryId : 0);

            $partnerBusinesses =  $partnerBusinesses->orderBy('businessRegistrations.created_at', 'desc')
                ->groupBy('businessRegistrations.partnerId')
                ->get();
        }
        $logType= $request->user()->tokenCan('gridBusiness')?'gridBusiness':'Admin';
        if (count((array)$partnerBusinesses) > 0 && count((array)$partnerBusinessAddress) > 0) {
            $partners = $this->partnerDetails($partnerBusinesses, $partnerBusinessAddress, $logType);
        }

        $message = count($partners) > 0 ? 'Near By Related Partners retrieved successfully' : 'No results found!';

        return $this->sendResponse($partners, $message);
    }



    private function partnerDetails($partnerBusinesses, $partnerBusinessAddress, $logType)
    {

        return collect($partnerBusinesses ?? [])->map(function ($partner) use ($partnerBusinessAddress, $logType) {

            if (count((array)$partnerBusinessAddress) > 0) {

                $origins = $this->getGrideCodeDetails($partnerBusinessAddress, $logType);

                $destinations = $this->getGrideCodeDetails($partner->businessAddresses, $logType);


                $currentDistanceMatrix = $this->calculateDistanceMatrix($origins, $destinations);

                if (!empty($currentDistanceMatrix)) {
                    return [
                        'partnerId' => $partner->partnerId,
                        'businessName' => $partner->businessName,
                        'businessEmail' => $partner->businessEmail,
                        'description' => $partner->description,
                        'website' => $partner->website,
                        'logo' => Storage::disk('azure')->url('/businessLogo/' . $partner->logo),
                        'isHeadQuarter' => $partner->isHeadQuarter,
                        'headQuarterId' => $partner->headQuarterId,
                        'businessCategoryId' => $partner->businessCategoryId,
                        'partnerCategory' => new BusinessCategoryResource($partner->businessCategory),
                        'partnerAddresses' => BusinessAddressResource::collection($partner->businessAddresses),
                        'partnerImages' => BusinessImageResource::collection($partner->businessImages),
                        'currentDistanceMatrix' => $currentDistanceMatrix

                    ];
                }
                return false;
            }
            return false;
        })->reject(function ($value) {
            return $value === false || $value === null;
        });
    }
}
