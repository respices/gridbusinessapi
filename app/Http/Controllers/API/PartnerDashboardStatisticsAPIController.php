<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\PaymentResource;
use App\Models\AccessLog;
use App\Models\BusinessAddress;
use App\Models\BusinessContact;
use App\Models\BusinessRegistration;
use App\Models\PartnerSubscription;
use App\Models\Payment;
use Illuminate\Support\Facades\DB;
use Response;

/**
 * Class PartnerDashboardStatisticsAPIController  
 * @package App\Http\Controllers\API
 */

class PartnerDashboardStatisticsAPIController   extends AppBaseController
{


    /**
     *  Get API statistics that has been made by a Partner from GridNav API
     * 
     * GET|HEAD /gridCodeStatistics
     *
     * @param Request $request
     * @return Response
     */


    public function index($partnerId)
    {

       

        $statistics = [
            'apiCalls' =>$this->apiCalls($partnerId),
            'gridCodeSearched' => count($this->gridCodeSearched($partnerId)),
            'endPointCalls' => count($this->endPointCalls($partnerId)),
            'totalBranches' => $this->totalBranches($partnerId),
            'totalBusinessAddress' => $this->totalBusinessAddress($partnerId),
            'totalBusinessContacts' => $this->totalBusinessContacts($partnerId),
            'invoices'=>$this->invoices($partnerId),
            'latestGridCodes'=>$this->latestGridCodes($partnerId),
            'creditCountLeft'=>$this->creditCountLeft($partnerId)
        ];
        
        return $this->sendResponse($statistics, 'Statistics retrieved successfully');
    }

    private function getPartnerWithBranches($partnerId){
        $branchIds=[];
        $branchIds = BusinessRegistration::where('headQuarterId',$partnerId)->pluck('partnerId');
        $branchIds[]=$partnerId;
        return  $branchIds;
    }

    private function apiCalls($partnerId)
    {
        return  AccessLog::whereIn('partnerId',$this->getPartnerWithBranches($partnerId))->count();
    }

    private function creditCountLeft($partnerId)
    {
        $partnerSubscription = PartnerSubscription::where('partnerId', $partnerId)->where('status', 'Active')->first();
        return  $partnerSubscription?$partnerSubscription->currentCredits:0;
    }


    private function gridCodeSearched($partnerId)
    {
        
        return  AccessLog::select('searchedGridCode', DB::raw('count(searchedGridCode) as totalGridCode,searchedGridCode'))
        ->whereNotNull('searchedGridCode')
        ->whereNotNull('partnerId')
        ->whereIn('partnerId',$this->getPartnerWithBranches($partnerId))
        ->groupBy('searchedGridCode')->get();
    }


    private function endPointCalls($partnerId)
    {
        return  AccessLog::select('sourceUrl', DB::raw('count(sourceUrl) as totalendPointsCalls,sourceUrl'))
        ->whereNotNull('sourceUrl')
        ->whereNotNull('partnerId')
        ->whereIn('partnerId',$this->getPartnerWithBranches($partnerId))
        ->groupBy('sourceUrl')->get();
    }

    private function latestGridCodes($partnerId)
    {

        $gridCodeLists=  AccessLog::whereNotNull('searchedGridCode')->whereIn('partnerId',$this->getPartnerWithBranches($partnerId))->orderBy('id','desc')->take(5)->get();
        
        if(count( $gridCodeLists ) == 0){
            return [];
        }
        return collect($gridCodeLists)->map(function ($gridCodeList)  {
            $timezone=config('app.timezone');
            $gridCodeList['humanReadableConStartDateTime'] = \Carbon\Carbon::parse($gridCodeList->conStartDateTime)->timezone($timezone)->diffForHumans();
            $gridCodeList['humanReadableConEndDateTime']= \Carbon\Carbon::parse($gridCodeList->conEndDateTime)->timezone($timezone)->diffForHumans();
           return $gridCodeList;
        })->reject(function ($value) {
            return $value === false || $value === null;
        });
       
    }
    private function invoices($partnerId)
    {
        $payments = Payment::where('partnerId',$partnerId)->where('paymentStatus','!=','pending')->orderBy('id','desc')->take(5)->get();
          return PaymentResource::collection($payments);
    }
    


    private function totalBranches($partnerId)
    {
        return  BusinessRegistration::where('headQuarterId',$partnerId)->count();
    }

    //totalBusinessAddress
    private function totalBusinessAddress($partnerId)
    {
        $countAdress=[];
        $partnerBranches = BusinessRegistration::whereIn('partnerId',$this->getPartnerWithBranches($partnerId))->with('businessAddresses')->get();
        foreach($partnerBranches as $partnerBranch){
            $countAdress[]=count($partnerBranch->businessAddresses);
        }
        return  array_sum($countAdress);
    }
    //totalBusinessContacts
    private function totalBusinessContacts($partnerId)
    {
        return  BusinessContact::where('partnerId',$partnerId)->count();
    }
}
