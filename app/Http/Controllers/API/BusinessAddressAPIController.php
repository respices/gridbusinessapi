<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBusinessAddressAPIRequest;
use App\Http\Requests\API\UpdateBusinessAddressAPIRequest;
use App\Models\BusinessAddress;
use App\Repositories\BusinessAddressRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\BusinessAddressResource;
use App\Models\BusinessRegistration;
use App\Models\Country;
use App\Models\User;
use App\Traits\APIConnecter;
use Response;

/**
 * Class BusinessAddressController
 * @package App\Http\Controllers\API
 */

class BusinessAddressAPIController extends AppBaseController
{
    use APIConnecter;
    /** @var  BusinessAddressRepository */
    private $businessAddressRepository;

    public function __construct(BusinessAddressRepository $businessAddressRepo)
    {
        $this->businessAddressRepository = $businessAddressRepo;
    }

    /**
     * Display a listing of the BusinessAddress.
     * GET|HEAD /businessAddresses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        
        if (count($request->except(['partnerId'])) > 0) {
            $businessAddresses = $this->businessAddressRepository->businessAddressQueries($request);
        } else {

           
                $request->merge(['partnerId' => User::activeBusiness()->partnerId, 'viewHiddenColumns' => true]);
            
            
            $businessAddresses = $this->businessAddressRepository->all(
                $request->except(['skip', 'limit']),
                $request->get('skip'),
                $request->get('limit')
            );
        }


        $message = count($businessAddresses) > 0 ? 'Partner Addresses retrieved successfully' : 'No results found!';
        return $this->sendResponse(BusinessAddressResource::collection($businessAddresses), $message);
    }




    /**
     * Display a listing of the BusinessAddress By Partner ID.
     * GET|HEAD /getAllBusinessAddressByPartnerID
     *
     * @param Request $request
     * @return Response
     */

    public function getAllBusinessAddressByPartnerID($partnerId)
    {
        $businessRegistration = BusinessRegistration::where('partnerId', $partnerId)->first();
        if (empty($businessRegistration)) {
            return $this->sendError('Partner Id not found', 404);
        }

        $this->userHasAccess($businessRegistration->userId);

        $businessAddresses = $this->businessAddressRepository->all(
            ['partnerId' => $businessRegistration->id]
        );


        return $this->sendResponse(BusinessAddressResource::collection($businessAddresses), 'Partner Addresses retrieved successfully');
    }


    public function businessAddressByPartnerID($partnerId)
    {
        
        $businessRegistration = BusinessRegistration::where('partnerId', $partnerId)->first();
        if (empty($businessRegistration)) {
            return $this->sendError('Partner Id not found', 404);
        }

        $this->userHasAccess($businessRegistration->userId);

        $businessAddresses = BusinessAddress::where('partnerId', $partnerId)->first();

        return $this->sendResponse($businessAddresses?new BusinessAddressResource($businessAddresses):null, 'Partner Addresses retrieved successfully');
    }

    /**
     * Store a newly created BusinessAddress in storage.
     * POST /businessAddresses
     *
     * @param CreateBusinessAddressAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBusinessAddressAPIRequest $request)
    {
        $input = $request->all();

        $businessRegistration = BusinessRegistration::find($request->get('partnerId'));
        $this->userHasAccess($businessRegistration->userId);

        $countryCode = Country::find($request->get('countryId'))->countryCode;


        $response = $this->connectToFindGridcodeAPI($request->get('grideCode'), $countryCode, $request->get('partnerId'), url()->full(),'gridBusiness');

        if ($response['success']) {
            
            $input['address'] = $response['response']['title'] ? $response['response']['title'] : '....';
         
            $businessAddress =  BusinessAddress::updateOrCreate(['partnerId'=>$request->get('partnerId')], $input);

            $this->sendToTwilio($businessAddress->telephone);
          
            return $this->sendResponse(new BusinessAddressResource($businessAddress), 'Partner Address saved successfully');
        } else {
            return $this->sendError($response['response'], 404);
        }
    }


    public function resendTelephoneToTwilio($businessAddressId, Request $request)
    {
       
        $businessAddress = $this->businessAddressRepository->find($businessAddressId);

        if (empty($businessAddress)) {
            return $this->sendError('Partner Address not found');
        }

        $data = $request->validate([
            'telephone' => ['required', 'string'],
        ]);

        if($businessAddress->telephone==$data['telephone'] && $businessAddress->isTelephoneVerified){
            return $this->sendResponse(new BusinessAddressResource($businessAddress),'Telephone already verified', 200);
        }

        $isValid =   $this->sendToTwilio($data['telephone']);

        if ($isValid) {
            $businessAddress = $businessAddress->update(['telephone' => $data['telephone']]);
            return $this->sendResponse('Verification code sent to your phone number', 200);
        }
        return $this->sendError('Invalid phone number!,please enter valid phone number, make sure country code is added!', 422);
    }

    public function resendTelephone2ToTwilio($businessAddressId, Request $request)
    {
        $businessAddress = $this->businessAddressRepository->find($businessAddressId);

        if (empty($businessAddress)) {
            return $this->sendError('Business Address not found');
        }

        $data = $request->validate([
            'telephone2' => ['required', 'string'],
        ]);

        $isValid =   $this->sendToTwilio($data['telephone2']);
        if ($isValid) {
            $businessAddress = $businessAddress->update(['telephone2' => $data['telephone2']]);
            return $this->sendResponse('Verification code sent to your phone number', 200);
        }
        return $this->sendError('Invalid phone number!,please enter valid phone number, make sure country code is added!', 422);
    }




    public function verifyTelephone($businessAddressId, Request $request)
    {
        $businessAddress = $this->businessAddressRepository->find($businessAddressId);

        if (empty($businessAddress)) {
            return $this->sendError('Partner Address not found');
        }

        $data = $request->validate([
            'verification_code' => ['required', 'numeric'],
        ]);

        $isValid =  $this->twilioVerificationCode(['phone' => $businessAddress->telephone, 'verification_code' => $data['verification_code']]);
        if ($isValid) {
             $businessAddress->isTelephoneVerified = true;
            $businessAddress->save();
            return $this->sendResponse(new BusinessAddressResource($businessAddress),'Telephone verified successfully', 200);
            
        }
        return $this->sendError('Invalid verification code,please resend again!', 422);
    }



    public function verifyTelephone2($businessAddressId, Request $request)
    {
        $businessAddress = $this->businessAddressRepository->find($businessAddressId);

        if (empty($businessAddress)) {
            return $this->sendError('Partner Address not found');
        }

        $data = $request->validate([
            'verification_code' => ['required', 'numeric'],
        ]);

        $isValid =  $this->twilioVerificationCode(['phone' => $businessAddress->telephone2, 'verification_code' => $data['verification_code']]);
        if ($isValid) {
            $businessAddress = $businessAddress->update(['isTelephone2Verified' => true]);
            return $this->sendResponse('Telephone verified successfully', 200);
        }
        return $this->sendError('Invalid verification code,please resend again!', 422);
    }

    /**
     * Display the specified BusinessAddress.
     * GET|HEAD /businessAddresses/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BusinessAddress $businessAddress */
        $businessAddress = $this->businessAddressRepository->find($id);

        if (empty($businessAddress)) {
            return $this->sendError('Partner Address not found');
        }
        $this->userHasAccess($businessAddress->businessRegistration->userId);

        return $this->sendResponse(new BusinessAddressResource($businessAddress), 'Partner Address retrieved successfully');
    }

    /**
     * Update the specified BusinessAddress in storage.
     * PUT/PATCH /businessAddresses/{id}
     *
     * @param int $id
     * @param UpdateBusinessAddressAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBusinessAddressAPIRequest $request)
    {
        $input = $request->all();

        /** @var BusinessAddress $businessAddress */

        $businessAddress = $this->businessAddressRepository->find($id);

        if (empty($businessAddress)) {
            return $this->sendError('Partner Address not found');
        }

        $this->userHasAccess($businessAddress->businessRegistration->userId);
       

        if ($request->has('grideCode')) {

            if ($request->has('countryId')) {
                $countryCode = Country::find($request->get('countryId'))->countryCode;
            } else {
                $countryCode = $businessAddress->country->countryCode;
            }
            
            $partnerId = $request->has('partnerId')?$request->get('partnerId'): $businessAddress->partnerId;
            
            $response = $this->connectToFindGridcodeAPI($request->get('grideCode'), $countryCode, $partnerId , url()->full(),'gridBusiness');

            if ($response['success']) {
                $input['address'] = $response['response']['title'] ? $response['response']['title'] : '....';
            } else {
                return $this->sendError($response['response'], 404);
            }
        }

        if($businessAddress->telephone!=$request->get('telephone')){
            
            $input['isTelephoneVerified']=false;
            
            $this->sendToTwilio($request->get('telephone'));
        }

        $businessAddress = $this->businessAddressRepository->update($input, $id);
      
       

        return $this->sendResponse(new BusinessAddressResource($businessAddress), 'BusinessAddress updated successfully');
    }

    /**
     * Remove the specified BusinessAddress from storage.
     * DELETE /businessAddresses/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BusinessAddress $businessAddress */
        $businessAddress = $this->businessAddressRepository->find($id);

        if (empty($businessAddress)) {
            return $this->sendError('Partner Address not found',404);
        }

        $this->userHasAccess($businessAddress->businessRegistration->userId);

        $businessAddress->delete();

        return $this->sendSuccess('Partner Address deleted successfully');
    }


   


    /**
     * Remove the specified BusinessAddress from storage.
     * DELETE /businessAddresses/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */

    public function destoryAllBusinessAddressByPartnerID($partnerId)
    {
        $businessRegistration = BusinessRegistration::where('partnerId', $partnerId)->first();

        if (empty($businessRegistration)) {
            return $this->sendError('Partner Id not found', 404);
        }
        $this->userHasAccess($businessRegistration->userId);

        $businessAddresses = $this->businessAddressRepository->all(
            ['partnerId' => $businessRegistration->id]
        );

        foreach ($businessAddresses as $businessAddress) {
            $businessAddress->delete();
        }

        return $this->sendSuccess('All Business Address deleted successfully');
    }
}
