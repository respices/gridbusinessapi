<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBusinessCategoryAPIRequest;
use App\Http\Requests\API\UpdateBusinessCategoryAPIRequest;
use App\Models\BusinessCategory;
use App\Repositories\BusinessCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\BusinessCategoryResource;
use Illuminate\Support\Facades\Auth;
use Response;

/**
 * Class BusinessCategoryController
 * @package App\Http\Controllers\API
 */

class BusinessCategoryAPIController extends AppBaseController
{
    /** @var  BusinessCategoryRepository */
    private $businessCategoryRepository;

    public function __construct(BusinessCategoryRepository $businessCategoryRepo)
    {
        $this->middleware(['auth:admin','scope:admin'])->except(['index','show']);
        $this->businessCategoryRepository = $businessCategoryRepo;
    }

    /**
     * Display a listing of the BusinessCategory.
     * GET|HEAD /businessCategories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        

        $businessCategories = $this->businessCategoryRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );
        
        $showTimestamp = false;
        
        if (Auth::guard('admin')->check()) {
            $request->merge(['viewHiddenColumns'=>true]);
            
        }
        $message = count($businessCategories) > 0?'Business Categories retrieved successfully':'No results found!';

        return $this->sendResponse(BusinessCategoryResource::collection($businessCategories, $showTimestamp), $message);
    }

    /**
     * Store a newly created BusinessCategory in storage.
     * POST /businessCategories
     *
     * @param CreateBusinessCategoryAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBusinessCategoryAPIRequest $request)
    {
        $input = $request->all();

        $businessCategory = $this->businessCategoryRepository->create($input);

        return $this->sendResponse(new BusinessCategoryResource($businessCategory), 'Business Category saved successfully');
    }

    /**
     * Display the specified BusinessCategory.
     * GET|HEAD /businessCategories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BusinessCategory $businessCategory */
        $businessCategory = $this->businessCategoryRepository->find($id);

        if (empty($businessCategory)) {
            return $this->sendError('Business Category not found');
        }

        return $this->sendResponse(new BusinessCategoryResource($businessCategory), 'Business Category retrieved successfully');
    }

    /**
     * Update the specified BusinessCategory in storage.
     * PUT/PATCH /businessCategories/{id}
     *
     * @param int $id
     * @param UpdateBusinessCategoryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBusinessCategoryAPIRequest $request)
    {
        $input = $request->all();

        /** @var BusinessCategory $businessCategory */
        $businessCategory = $this->businessCategoryRepository->find($id);

        if (empty($businessCategory)) {
            return $this->sendError('Business Category not found');
        }

        $businessCategory = $this->businessCategoryRepository->update($input, $id);

        return $this->sendResponse(new BusinessCategoryResource($businessCategory), 'BusinessCategory updated successfully');
    }

    /**
     * Remove the specified BusinessCategory from storage.
     * DELETE /businessCategories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BusinessCategory $businessCategory */
        $businessCategory = $this->businessCategoryRepository->find($id);

        if (empty($businessCategory)) {
            return $this->sendError('Business Category not found');
        }

        $businessCategory->delete();

        return $this->sendSuccess('Business Category deleted successfully');
    }
}
