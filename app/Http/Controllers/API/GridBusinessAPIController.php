<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\GridNavCodeResource;
use App\Traits\APIConnecter;
use Response;

/**
 * Class GridNavCodeController
 * @package App\Http\Controllers\API
 */

class GridBusinessAPIController extends AppBaseController
{

    use APIConnecter;
 

    public function gridBusinessFinder(Request $request)
    {

           
            $response = $this->connectToFindGridcodeAPI($request->get('gridCode'), $request->get('countryCode'), $request->get('partnerId'),url()->full());

            if ($response['success']) {

                return $this->sendResponse($response['response'], 'Grid Code retrieved successfully');
            } else {
                return $this->sendError($response['response'], 404);
            }
        
    }

}
