<?php

namespace App\Http\Controllers\API;

use App\Events\PaymentEvent;
use App\Http\Requests\API\CreatePaymentAPIRequest;
use App\Http\Requests\API\UpdatePaymentAPIRequest;
use App\Models\Payment;
use App\Repositories\PaymentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\PaymentOtpValidateAPIRequest;
use App\Http\Resources\PaymentResource;
use App\Models\BusinessRegistration;
use App\Models\Configuration;
use App\Models\PartnerCredit;
use App\Models\PartnerSubscription;
use App\Models\PaymentGateway;
use App\Models\Subscription;
use App\Traits\APIConnecter;
use App\Traits\GDateTime;
use Response;

/**
 * Class PaymentController
 * @package App\Http\Controllers\API
 */

class PaymentAPIController extends AppBaseController
{
    use GDateTime;
    /** @var  PaymentRepository */
    private $paymentRepository;

    use APIConnecter;
    public function __construct(PaymentRepository $paymentRepo)
    {
        Configuration::getConfig();
        $this->paymentRepository = $paymentRepo;
    }

    /**
     * Display a listing of the Payment.
     * GET|HEAD /payments
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if (empty($request->get('partnerId'))) {
            return $this->sendError('PartnerId not found');
        }
        $request->merge(['partnerId' => $request->get('partnerId')]);
        $payments = $this->paymentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        $message = count($payments) > 0 ? 'Payments retrieved successfully' : 'No results found!';
      
        return $this->sendResponse(PaymentResource::collection($payments), $message);
    }

    /**
     * Store a newly created Payment in storage.
     * POST /payments
     *
     * @param CreatePaymentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePaymentAPIRequest $request)
    {
        $input = $request->all();

        //mobile-money
\Log::info($request->all());
        if ($request->get('paymentMethod') == 'card' || $request->get('paymentMethod') == 'mobile-money') {

            if ($request->get('paymentMethod') == 'mobile-money') {
                $request->validate(['phoneNumber'  => 'required|string']);
            }

            if ($request->get('paymentMethod') == 'card') {
                $request->validate(['cardId'  => 'required|string']);
            }
        }

        $paymentGatway = PaymentGateway::activeGateway('Flutterwave');
        $subscription = Subscription::find($request->get('subscriptionId'));

        $input['paymentGatewayId'] = $paymentGatway->id;
        $input['paymentStatus'] = 'pending';
        $input['amount'] = $request->get('total');
        $input['currencyId'] = $subscription->currencyId;

        if ($paymentGatway) {
            if ($payment = Payment::parnterPaymentStatus($request->get('partnerId'), 'pending')) {

                $payment = $this->paymentRepository->update($input, $payment->id);
            } else {
                
                $payment = $this->paymentRepository->create($input);
            }
                
            if($payment){
                    
                        $partnerSubscription = PartnerSubscription::updateOrCreate(
                            [
                            'partnerId' => $request->get('partnerId'),
                            'status' => 'Pending'
                            ],
                            [
                            'partnerId'=>$request->get('partnerId'),
                            'subscriptionId'=>$subscription->id,
                            'dateSubscribed'=>$this->getTodayDate(),
                            'dateExpired'=>$this->getNextDate($request->get('subscriptionPeriod'),$request->get('subscriptionQuantity')),
                            'subscriptionQuantity'=>$request->get('subscriptionQuantity'),
                            'subscriptionPeriod'=>$request->get('subscriptionPeriod'),
                            'paymentId'=>$payment->id,
                            'status'=>'Pending'
                            ]
                        );
                    
                    }


            if ($request->get('paymentMethod') == 'mobile-money') {
                return $this->payWithMomo($payment, $request->get('phoneNumber'), $paymentGatway,$partnerSubscription);
            }
        } else {

            return $this->sendError('Payment Gateway not found');
        }
    }


    public function payWithMomo($payment, $phoneNumber, $paymentGatway, $partnerSubscription)
    {

        $tx_ref = base64_encode($payment->partnerId . '|' . $payment->id . '|' . $partnerSubscription->id);
        \Log::info($tx_ref);
        $jsonResponse = $this->connectToFlutterwaveMobileMoney([
            'tx_ref' => $tx_ref,
            "order_id" => $payment->id,
            'amount' => $payment->amount,
            'currency' => $payment->currency->currencySymbol,
            'fullname' => $payment->businessRegistration->businessName,
            'email' => $payment->businessRegistration->businessEmail,
            'phone_number' => $phoneNumber
        ], $paymentGatway->secretKey);

    
        if (isset($jsonResponse) && $jsonResponse['status'] == 'error') {
            return $this->sendError($jsonResponse['message'], 400);
        }
        setcookie('flutterWaveCurrentRedirectuRL-' . $payment->partnerId, $jsonResponse['meta']['authorization']['redirect'], time() + (86400 * 30), "/");


        return $this->sendResponse(['payment' => new PaymentResource($payment), 'redirectUrl' => $jsonResponse['meta']['authorization']['redirect']], $jsonResponse['message']);
    }




    public function otpValidate(PaymentOtpValidateAPIRequest $request)
    {

        $payment = Payment::parnterPaymentStatus($request->get('partnerId'), 'pending');

        if (empty($payment)) {
            return $this->sendError('Payment not found');
        }

        $paymentGatway = PaymentGateway::activeGateway('Flutterwave');

        if ($paymentGatway) {


            $jsonResponse = $this->connectToFlutterwaveOptValidate($request->get('otp'), $payment->partnerId, $paymentGatway->secretKey);

           
            if ($jsonResponse['status'] == 'error') {
                return $this->sendError($jsonResponse['message'], 400);
            }

            return $this->sendResponse($jsonResponse, $jsonResponse['message']);
            
        } else {
            return $this->sendError('Payment Gateway not found');
        }
    }



    public function approveTransaction(Request $request)
    {

        $jsonResponse = $request->all();

        $jsonResponseData = $jsonResponse['data'];
\Log::info($jsonResponseData);
        if (isset($jsonResponseData['tx_ref'])) {

            // (int)$tx_ref[0] = partner ID
            // (int)$tx_ref[1] = payment ID
            // (int)$tx_ref[2] = subscription ID
            
            $tx_ref = explode('|',  base64_decode($jsonResponseData['tx_ref']));
            \Log::info($tx_ref);
            $partner = BusinessRegistration::find($tx_ref[0]);
            $payment = $this->paymentRepository->find($tx_ref[1]);
            $partnerSubscription = PartnerSubscription::find($tx_ref[2]);
        
          
            if ($payment) {
                $payment->paymentStatus = strtoupper($jsonResponseData['status']);
                $payment->paymentMethod = strtoupper($jsonResponseData['payment_type']);
                $payment->paymentConfirmationId = $jsonResponseData['flw_ref'];
                
                $payment->save();


                if($jsonResponseData['status']=='successful' && $partner && $partnerSubscription){

                    // PartnerSubscription::rollOverUnsusedSubscription($partner->partnerId);
                    
                    $currentPaidCredits=(int)$partnerSubscription->subscription->credit*$partnerSubscription->subscriptionQuantity;

                    PartnerSubscription::addCurrentPartnerCredits($partnerSubscription,$currentPaidCredits);
                    PartnerSubscription::makeAllActivePartnerSubscriptionToInactive($partner->partnerId);
                    $partnerSubscription->status= 'Active';
                    $partnerSubscription->save();
                     
                }
                // event(new PaymentEvent($payment));
            }
        }

        return $this->sendResponse($jsonResponseData, 'Payment made successfully');
    }


    /**
     * Display the specified Payment.
     * GET|HEAD /payments/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Payment $payment */
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            return $this->sendError('Payment not found');
        }

        return $this->sendResponse(new PaymentResource($payment), 'Payment retrieved successfully');
    }

    /**
     * Update the specified Payment in storage.
     * PUT/PATCH /payments/{id}
     *
     * @param int $id
     * @param UpdatePaymentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaymentAPIRequest $request)
    {
        $input = $request->all();

        /** @var Payment $payment */
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            return $this->sendError('Payment not found');
        }

        $payment = $this->paymentRepository->update($input, $id);

        return $this->sendResponse(new PaymentResource($payment), 'Payment updated successfully');
    }

    /**
     * Remove the specified Payment from storage.
     * DELETE /payments/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Payment $payment */
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            return $this->sendError('Payment not found');
        }

        $payment->delete();

        return $this->sendSuccess('Payment deleted successfully');
    }
}
