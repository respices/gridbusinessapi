<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;


/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Grid Business Api documentation",
 *      description="Grid Business Api documentation",
 *      @OA\Contact(
 *          name="API Support",
 *          email="admin@admin.com",
 *          url= "http://www.swagger.io/support",
 *      ),
 *      @OA\License(
 *          name="Apache 2.0",
 *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *      )
 * )
* @OA\SecurityScheme(
*   securityScheme="token",
*   type="apiKey",
*   name="Authorization",
*   in="header"
* ),
 *
 * @OA\Server(
 *      url=L5_SWAGGER_CONST_HOST,
 *      description="API Server"
 * )

 *
 * @OA\Tag(
 *     name="GridNav APIs",
 *     description="API Endpoints of GridNav"
 * )
 * @OA\Tag(
 *     name="Grid Business Portal APIs",
 *     description="Endpoints of GridBusiness Portal"
 * )
 *@OA\Tag(
 *     name="Grid Business Admin APIs",
 *     description="Endpoints of GridBusiness Admin"
 * )
 */

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}

