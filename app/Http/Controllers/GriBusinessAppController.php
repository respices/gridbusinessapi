<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GriBusinessAppController extends Controller
{
   

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function portal()
    {
        return view('portal');
    }
}
