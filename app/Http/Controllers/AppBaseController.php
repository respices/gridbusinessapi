<?php

namespace App\Http\Controllers;

use App\Mail\EmailValidation;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use InfyOm\Generator\Utils\ResponseUtil;
use Response;


/**
 * @SWG\Swagger(
 *   basePath="/api",
 *   @SWG\Info(
 *     title="GridBusiness Api documentation",
 *     version="1.0.0",
 *   )
 * )
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */
class AppBaseController extends Controller
{


    public function sendResponse($result, $message)
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error), $code);
    }

    public function sendSuccess($message)
    {
        return Response::json([
            'success' => true,
            'message' => $message
        ], 200);
    }


    public function generateUniqueCode()
    {
        do {
            $code = random_int(100000, 999999);
        } while (User::where("emailVerifyToken", "=", $code)->first());

        return $code;
    }

    public function sendVerificationEmail($user)
    {
         $raw       = $user->id;

        $user->emailVerifyToken = $this->generateUniqueCode();
        $user->save();
        $token = base64_encode($raw);
        Mail::to($user->email)->send(new EmailValidation($user, $token, $user->emailVerifyToken));
    }

    public function getMachineId()
    {
        ob_start();
        system('ipconfig /all');
        $mycom = ob_get_contents();
        ob_clean();
        $findme = "Physical";
        $pmac = strpos($mycom, $findme);
        $mac = substr($mycom, ($pmac + 36), 17);
        return $mac;
    }

    public function userHasAccess($userId)
    {
        if(Auth::guard('gridBusiness')->user() && Auth::guard('gridBusiness')->user()->id != $userId){
            return $this->sendError("Unauthorized (you don't have permission to access this api )",406);
        }
        
    }

    
    public function transformGoogleMapSingleToMultDistanceMatrix($response, $travelmode = 'DRIVING', $traffic_model = "BEST_GUESS", $sourceDetails = null,$destinationDetails=[])
    {
        $results = [];
        $i = 0;
        
        while ($i < count($response['rows'][0]['elements'])) {
            $distanceValue = $response['rows'][0]['elements'][$i]['distance'];
            $durationValue = $response['rows'][0]['elements'][$i]['duration'];
            $durationInTraffic = $response['rows'][0]['elements'][$i]['duration_in_traffic'];

          
            $results[] = [
                'source' => $sourceDetails,
                'destination' => $destinationDetails[$i],
                'distance_text' => $distanceValue['text'],
                'duration_text' => $durationValue['text'],
                'distance_value' => round($distanceValue['value']/1000,1),
                'duration_value' => round($durationValue['value']/60,0),
                'duration_traffic_text' => $durationInTraffic['text'],
                'duration_traffic_value' => round($durationInTraffic['value']/60,0),
                'travel_mode' => strtolower($travelmode),
                'traffic_model' => strtolower($traffic_model)
            ];
            
            $i++;
        }
        return $results;
    }

    public function transformGoogleMapDistanceMatrix($response, $travelmode = 'DRIVING', $traffic_model = "BEST_GUESS", $sourceDetails = null,$destinationDetails=null)
    {
        $results = [];
        $i = 0;
     
           $distanceValue = $response['rows'][0]['elements'][$i]['distance'];
            $durationValue = $response['rows'][0]['elements'][$i]['duration'];
            $durationInTraffic = $response['rows'][0]['elements'][$i]['duration_in_traffic'];

          
           return [
                'source' => $sourceDetails,
                'destination' => $destinationDetails,
                'distance_text' => $distanceValue['text'],
                'duration_text' => $durationValue['text'],
                'distance_value' => round($distanceValue['value']/1000,1),
                'duration_value' => round($durationValue['value']/60,0),
                'duration_traffic_text' => $durationInTraffic['text'],
                'duration_traffic_value' => round($durationInTraffic['value']/60,0),
                'travel_mode' => strtolower($travelmode),
                'traffic_model' => strtolower($traffic_model)
            ];
            
    }


    public function getGrideCodeDetails($businessAddresses,$logType)
    {
        $i = 0;
        $data = [];
        $titles = [];
        while ($i < count($businessAddresses)) {

            $businessAddress = $businessAddresses[$i];

            $destination = $this->connectToFindGridcodeAPI(
                isset($businessAddress->grideCode) ? $businessAddress->grideCode : null,
                isset($businessAddress->country) ? $businessAddress->country->countryCode : null,
                $businessAddress->partnerId,
                url()->full(),
                $logType
            );

            if ($destination['success']) {
                $data[]          = $destination['response']['latitude'] . ',' . $destination['response']['longitude'];
                $titles[]  = $destination['response']['title'];
            }

            $i++;
        }
        return ['locations' => $data, 'titles' => $titles];
    }

    public function calculateDistanceMatrix($origins, $destinations,$partnerNearByTo=null,$partnerNearByFrom=null)
    {

        $distanceMatrix = [];

        if (count((array)$origins) > 0 && count((array)$destinations) > 0) {

            $originsImploded =  implode(",", $origins['locations']);
            $destinationsImploded =  implode("|", $destinations['locations']);


            $response = $this->connectToGoogleMapDistanceMatrix($originsImploded, $destinationsImploded);

            if ($response['status'] == 'OK') {

                $i = 0;

                while ($i < count($response['rows'][0]['elements'])) {
                    $distanceValue = $response['rows'][0]['elements'][$i]['distance'];
                    $durationValue = $response['rows'][0]['elements'][$i]['duration'];
                    $durationInTraffic = $response['rows'][0]['elements'][$i]['duration_in_traffic'];
                   
                    $distanceObj=[
                        'destination_addresses' => is_array($destinations['titles']) ? $destinations['titles'][$i] : '',
                        'distance_text' => $distanceValue['text'],
                        'distance_value' => $distanceValue['value'] / 1000,
                        'duration_text' => $durationValue['text'],
                        'duration_value' => $durationValue['value'] / 60,
                        'duration_in_traffic' => $durationInTraffic['text']
                    ];

                    if ($partnerNearByTo) {

                        $distance = $distanceValue['value'] / 1000;

                        if ($distance >= $partnerNearByFrom  && $distance <= $partnerNearByTo) {

                            $distanceMatrix[] = $distanceObj;
        
                        }

                    }else{

                        $distanceMatrix[] = $distanceObj;
    
                    } 

                  

                    $i++;
                }
            }
        }
        return $distanceMatrix;
    }
}
