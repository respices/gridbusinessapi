<?php

namespace App\Http\Middleware;

use App\Http\Controllers\AppBaseController;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IsEmailVerified extends AppBaseController
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::guard('gridBusiness')->check()) {
            if (Auth::guard('gridBusiness')->user()->email_verified_at) {

                return $next($request);
            }


            return $this->sendError('Please verify your email address.', 403);
        }
        
        return $next($request);
    }
}
