<?php

namespace App\Http\Middleware;

use App\Enums\ConfigKeyParameter;
use App\Http\Controllers\AppBaseController;
use App\Models\BusinessRegistration;
use App\Models\Configuration;
use App\Models\GridNavAccessToken;
use Closure;
use Illuminate\Http\Request;
use App\Traits\Encryption;

class AuthorizeGridNav extends AppBaseController
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    use Encryption;

    public function handle(Request $request, Closure $next)
    {
        try {


            $token = null;

            if ($request->header('Authorization')) {
                $token = $request->header('Authorization');
            }
            if ($request->bearerToken()) {
                $token = $request->bearerToken();
            }


            if ($token) {
             
                $decodeToken=explode('|',base64_decode($token));
                if(count($decodeToken) == 2){

                $secretKey=$decodeToken[0];
                $cypherText=$decodeToken[1];
                
                $originData= $this->decrypt($cypherText,$secretKey);
           
                if ($originData) {

                    parse_str($originData, $payload);

                    if ($payload && count((array)$payload) > 0) {

                        if (isset($payload['partnerId']) && isset($payload['userId']) && isset($payload['partnerBusinessName'])) {

                             if(!$this->showPartner($payload)){

                                return $this->sendError('Partner not found', 404);
                             }
                             //isSubscriptionActive
                             if (!BusinessRegistration::isSubscriptionActive($payload['partnerId'])) {
                                return $this->sendError('You do not have an active subscription!', 403);
                            }

                            if (!BusinessRegistration::isActive($payload['partnerId'])) {
                                return $this->sendError('You do not have an active business!', 403);
                            }
                
                            if (!BusinessRegistration::hasCredits($payload['partnerId'])) {
                                return $this->sendError('You do not have credits!', 403);
                            }
                
                            BusinessRegistration::reducePartnerCredits($payload['partnerId'],url()->full());

                            $request->merge(['partnerId' => $payload['partnerId'],'logType'=>'gridNav']);

                            return $next($request);
                        }
                    }
                }
              
            }
        }
           return $this->sendError('Unauthorized', 401);
        } catch (\Exception $e) {
            return $this->sendError('Unauthorized', 401);
        }
    }


    private function showPartner($payload)
    {

        return BusinessRegistration::where('partnerId', $payload['partnerId'])->where('businessName', $payload['partnerBusinessName'])->where('userId', $payload['userId'])->first();

    }
}
