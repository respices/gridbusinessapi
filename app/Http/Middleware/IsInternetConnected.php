<?php

namespace App\Http\Middleware;

use App\Http\Controllers\AppBaseController;
use Closure;
use Illuminate\Http\Request;

class IsInternetConnected extends AppBaseController
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ( @fopen("https://codespeedy.com", "r") ) 
            {
                return $next($request);
            } 
           
            return $this->sendError("You seem to be offline. Please check your internet connection.", 403);
       
        
    }
}
