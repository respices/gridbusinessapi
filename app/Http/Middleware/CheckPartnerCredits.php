<?php

namespace App\Http\Middleware;

use App\Http\Controllers\AppBaseController;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class CheckPartnerCredits extends AppBaseController
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
      
            // if (User::activeBusiness() && User::activeBusiness()->partnerCredits > 0) {
            //     return $next($request);
            // }

            // return $this->sendError('You dont have credits!', 403);
            return $next($request);
        
    }
}
