<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\BusinessRegistration;

class CanUsedByActiveBusiness extends AppBaseController
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
            if (User::activeBusiness()) {
                return $next($request);
            }

            return $this->sendError('You dont have an active business!', 403);
       
    }
}
