<?php

namespace App\Traits;

use Carbon\Carbon;

trait GDateTime
{

    function getTodayDate($timezone=null){
        $timezone=$timezone?$timezone:config('app.timezone');
        return Carbon::now()->timezone($timezone)->format('Y-m-d H:i:s');
    }

    function getLastMonthDate($timezone=null){
        $timezone=$timezone?$timezone:config('app.timezone');
        return Carbon::now()->timezone($timezone)->endOfMonth()->toDateString();
    }
   
    function getNextDate($period = 'Today', $number=0,$timezone=null){
        $timezone=$timezone?$timezone:config('app.timezone');
        switch (strtolower($period)) {
            case 'today':
                $date = Carbon::now()->timezone($timezone)->format('Y-m-d H:i:s');
                break;
            case 'day':
                $date =  Carbon::now()->timezone($timezone)->addDays($number)->format('Y-m-d H:i:s');
                break;

            case 'month':
                $date =  Carbon::now()->timezone($timezone)->addMonths($number)->format('Y-m-d H:i:s');
                break;

            case 'year':
                $date =  Carbon::now()->timezone($timezone)->addYears($number)->format('Y-m-d H:i:s');
                break;
            default:
            $date =  Carbon::now()->timezone($timezone)->format('Y-m-d H:i:s');
        }

        return  $date;
    }

    // Carbon::parse($yourDate)
}
