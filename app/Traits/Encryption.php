<?php

namespace App\Traits;


trait Encryption
{
    

    // --- Encrypt --- //
    function encrypt($plaintext, $secret_key = "5fgf5HJ5g27", $cipher = "AES-128-CBC")
    {

        $key = openssl_digest($secret_key, 'SHA256', TRUE);

        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivlen);
        // binary cipher
        $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, OPENSSL_RAW_DATA, $iv);
        // or replace OPENSSL_RAW_DATA & $iv with 0 & bin2hex($iv) for hex cipher (eg. for transmission over internet)

        // or increase security with hashed cipher; (hex or base64 printable eg. for transmission over internet)
        $hmac = hash_hmac('sha256', $ciphertext_raw, $key, true);
        return base64_encode($iv . $hmac . $ciphertext_raw);
    }


    // --- Decrypt --- //
    function decrypt($ciphertext, $secret_key = "5fgf5HJ5g27", $cipher = "AES-128-CBC")
    {

        $c = base64_decode($ciphertext);

        $key = openssl_digest($secret_key, 'SHA256', TRUE);

        $ivlen = openssl_cipher_iv_length($cipher);

        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len = 32);
        $ciphertext_raw = substr($c, $ivlen + $sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, OPENSSL_RAW_DATA, $iv);

        $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, true);
        if (hash_equals($hmac, $calcmac))
            return $original_plaintext;
    }


    // $encrypt_method = "AES-256-CBC";
    // $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
    // $secret_iv = '5fgf5HJ5g27'; // user define secret key
    // $key = hash('sha256', $secret_key);
    // $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo

    //     $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);

    // return $output;

    // --- Decrypt --- //
    // \Log::info($ciphertext);
    // --- Decrypt --- //

    // $encrypt_method = "AES-256-CBC";
    // $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
    // $secret_iv = '5fgf5HJ5g27'; // user define secret key
    // $key = hash('sha256', $secret_key);
    // $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
    //     $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
    //     $output = base64_encode($output);

    // return $output;


}
