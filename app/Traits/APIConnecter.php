<?php

namespace App\Traits;

use App\Enums\ConfigKeyParameter;
use App\Http\Controllers\API\AccessLogAPIController;
use App\Models\Configuration;
use GuzzleHttp\Client;
use Twilio\Rest\Client as Twilio;
use Larinfo;

trait APIConnecter
{
    use GDateTime;

    function connectToFindGridcodeAPI($gridCode, $countryCode, $partnerId = null, $sourceUrl = null,$logType=null)
    {
        $url_here = 'https://sms.findgridcode.com/index.php/user/SearchByGridCodes?grid_code=' . $gridCode . '&country_code=' . $countryCode . '&user_id=';

        $accessLogApp = app(AccessLogAPIController::class);
    
        $savedAccressLog =  $accessLogApp->storeOrUpdate([
            'sourceUrl' => $sourceUrl ? $sourceUrl : $url_here,
            'partnerId' => $partnerId,
            'searchedGridCode' => $gridCode,
            'conStartDateTime' => $this->getTodayDate(),
            'conEndDateTime' => $this->getTodayDate(),
            'logType'=>$logType
        ]);


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url_here);
        $result = curl_exec($ch);

        $response = json_decode($result, true);
        curl_close($ch);
        if ($response) {
            $savedAccressLog->conEndDateTime = $this->getTodayDate();
            $accessLogApp->storeOrUpdate($savedAccressLog);
        }

        if ($response && $response['status'] == 'success') {

            return ['success' => true, 'response' => $response['response']];
        }
        return ['success' => false, 'response' => $response['response']];
    }

    //https://papi.gridweb.net:8077

    function connectToGridNavAPI($gridCode = null, $countryCode = null, $partnerId = null, $sourceUrl = null)
    {

        return $this->apiRequest([
            'base_uri' => 'https://papi.gridweb.net:8077',
            'endPoint' => '/api/partnergridsearch?gridCode=' . $gridCode . '&countryCode=' . $countryCode,
            'token' => 'Basic ' . Configuration::getActiveConfigValue(ConfigKeyParameter::$GRID_CODE_SECRET_KEY)
        ]);
    }

    public function apiRequest($parmas = [])
    {
        $base_uri = $parmas['base_uri'] ? $parmas['base_uri'] : env('GRID_CODE_BASE_URL');
        $token = $parmas['token'] ? $parmas['token'] : '';
        $method = isset($parmas['method']) ? $parmas['method'] : 'GET';
        $endPoint = $parmas['endPoint'] ? $parmas['endPoint'] : '/';

        $client = new Client(['base_uri' => $base_uri]);


        $headers = [
            'Authorization' => $token,
            'Accept'        => 'application/json',
        ];

        $response = $client->request($method, $endPoint, [
            'headers' => $headers
        ]);

        return json_decode($response->getBody(), true);
    }

    public function sendToTwilio($phone_number)
    {
        /* Get credentials from .env */
        $token = Configuration::getActiveConfigValue(ConfigKeyParameter::$TWILIO_AUTH_TOKEN);
        $twilio_sid = Configuration::getActiveConfigValue(ConfigKeyParameter::$TWILIO_SID);
        $twilio_verify_sid = Configuration::getActiveConfigValue(ConfigKeyParameter::$TWILIO_VERIFY_SID);

     
           try {
            $twilio = new Twilio($twilio_sid, $token);

          $response= $twilio->verify->v2->services($twilio_verify_sid)
                ->verifications
                ->create($phone_number, "sms");
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }
    

    public function twilioVerificationCode($data = [])
    {

        /* Get credentials from .env */
        $token = Configuration::getActiveConfigValue(ConfigKeyParameter::$TWILIO_AUTH_TOKEN);
        $twilio_sid = Configuration::getActiveConfigValue(ConfigKeyParameter::$TWILIO_SID);
        $twilio_verify_sid = Configuration::getActiveConfigValue(ConfigKeyParameter::$TWILIO_VERIFY_SID);

        try {
            $twilio = new Twilio($twilio_sid, $token);

            $verification = $twilio->verify->v2->services($twilio_verify_sid)
                ->verificationChecks
                ->create($data['verification_code'], array('to' => $data['phone']));
            if ($verification->valid) {
                return true;
            }
            return false;
        } catch (\Exception $exception) {

            return false;
        }
    }


    public function connectToIPStack()
    {
        $hostInfo =  Larinfo::getHostIpinfo();
        $ip = $hostInfo['ip'];

        $ch = curl_init('http://api.ipstack.com/' . $ip . '?access_key=' . Configuration::getActiveConfigValue(ConfigKeyParameter::$IP_STACK_ACCESS_KEY) . '');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Store the data:
        $json = curl_exec($ch);
        curl_close($ch);
        // Decode JSON response:
        return json_decode($json, true);
    }

    //https://maps.googleapis.com/maps/api/geocode/json?latlng=44.4647452,7.3553838&key=YOUR_API_KEY
   
    public function connectToGoogleMapGeocode($lat='44.4647452', $long='7.3553838')
    {
      
        $ch = curl_init("https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&sensor=false&key=" . Configuration::getActiveConfigValue(ConfigKeyParameter::$GOOGLE_API_KEY));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Store the data:
        $json = curl_exec($ch);
        curl_close($ch);
        // Decode JSON response:
        return json_decode($json, true);

    }
    public function connectToGoogleMapDistanceMatrix($origins, $destinations,$travelmode='DRIVING',$traffic_model = "BEST_GUESS",$departure_time='now')
    {
       
        //...............TravelModes
        // DRIVING (Default) indicates standard driving directions using the road network.
        // BICYCLING requests bicycling directions via bicycle paths & preferred streets.
        // TRANSIT requests directions via public transit routes.
        // WALKING requests walking directions via pedestrian paths & sidewalks.
        
        //................TrafficModels constants
        // BEST_GUESS	(Default) Use historical traffic data to best estimate the time spent in traffic.
        // OPTIMISTIC	Use historical traffic data to make an optimistic estimate of what the duration in traffic will be.
        // PESSIMISTIC	Use historical traffic data to make a pessimistic estimate of what the duration in traffic will be.
       
        //...............TransitMode
        //BUS
        //RAIL
        //SUBWAY
        //TRAIN
        //TRAM

        $ch = curl_init('https://maps.googleapis.com/maps/api/distancematrix/json?origins=' . $origins . '&destinations=' . $destinations . '&departure_time=' . $departure_time . '&travelmode=' . strtolower($travelmode) . '&traffic_model=' . strtolower($traffic_model) . '&key=' . Configuration::getActiveConfigValue(ConfigKeyParameter::$GOOGLE_API_KEY) . '');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Store the data:
        $json = curl_exec($ch);
        curl_close($ch);
        // Decode JSON response:
        return json_decode($json, true);
    }

    public function connectToFlutterwaveMobileMoney($momoFormData = [], $token = null)
    {


        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.flutterwave.com/v3/charges?type=mobile_money_rwanda",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($momoFormData, true),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: Bearer " . $token
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $jsonResponse = json_decode($response, true);
        return $jsonResponse;
    }

    public function connectToFlutterwaveOptValidate($otp, $partnerId,$token = null)
    {


        $curl = curl_init();

        $url = $_COOKIE['flutterWaveCurrentRedirectuRL-'.$partnerId]. "?solution=" . $otp;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode(['solution' => $otp], true),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: Bearer " . $token
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $jsonResponse = json_decode($response, true);
        return $jsonResponse;
    }
}
