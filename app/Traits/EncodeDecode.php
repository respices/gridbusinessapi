<?php

namespace App\Traits;

use App\Models\Configuration;
use App\Models\GridNavAccessToken;
use Firebase\JWT\JWT;


trait EncodeDecode
{
    /**code

     * @var int

     */

    protected $code = 200;

    /** message

     * @var string

     */

    protected $message = 'success';

    /** Get Processing Code

     * @return int

     */

    public function getCode()

    {

        return $this->code;
    }

    /** Get Processing Message

     * @return string

     */

    public function getMessage()

    {

        return $this->message;
    }

    public function signEncode($payload, $hMacKey, $mac_algorithm = "sha256")
    {

        $init = http_build_query($payload);
        $signature = base64_encode(hash_hmac($mac_algorithm, $init, utf8_encode($hMacKey),true) .'payload='. $init);
        return $signature;
    }



    public function signDecrypt($initSign)
    {
       $sign = base64_decode($initSign);

        $payload = explode('payload=',$sign)[1];

        parse_str($payload, $signArr);

        return ['data' => $signArr, 'payload' => $payload];
    }

    public function signVerify($initSign, $hMacKey, $mac_algorithm = "sha256")
    {

        $decrypted = $this->signDecrypt($initSign);

        $signArr = $decrypted['data'];

        $tokenId= isset($signArr['tokenId'])?$signArr['tokenId']:0;

        $secretKeyIsValid = GridNavAccessToken::where('tokenId',$tokenId)->where('revoked',1)->first();

        if(!$secretKeyIsValid){
            $this->code = 400;

            $this->message = 'Secret Key revoked';

            return false;
        }
    
        if (isset($signArr) && isset($signArr['currentTimeStamp']) &&  isset($signArr['expireTime']) && $signArr['currentTimeStamp'] > $signArr['expireTime']) {

            $this->code = 400;

            $this->message = 'error currentTimeStamp';

            return false;
        }

        // expire

        if ($signArr && isset($signArr['enableExpireTime']) && isset($signArr['expireTime']) && $signArr['expireTime'] < time()) {

            $this->code = 400;

            $this->message = 'expireTime expired';

            return false;
        }

        // Calculate signature

        $computed_hash = base64_encode(hash_hmac($mac_algorithm, $decrypted['payload'] , utf8_encode($hMacKey),true) .'payload='. $decrypted['payload']);

        if (hash_equals($initSign,$computed_hash)) {
            
            $this->code = 200;

            $this->message = 'success';

            return true;
        } else {

            $this->code = 400;

            $this->message = 'error signature';

            return false;
        }
    }
}
