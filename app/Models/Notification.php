<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Notification
 * @package App\Models
 * @version June 25, 2021, 10:47 am UTC
 *
 * @property integer $notificationTypeId
 * @property integer $partnerId
 * @property string $message
 */
class Notification extends Model
{

    use HasFactory;

    public $table = 'notifications';
    



    public $fillable = [
        'notificationTypeId',
        'partnerId',
        'message'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'notificationTypeId' => 'integer',
        'partnerId' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'notificationTypeId' => 'required|integer',
        'partnerId' => 'required|string|exists:businessRegistrations,partnerId',
        'message' => 'required|string'
    ];

    public function notificationType()
    {
        return $this->belongsTo(NotificationType::class, 'notificationTypeId');
    }
    
    public function businessRegistration()
    {
        return $this->belongsTo(BusinessRegistration::class, 'partnerId');
    }
}
