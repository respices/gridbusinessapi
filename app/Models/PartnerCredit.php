<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class PartnerCredit
 * @package App\Models
 * @version August 19, 2021, 5:28 am EET
 *
 * @property string $partnerId
 * @property integer $partnerSubscriptionId
 * @property integer $credits
 */
class PartnerCredit extends Model
{

    use HasFactory;

    public $table = 'partner_credits';
    



    public $fillable = [
        'partnerId',
        'partnerSubscriptionId',
        'credits'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'partnerId' => 'string',
        'partnerSubscriptionId' => 'integer',
        'credits' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'partnerId' => 'required',
        'partnerSubscriptionId' => 'required',
        'credits' => 'required'
    ];

    
}
