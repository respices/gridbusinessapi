<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Privacy
 * @package App\Models
 * @version June 25, 2021, 10:08 am UTC
 *
 * @property integer $partnerId
 * @property boolean $showEmailInDirectory
 * @property boolean $showPhoneNumberInDirectory
 * @property boolean $showBranches
 * @property boolean $showInDirectory
 */
class Privacy extends Model
{

    use HasFactory;

    public $table = 'privacies';
    



    public $fillable = [
        'partnerId',
        'showEmailInDirectory',
        'showPhoneNumberInDirectory',
        'showBranches',
        'showInDirectory',
        'showContactPersonInDirectory',
        'allowUnsusedCreditRollover',
        'allowUnsusedSubscriptionRollover'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'partnerId' => 'string',
        'showEmailInDirectory' => 'boolean',
        'showPhoneNumberInDirectory' => 'boolean',
        'showBranches' => 'boolean',
        'showInDirectory' => 'boolean',
        'showContactPersonInDirectory' => 'boolean'
    ];
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'partnerId' => 'required|string|exists:businessRegistrations,partnerId',
        'showEmailInDirectory' => 'required|boolean',
        'showPhoneNumberInDirectory' => 'required|boolean',
        'showBranches' => 'required|boolean',
        'showInDirectory' => 'required|boolean',
        'showContactPersonInDirectory' => 'required|boolean'
    ];

    public function businessRegistration()
    {
        return $this->belongsTo(BusinessRegistration::class, 'partnerId');
    }
}
