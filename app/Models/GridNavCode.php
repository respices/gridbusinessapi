<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class GridNavCode
 * @package App\Models
 * @version June 22, 2021, 2:43 pm UTC
 *
 */
class GridNavCode extends Model
{

    use HasFactory;

    public $table = 'grid_codes';
    



    public $fillable = [
        'gridCodeId','latitude','longitude','gridCode','title'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'gridCode'=> 'string',
        'googleMapURL'=> 'string',
        'gridCodeId'=> 'integer',
        'latitude'=> 'string',
        'longitude'=> 'string',
        'title'=> 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
     
    ];

    
}
