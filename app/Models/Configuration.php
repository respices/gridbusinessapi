<?php

namespace App\Models;

use App\Enums\ConfigKeyParameter;
use App\Enums\Configuration as EnumsConfiguration;
use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;

/**
 * Class Configuration
 * @package App\Models
 * @version June 25, 2021, 10:17 am UTC
 *
 * @property string $parameter
 * @property string $value
 * @property string $status
 * @property string $hMacKey
 */
class Configuration extends Model
{

    use HasFactory;

    public $table = 'configurations';




    public $fillable = [
        'parameter',
        'value',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'parameter' => 'string',
        'value' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
   
    public function rules()
    {
        return [
            'parameter' => ['required',Rule::in(ConfigKeyParameter::parameters())],
            'value' => 'required|string',
            'status' => ['required',Rule::in(EnumsConfiguration::$status)],
        ];
    }
    
    public static function getActiveConfigValue($parameter)
    {
        $config = Configuration::where('parameter', $parameter)->where('status', 'Active')->first();
        if ($config) {
            return $config->value;
        }
        return null;
    }

    public static function getConfig(){
        Config::set('filesystems.disks.azure.name', self::getActiveConfigValue(ConfigKeyParameter::$AZURE_STORAGE_NAME));
        Config::set('filesystems.disks.azure.key', self::getActiveConfigValue(ConfigKeyParameter::$AZURE_STORAGE_KEY));
        Config::set('filesystems.disks.azure.container', self::getActiveConfigValue(ConfigKeyParameter::$AZURE_STORAGE_CONTAINER));
        Config::set('filesystems.disks.azure.url', self::getActiveConfigValue(ConfigKeyParameter::$AZURE_STORAGE_URL));

    }
}
