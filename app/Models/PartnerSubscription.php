<?php

namespace App\Models;

use App\Enums\PartnerSubscription as EnumsPartnerSubscription;
use App\Traits\GDateTime;
use Eloquent as Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Validation\Rule;

/**
 * Class PartnerSubscription
 * @package App\Models
 * @version June 30, 2021, 7:17 am UTC
 *
 * @property string $partnerId
 * @property integer $subscriptionId
 * @property string $dateSubscribed
 * @property string $status
 * @property integer $paymentId
 */
class PartnerSubscription extends Model
{

    use HasFactory;
    use GDateTime;

    public $table = 'partnerSubscriptions';


    public $fillable = [
        'partnerId',
        'subscriptionId',
        'dateSubscribed',
        'dateExpired',
        'subscriptionQuantity',
        'subscriptionPeriod',
        'paymentId',
        'status',
        'isReminderEmailSent',
        'isFreeSubscription',
        'currentCredits'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'partnerId' => 'string',
        'subscriptionId' => 'integer',
        'paymentId' => 'integer',
        'status' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */

    public function rules()
    {
        return [
            'dateSubscribed' => 'required|date',
            'subscriptionQuantity' => 'required|integer',
            'subscriptionPeriod' => 'required|integer',
            'paymentId' => 'required|integer',
            'subscriptionId' => 'required|integer|exists:subscriptions,id',
            'partnerId' => 'required|string|exists:businessRegistrations,partnerId',
            'status' => ['required', Rule::in(EnumsPartnerSubscription::$status)]
        ];
    }

    public function businessRegistration()
    {
        return $this->belongsTo(BusinessRegistration::class, 'partnerId');
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class, 'paymentId');
    }

    public function subscription()
    {
        return $this->belongsTo(Subscription::class, 'subscriptionId');
    }

    public static function rollOverUnsusedSubscription($partnerId)
    {
        $partner = BusinessRegistration::find($partnerId);
        $privacy = Privacy::where('partnerId', $partnerId)->first();
        if ($privacy && $privacy->allowUnsusedSubscriptionRollover) {
            PartnerSubscription::where('partnerId', $partner->partnerId)->where('status', 'Active')->update(['status', 'Canceled']);
        }
    }

    public static function addCurrentPartnerCredits($partnerSubscription, $currentPaidCredits)
    {
        $privacy = Privacy::where('partnerId', $partnerSubscription->partnerId)->first();



        $activePartnerSubscription = PartnerSubscription::where('partnerId', $partnerSubscription->partnerId)
            ->where('status', 'Active')->where('currentCredits', '>', 0)->where('isFreeSubscription', 0)->first();


        if ($privacy && !$privacy->allowUnsusedCreditRollover && $activePartnerSubscription) {

            $partnerSubscription->currentCredits = $activePartnerSubscription->currentCredits + $currentPaidCredits;
            $activePartnerSubscription->currentCredits = 0;
            $activePartnerSubscription->save();
        } else {
            $partnerSubscription->currentCredits =  $currentPaidCredits;
        }
        $partnerSubscription->save();
    }


    public static function makeAllActivePartnerSubscriptionToInactive($partnerId)
    {

        return PartnerSubscription::where('partnerId', $partnerId)->where('status', 'Active')->update(['status' => 'Inactive']);
    }

    public static function makePartnerSubscriptionActive($partnerId, $subscriptionId)
    {

        $recentPartnerSubscriptionActive = PartnerSubscription::where('partnerId', $partnerId)->where('status', 'Active')->first();
       
        $currentPartnerSubscriptionActive =  PartnerSubscription::find($subscriptionId);

        if ($recentPartnerSubscriptionActive &&  $currentPartnerSubscriptionActive) {
            
            $recentPartnerSubscriptionActive->status = 'Inactive';
            $currentPartnerSubscriptionActive->status = "Active";
            $currentPartnerSubscriptionActive->currentCredits = $recentPartnerSubscriptionActive->currentCredits;
            $recentPartnerSubscriptionActive->currentCredits = 0;

            $currentPartnerSubscriptionActive->save();
            $recentPartnerSubscriptionActive->save();
        }
    }
}
