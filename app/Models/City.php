<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class City
 * @package App\Models
 * @version June 25, 2021, 8:37 am UTC
 *
 * @property string $cityName
 */
class City extends Model
{

    use HasFactory;

    public $table = 'cities';
    



    public $fillable = [
        'cityName',
        'stateId'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'stateId'=> 'integer',
        'cityName' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'cityName' => 'required|string|max:255',
        'stateId'=> 'required|integer|exists:states,id'
    ];

    public function businessAddresses()
    {
        return $this->hasMany(BusinessAddress::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'stateId');
    }
}
