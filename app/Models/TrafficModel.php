<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class TrafficModel
 * @package App\Models
 * @version July 14, 2021, 10:36 am UTC
 *
 * @property string $model
 */
class TrafficModel extends Model
{

    use HasFactory;

    public $table = 'traffic_models';
    



    public $fillable = [
        'model',
        'default'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'model' => 'string',
        'default'=>'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'model' => 'required|string',
        'default' => 'boolean'
    ];

    public static function viewModel($modelId){
    
        if($modelId){
            $showModel = TrafficModel::find($modelId);
            if($showModel) return $showModel->model;
        }

        $showModel = TrafficModel::where('default',1)->first();
        
        if($showModel){
            return $showModel->model;
        }
        return null;
    }
}
