<?php

namespace App\Models;

use App\Traits\Uuids;
use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class BusinessRegistration
 * @package App\Models
 * @version June 25, 2021, 9:27 am UTC
 *
 * @property string $partnerId
 * @property string $businessName
 * @property string $businessEmail
 * @property string $description
 * @property string $website
 * @property string $logo
 * @property string $status
 * @property boolean $isHeadQuarter
 * @property string $headQuarterId
 * @property integer $businessCategoryId
 * @property integer $userId
 * @property integer $subscriptionId
 */
class BusinessRegistration extends Model
{

    use HasFactory;
    use Uuids;

    public $table = 'businessRegistrations';

    protected $primaryKey = 'partnerId';


    public $fillable = [
        'partnerId',
        'businessName',
        'businessEmail',
        'description',
        'website',
        'logo',
        'status',
        'isHeadQuarter',
        'headQuarterId',
        'businessCategoryId',
        'userId',
        'partnerCredits'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'partnerId' => 'string',
        'businessName' => 'string',
        'businessEmail' => 'string',
        'website' => 'string',
        'logo' => 'string',
        'status' => 'string',
        'isHeadQuarter' => 'boolean',
        'headQuarterId' => 'string',
        'businessCategoryId' => 'integer',
        'userId' => 'integer',
        'partnerCredits' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'businessName' => 'required|string|max:255',
        'businessEmail' => 'required|string|email|max:255',
        'description' => 'required|string',
        'website' => 'required|string',
        'businessCategoryId' => 'required|integer|exists:businessCategories,id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'userId');
    }

    public function businessCategory()
    {
        return $this->belongsTo(BusinessCategory::class, 'businessCategoryId');
    }

    public function subscriptions()
    {
        return $this->hasMany(PartnerSubscription::class, 'partnerId');
    }

    public function businessContacts()
    {
        return $this->hasMany(BusinessContact::class, 'businessContactId');
    }

    public function businessAddresses()
    {
        return $this->hasMany(BusinessAddress::class, 'partnerId');
    }

    public function privacies()
    {
        return $this->hasMany(Privacy::class, 'partnerId');
    }
    public function payments()
    {
        return $this->hasMany(Payment::class, 'partnerId');
    }

    public function accessLogs()
    {
        return $this->hasMany(AccessLog::class, 'partnerId');
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class, 'partnerId');
    }


    public function businessImages()
    {
        return $this->hasMany(BusinessImage::class, 'partnerId');
    }

    public static function isSubscriptionActive($partnerId)
    {
        return PartnerSubscription::where('partnerId',$partnerId)->where('status', 'Active')->first()? true : false;
    }

    public static function hasCredits($partnerId)
    {

        return PartnerSubscription::where('partnerId',$partnerId)
        ->where('status', 'Active')
        ->where('currentCredits','>', 0)->first()? true : false;
    }

    public static function isActive($partnerId)
    {
        return BusinessRegistration::where('partnerId', $partnerId)->where('status', 'Active')->first() ? true : false;
    }

    public static function reducePartnerCredits($partnerId = null,$api_url)
    {

       $partnerSubscription = PartnerSubscription::where('partnerId',$partnerId)
        ->where('status', 'Active')->first();

        $partnerSubscription->currentCredits = $partnerSubscription->currentCredits > 0?$partnerSubscription->currentCredits-1:0;
        $partnerSubscription->save();
        
        return $partnerSubscription;
    }
}
