<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class NotificationType
 * @package App\Models
 * @version June 25, 2021, 10:45 am UTC
 *
 * @property string $description
 */
class NotificationType extends Model
{

    use HasFactory;

    public $table = 'notificationTypes';
    



    public $fillable = [
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'description' => 'required|string'
    ];

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }
}
