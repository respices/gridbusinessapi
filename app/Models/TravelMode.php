<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class TravelMode
 * @package App\Models
 * @version July 14, 2021, 10:34 am UTC
 *
 * @property string $mode
 */
class TravelMode extends Model
{

    use HasFactory;

    public $table = 'travel_modes';
    



    public $fillable = [
        'mode','default'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'mode' => 'string',
        'default'=>'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'mode' => 'required|string',
        'default' => 'boolean'
    ];


    public static function viewMode($modeId){
    
        if($modeId){
            $showMode = TravelMode::find($modeId);
            if($showMode) return $showMode->mode;
        }

        $showMode = TravelMode::where('default',1)->first();
        
        if($showMode){
            return $showMode->mode;
        }
        return null;
    }
    
}
