<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class BusinessAddress
 * @package App\Models
 * @version June 25, 2021, 9:48 am UTC
 *
 * @property string $grideCode
 * @property integer $cityId
 * @property integer $stateId
 * @property integer $countryId
 * @property integer $partnerId
 * @property string $area
 * @property string $telephone
 * @property string $telephone2
 */
class BusinessAddress extends Model
{

    use HasFactory;

    public $table = 'businessAddresses';
    



    public $fillable = [
        'grideCode',
        'cityId',
        'stateId',
        'countryId',
        'partnerId',
        'address',
        'telephone',
        'telephone2',
        'isTelephoneVerified',
        'isTelephone2Verified',
        'landmark'
    ];

      /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'telephoneVerificationCode', 'telephone2VerificationCode'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'grideCode' => 'string',
        'cityId' => 'integer',
        'stateId' => 'integer',
        'countryId' => 'integer',
        'partnerId' => 'string',
        'address' => 'string',
        'telephone' => 'string',
        'telephone2' => 'string',
        'landmark'=> 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'grideCode' => 'required|string|max:9',
        'cityId' => 'required|integer|exists:cities,id',
        'stateId' => 'required|integer|exists:states,id',
        'countryId' => 'required|integer|exists:countries,id',
        'telephone' => 'required|string|max:13',
        'landmark'=> 'required|string'
    ];

    public static $rulesForUpdate = [
        'grideCode' => 'required|string|max:9|min:8',
        'cityId' => 'required|integer|exists:cities,id',
        'stateId' => 'required|integer|exists:states,id',
        'countryId' => 'required|integer|exists:countries,id',
        'partnerId' => 'required|string',
        'telephone' => 'required|string|max:13',
        'landmark'=> 'required|string'
    ];


    public function businessRegistration()
    {
        return $this->belongsTo(BusinessRegistration::class, 'partnerId');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'cityId');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'stateId');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'countryId');
    }
    
}
