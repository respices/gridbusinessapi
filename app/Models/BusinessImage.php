<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class BusinessImage
 * @package App\Models
 * @version June 25, 2021, 10:51 am UTC
 *
 * @property string $imageUrl
 * @property integer $partnerId
 */
class BusinessImage extends Model
{

    use HasFactory;

    public $table = 'businessImages';
    



    public $fillable = [
        'imageUrl',
        'partnerId'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'imageUrl' => 'string',
        'partnerId' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'file' => 'required|array',
        'file.*' => 'file|mimes:jpeg,png,jpg|max:1048',
        'partnerId' => 'required|string|exists:businessRegistrations,partnerId'
    ];

    public function businessRegistration()
    {
        return $this->belongsTo(BusinessRegistration::class, 'partnerId');
    }
    
    
}
