<?php

namespace App\Models;

use App\Enums\Payment as EnumsPayment;
use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Validation\Rule;

/**
 * Class Payment
 * @package App\Models
 * @version June 25, 2021, 10:30 am UTC
 *
 * @property number $amount
 * @property integer $currencyId
 * @property string $paymentStatus
 * @property integer $paymentMethod
 * @property string $paymentConfirmationId
 * @property integer $paymentGatewayId
 * @property integer $partnerId
 */
class Payment extends Model
{

    use HasFactory;

    public $table = 'payments';
    



    public $fillable = [
        'amount',
        'currencyId',
        'paymentStatus',
        'paymentMethod',
        'paymentConfirmationId',
        'paymentGatewayId',
        'partnerId'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'amount' => 'decimal:2',
        'currencyId' => 'integer',
        'paymentStatus' => 'string',
        'paymentMethod' => 'string',
        'paymentConfirmationId' => 'string',
        'paymentGatewayId' => 'integer',
        'partnerId' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
   
    public function rules()
    {
        return [
            'paymentMethod' => ['required',Rule::in(EnumsPayment::$paymentMethod)],
            'partnerId' => 'required|string|exists:businessRegistrations,partnerId',
            'subscriptionId' => 'required|integer|exists:subscriptions,id',
            'subscriptionQuantity'=> 'required|integer',
            'subscriptionPeriod'=> 'required',
        ];
    }


    public function optRules()
    {
        return [
            'otp' => 'required|integer',
            'partnerId' => 'required|string|exists:businessRegistrations,partnerId'
        ];
    }


    public function businessRegistration()
    {
        return $this->belongsTo(BusinessRegistration::class, 'partnerId');
    }

    public function paymentGateway()
    {
        return $this->belongsTo(PaymentGateway::class, 'paymentGatewayId');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currencyId');
    }

    public static function partnerSubscription($paymentID)
    {
        $partnerSubscription=PartnerSubscription::where('paymentId',$paymentID)->first();
        return $partnerSubscription?$partnerSubscription:null;
    }

    public static function subscription($paymentID)
    {
        $partnerSubscription=PartnerSubscription::where('paymentId',$paymentID)->first();
        return $partnerSubscription?$partnerSubscription->subscription:null;
    }

    public static function parnterPaymentStatus($partnerId,$paymentStatus='pending')
    {
        return Payment::where('partnerId',$partnerId)->where('paymentStatus',$paymentStatus)->first();
    }
}
