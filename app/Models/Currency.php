<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Currency
 * @package App\Models
 * @version June 25, 2021, 8:54 am UTC
 *
 * @property string $currencyName
 * @property string $currencySymbol
 */
class Currency extends Model
{

    use HasFactory;

    public $table = 'currencies';
    



    public $fillable = [
        'currencyName',
        'currencySymbol'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'currencyName' => 'string',
        'currencySymbol' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'currencyName' => 'required|string|max:255',
        'currencySymbol' => 'required|string|max:255'
    ];

    public function payments()
    {
        return $this->hasMany(Payment::class,'currencyId');
    }
}
