<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class BusinessCategory
 * @package App\Models
 * @version June 25, 2021, 8:56 am UTC
 *
 * @property string $description
 */
class BusinessCategory extends Model
{

    use HasFactory;

    public $table = 'businessCategories';
    



    public $fillable = [
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'description' => 'required|string|max:255'
    ];

    public function businessRegistrations()
    {
        return $this->hasMany(BusinessRegistration::class,'businessCategoryId');
    }
}
