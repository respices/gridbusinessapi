<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Country
 * @package App\Models
 * @version June 25, 2021, 8:27 am UTC
 *
 * @property string $countryCode
 * @property string $countryName
 */
class Country extends Model
{

    use HasFactory;

    public $table = 'countries';
    



    public $fillable = [
        'countryCode',
        'countryName'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'countryCode' => 'string',
        'countryName' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'countryCode' => 'required|string|max:2',
        'countryName' => 'required|string|max:255'
    ];

    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/

    public function states()
    {
        return $this->hasMany(State::class,'countryId');
    }
    public function businessAddresses()
    {
        return $this->hasMany(BusinessAddress::class,'partnerId');
    }
}
