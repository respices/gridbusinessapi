<?php

namespace App\Models;

use App\Traits\Uuids;
use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class BusinessRegistration
 * @package App\Models
 * @version June 25, 2021, 9:27 am UTC
 *
 * @property string $partnerId
 * @property string $businessName
 * @property string $businessEmail
 * @property string $description
 * @property string $website
 * @property string $logo
 * @property string $status
 * @property boolean $isHeadQuarter
 * @property string $headQuarterId
 * @property integer $businessCategoryId
 * @property integer $userId
 * @property integer $subscriptionId
 */
class GridNavAccessToken extends Model
{

    use HasFactory;
    use Uuids;

    public $table = 'partner_grid_nav_access_tokens';

    protected $primaryKey = 'tokenId';


    public $fillable = [
        'partnerId',
        'tokenId',
        'revoked',
        'token'
       
    ];

    protected $hidden = [
        'partnerId', 'revoked','created_at','updated_at'
    ];

}
