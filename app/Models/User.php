<?php

namespace App\Models;

use App\Models\BusinessRegistration;
use App\Notifications\VerifyApiEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use  HasApiTokens,  Notifiable;


    protected $guard = 'gridBusiness';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName','lastName', 'email', 'password','emailVerifyToken'
    ];
    

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function businessRegistrations()
    {
        return $this->hasMany(BusinessRegistration::class, 'userId');
    }

    public static function activeBusiness($userId=null){
        if($userId){
            return User::find($userId)?User::find($userId)->businessRegistrations->where('isHeadQuarter',1)->first():null;
        }else{
            return Auth::guard('gridBusiness')->user()?Auth::guard('gridBusiness')->user()->businessRegistrations->where('isHeadQuarter',1)->first():null;
        }
       
    }


    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value); // 
    }


    public function sendApiEmailVerificationNotification()
    {
        $this->notify(new VerifyApiEmail); // my notification
    }
}
