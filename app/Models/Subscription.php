<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Subscription
 * @package App\Models
 * @version June 25, 2021, 9:16 am UTC
 *
 * @property string $description
 * @property integer $credit
 * @property boolean $isDefault
 */
class Subscription extends Model
{

    use HasFactory;

    public $table = 'subscriptions';
    



    public $fillable = [
        'title',
        'description',
        'price',
        'currencyId',
        'credit',
        'isDefault'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'price' => 'decimal:2',
        'currencyId' => 'integer',
        'description' => 'string',
        'credit' => 'integer',
        'isDefault' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'price' => 'required|integer',
        'currencyId' => 'required|integer|exists:currencies,id',
        'description' => 'required|string|max:255',
        'title' => 'required|string|max:255',
        'credit' => 'required|integer',
        'isDefault' => 'required|boolean'
    ];

    public function businessRegistrations()
    {
        return $this->hasMany(BusinessRegistration::class);
    }
    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currencyId');
    }
}
