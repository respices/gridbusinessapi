<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class AccessLog
 * @package App\Models
 * @version June 25, 2021, 10:39 am UTC
 *
 * @property integer $partnerId
 * @property number $ipAddress
 * @property string $sourceUrl
 * @property string $conStartDateTime
 * @property string $conEndDateTime
 */
class AccessLog extends Model
{

    use HasFactory;

    public $table = 'accessLogs';
    



    public $fillable = [
        'partnerId',
        'searchedGridCode',
        'ipAddress',
        'sourceUrl',
        'conStartDateTime',
        'conEndDateTime',
        'sourceCountry',
        'logType'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'partnerId' => 'string',
        'searchedGridCode' => 'string',
        'sourceCountry' => 'string',
        'ipAddress' => 'string',
        'sourceUrl' => 'string',
        'conStartDateTime' => 'datetime',
        'conEndDateTime' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'partnerId' => 'required|string|exists:businessRegistrations,partnerId',
        'ipAddress' => 'required|ip',
        'sourceUrl' => 'required|url',
        'searchedGridCode' => 'required',
        'conStartDateTime' => 'required|date_format:Y-m-d H:i:s',
        'conEndDateTime' => 'required|date_format:Y-m-d H:i:s'
    ];
    
    public function businessRegistration()
    {
        return $this->belongsTo(BusinessRegistration::class, 'partnerId');
    }
    
}
