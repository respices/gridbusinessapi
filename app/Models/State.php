<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class State
 * @package App\Models
 * @version June 25, 2021, 8:47 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $countries
 * @property \App\Models\State $countryid
 * @property string $stateName
 * @property integer $countryId
 */
class State extends Model
{

    use HasFactory;

    public $table = 'states';
    



    public $fillable = [
        'stateName',
        'countryId'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'stateName' => 'string',
        'countryId' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'stateName' => 'required|string|max:255',
        'countryId' => 'required|integer|exists:countries,id'
    ];

   

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function country()
    {
        return $this->belongsTo(Country::class, 'countryId');
    }

    public function businessAddresses()
    {
        return $this->hasMany(BusinessAddress::class);
    }
}
