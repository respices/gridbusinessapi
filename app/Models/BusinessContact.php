<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class BusinessContact
 * @package App\Models
 * @version June 25, 2021, 10:49 am UTC
 *
 * @property integer $businessContactId
 * @property integer $partnerId
 */
class BusinessContact extends Model
{

    use HasFactory;

    public $table = 'businessContacts';
    



    public $fillable = [
        'businessContactId',
        'partnerId',
        'status',
        'action_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'businessContactId' => 'string',
        'partnerId' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'businessContactId' => 'required|string|exists:businessRegistrations,partnerId',
        'partnerId'         => 'required|string|exists:businessRegistrations,partnerId',
    ];


    public function businessRegistration()
    {
        return $this->belongsTo(BusinessRegistration::class, 'partnerId');
    }
    

    public function businessContact()
    {
        return $this->belongsTo(BusinessRegistration::class, 'businessContactId');
    }
}
