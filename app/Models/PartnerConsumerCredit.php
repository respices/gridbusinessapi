<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class PartnerConsumerCredit
 * @package App\Models
 * @version August 19, 2021, 5:32 am EET
 *
 * @property string $partnerId
 * @property integer $credit
 * @property string $api_url
 */
class PartnerConsumerCredit extends Model
{

    use HasFactory;

    public $table = 'partner_consumer_credits';
    



    public $fillable = [
        'partnerId',
        'credit',
        'api_url'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'partnerId' => 'string',
        'credit' => 'integer',
        'api_url' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
