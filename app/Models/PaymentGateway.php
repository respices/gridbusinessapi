<?php

namespace App\Models;

use App\Enums\PaymentGateway as EnumsPaymentGateway;
use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Validation\Rule;

/**
 * Class PaymentGateway
 * @package App\Models
 * @version June 25, 2021, 10:14 am UTC
 *
 * @property string $paymentGatewayName
 * @property string $publicKey
 * @property string $secretKey
 * @property string $status
 * @property string $lastUsedDate
 */
class PaymentGateway extends Model
{

    use HasFactory;

    public $table = 'paymentGateways';




    public $fillable = [
        'paymentGatewayName',
        'publicKey',
        'secretKey',
        'status',
        'lastUsedDate'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'paymentGatewayName' => 'string',
        'publicKey' => 'string',
        'secretKey' => 'string',
        'status' => 'string',
        'lastUsedDate' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    

    public function rules()
    {
        return [
            'paymentGatewayName' => 'required|string|max:255',
            'publicKey' => 'required|string',
            'secretKey' => 'required|string',
            'lastUsedDate' => 'required|date',
            'status' => ['required',Rule::in(EnumsPaymentGateway::$status)]
        ];
    }

    public function payments()
    {
        return $this->hasMany(Payment::class, 'paymentGatewayId');
    }

    public static function activeGateway($paymentGatewayName)
    {
        return PaymentGateway::where('status','Active')->where('paymentGatewayName',$paymentGatewayName)->orderBy('lastUsedDate','desc')->first();
    }
}
