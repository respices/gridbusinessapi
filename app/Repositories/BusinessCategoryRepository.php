<?php

namespace App\Repositories;

use App\Models\BusinessCategory;
use App\Repositories\BaseRepository;

/**
 * Class BusinessCategoryRepository
 * @package App\Repositories
 * @version June 25, 2021, 8:56 am UTC
*/

class BusinessCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BusinessCategory::class;
    }
}
