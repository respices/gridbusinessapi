<?php

namespace App\Repositories;

use App\Models\Privacy;
use App\Repositories\BaseRepository;

/**
 * Class PrivacyRepository
 * @package App\Repositories
 * @version June 25, 2021, 10:08 am UTC
*/

class PrivacyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'partnerId',
        'showEmailInDirectory',
        'showPhoneNumberInDirectory',
        'showBranches',
        'showInDirectory'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Privacy::class;
    }
}
