<?php

namespace App\Repositories;

use App\Models\GridNavCode;
use App\Repositories\BaseRepository;

/**
 * Class GridNavCodeRepository
 * @package App\Repositories
 * @version June 22, 2021, 2:43 pm UTC
*/

class GridNavCodeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return GridNavCode::class;
    }
}
