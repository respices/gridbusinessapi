<?php

namespace App\Repositories;

use App\Models\Configuration;
use App\Repositories\BaseRepository;

/**
 * Class ConfigurationRepository
 * @package App\Repositories
 * @version June 25, 2021, 10:17 am UTC
*/

class ConfigurationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'parameter',
        'value',
        'status',
        'hMacKey'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Configuration::class;
    }
}
