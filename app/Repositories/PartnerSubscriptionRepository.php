<?php

namespace App\Repositories;

use App\Models\PartnerSubscription;
use App\Repositories\BaseRepository;

/**
 * Class PartnerSubscriptionRepository
 * @package App\Repositories
 * @version June 30, 2021, 7:17 am UTC
*/

class PartnerSubscriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'partnerId',
        'subscriptionId',
        'dateSubscribed',
        'status',
        'paymentId'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PartnerSubscription::class;
    }
}
