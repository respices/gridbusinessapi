<?php

namespace App\Repositories;

use App\Models\BusinessRegistration;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;


/**
 * Class BusinessRegistrationRepository
 * @package App\Repositories
 * @version June 25, 2021, 9:27 am UTC
 */

class BusinessRegistrationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'partnerId',
        'businessName',
        'businessEmail',
        'description',
        'website',
        'logo',
        'status',
        'isHeadQuarter',
        'headQuarterId',
        'businessCategoryId',
        'userId'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BusinessRegistration::class;
    }

    public function businessQueries(Request $request)
    {
        $businessRegistrations = BusinessRegistration::join('businessAddresses', 'businessAddresses.partnerId', '=', 'businessRegistrations.partnerId')
            ->join('countries', 'businessAddresses.countryId', '=', 'countries.id')
            ->join('businessCategories',  'businessCategories.id','=','businessRegistrations.businessCategoryId')
            ->join('states', 'businessAddresses.stateId', '=', 'states.id')
            ->join('cities', 'businessAddresses.cityId', '=', 'cities.id');

  
        if ($request->has('userId')) {
            $businessRegistrations->where('businessRegistrations.userId', $request->get('userId'));
        }
        if ($request->has('partnerId')) {
            $businessRegistrations->where('businessRegistrations.partnerId', $request->get('partnerId'));
        }

        if ($request->has('countryId')) {
            $businessRegistrations->where('businessAddresses.countryId', $request->get('countryId'));
        }

        if ($request->has('stateId')) {
            $businessRegistrations->where('businessAddresses.stateId', $request->get('stateId'));
        }

        if ($request->has('cityId')) {
            $businessRegistrations->where('businessAddresses.cityId', $request->get('cityId'));
        }
        if ($request->has('grideCode')) {
            $businessRegistrations->where('businessAddresses.grideCode', $request->get('grideCode'));
        }
        if ($request->has('businessCategoryId')) {
            $businessRegistrations->where('businessRegistrations.businessCategoryId', $request->get('businessCategoryId'));
        }
        if ($request->has('telephone')) {
            $businessRegistrations->where('businessAddresses.telephone', $request->get('telephone'));
        }

        if ($request->has('telephone2')) {
            $businessRegistrations->where('businessAddresses.telephone2', $request->get('telephone2'));
        }
        if ($request->has('query')) {
            $businessRegistrations->orWhere('businessRegistrations.businessName', 'LIKE', '%' . $request->get('query') . '%')
                ->orWhere('businessAddresses.address', 'LIKE', '%' . $request->get('query') . '%')
                ->orWhere('businessAddresses.grideCode',$request->get('query'))
                ->orWhere('countries.countryCode',  $request->get('query'))
                ->orWhere('countries.countryName', 'LIKE', '%' . $request->get('query') . '%')
                ->orWhere('businessCategories.description', 'LIKE', '%' . $request->get('query') . '%')
                ->orWhere('states.stateName', 'LIKE', '%' . $request->get('query') . '%')
                ->orWhere('cities.cityName', 'LIKE', '%' . $request->get('query') . '%');
        }

        $businessRegistrationss =  $businessRegistrations->orderBy('businessRegistrations.created_at', 'desc')
            ->groupBy('businessRegistrations.partnerId')
            ->get();

        return  $businessRegistrationss;
    }
}
