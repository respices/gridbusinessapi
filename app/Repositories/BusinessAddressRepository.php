<?php

namespace App\Repositories;

use App\Models\BusinessAddress;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;

/**
 * Class BusinessAddressRepository
 * @package App\Repositories
 * @version June 25, 2021, 9:48 am UTC
 */

class BusinessAddressRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'grideCode',
        'cityId',
        'stateId',
        'countryId',
        'partnerId',
        'address',
        'telephone',
        'telephone2'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BusinessAddress::class;
    }

    public function businessAddressQueries(Request $request)
    {

        $businessAddresses = BusinessAddress::join('countries', 'businessAddresses.countryId', '=', 'countries.id')
            ->join('states', 'businessAddresses.stateId', '=', 'states.id')
            ->join('cities', 'businessAddresses.cityId', '=', 'cities.id');

        if ($request->has('partnerId')) {
            $businessAddresses->where('businessAddresses.partnerId', $request->get('partnerId'));
        }

        if ($request->has('countryId')) {
            $businessAddresses->where('businessAddresses.countryId', $request->get('countryId'));
        }

        if ($request->has('stateId')) {
            $businessAddresses->where('businessAddresses.stateId', $request->get('stateId'));
        }

        if ($request->has('cityId')) {
            $businessAddresses->where('businessAddresses.cityId', $request->get('cityId'));
        }
        if ($request->has('grideCode')) {
            $businessAddresses->where('businessAddresses.grideCode', $request->get('grideCode'));
        }
        if ($request->has('telephone')) {
            $businessAddresses->where('businessAddresses.telephone', $request->get('telephone'));
        }

        if ($request->has('telephone2')) {
            $businessAddresses->where('businessAddresses.telephone2', $request->get('telephone2'));
        }
        if ($request->has('query')) {
            $businessAddresses->orwhere('businessAddresses.address', 'LIKE', '%' . $request->get('query') . '%')
                ->orwhere('businessAddresses.grideCode', 'LIKE', '%' . $request->get('query') . '%')
                ->orwhere('countries.countryCode', $request->get('query'))
                ->orwhere('countries.countryName', 'LIKE', '%' . $request->get('query') . '%')
                ->orwhere('states.stateName', 'LIKE', '%' . $request->get('query') . '%')
                ->orwhere('cities.cityName', 'LIKE', '%' . $request->get('query') . '%');
        }

        return  $businessAddresses->orderBy('businessAddresses.created_at', 'desc')
            ->groupBy('businessAddresses.id')
            ->get(["businessAddresses.id as businessId"]);
    }
}
