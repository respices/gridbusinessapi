<?php

namespace App\Repositories;

use App\Models\BusinessContact;
use App\Repositories\BaseRepository;

/**
 * Class BusinessContactRepository
 * @package App\Repositories
 * @version June 25, 2021, 10:49 am UTC
*/

class BusinessContactRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'businessContactId',
        'partnerId'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BusinessContact::class;
    }
}
