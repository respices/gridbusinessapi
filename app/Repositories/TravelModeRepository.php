<?php

namespace App\Repositories;

use App\Models\TravelMode;
use App\Repositories\BaseRepository;

/**
 * Class TravelModeRepository
 * @package App\Repositories
 * @version July 14, 2021, 10:34 am UTC
*/

class TravelModeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'mode'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TravelMode::class;
    }
}
