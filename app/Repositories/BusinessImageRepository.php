<?php

namespace App\Repositories;

use App\Models\BusinessImage;
use App\Repositories\BaseRepository;

/**
 * Class BusinessImageRepository
 * @package App\Repositories
 * @version June 25, 2021, 10:51 am UTC
*/

class BusinessImageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'imageUrl',
        'partnerId'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BusinessImage::class;
    }
}
