<?php

namespace App\Repositories;

use App\Models\PartnerSubscription;
use App\Models\Subscription;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;

/**
 * Class SubscriptionRepository
 * @package App\Repositories
 * @version June 25, 2021, 9:16 am UTC
*/

class SubscriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'description',
        'credit',
        'isDefault'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Subscription::class;
    }
    public function subscriptionQueries(Request $request)
    {
        $defaultSubscription=PartnerSubscription::where('partnerId',$request->get('partnerId'))->where('isFreeSubscription',1)->first();
        if($defaultSubscription){
            return Subscription::where('id','!=',$defaultSubscription->subscriptionId)->get();
        }else{
           return Subscription::orderBy('isDefault','desc')->get();
        }

    }
}
