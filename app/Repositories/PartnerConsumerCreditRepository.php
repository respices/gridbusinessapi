<?php

namespace App\Repositories;

use App\Models\PartnerConsumerCredit;
use App\Repositories\BaseRepository;

/**
 * Class PartnerConsumerCreditRepository
 * @package App\Repositories
 * @version August 19, 2021, 5:32 am EET
*/

class PartnerConsumerCreditRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'partnerId',
        'credit',
        'api_url'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PartnerConsumerCredit::class;
    }
}
