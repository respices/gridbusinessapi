<?php

namespace App\Repositories;

use App\Models\PartnerCredit;
use App\Repositories\BaseRepository;

/**
 * Class PartnerCreditRepository
 * @package App\Repositories
 * @version August 19, 2021, 5:28 am EET
*/

class PartnerCreditRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'partnerId',
        'partnerSubscriptionId',
        'credits'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PartnerCredit::class;
    }
}
