<?php

namespace App\Repositories;

use App\Models\TrafficModel;
use App\Repositories\BaseRepository;

/**
 * Class TrafficModelRepository
 * @package App\Repositories
 * @version July 14, 2021, 10:36 am UTC
*/

class TrafficModelRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'model'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TrafficModel::class;
    }
}
