<?php

namespace App\Repositories;

use App\Models\NotificationType;
use App\Repositories\BaseRepository;

/**
 * Class NotificationTypeRepository
 * @package App\Repositories
 * @version June 25, 2021, 10:45 am UTC
*/

class NotificationTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return NotificationType::class;
    }
}
