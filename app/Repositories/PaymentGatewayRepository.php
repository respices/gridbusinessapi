<?php

namespace App\Repositories;

use App\Models\PaymentGateway;
use App\Repositories\BaseRepository;

/**
 * Class PaymentGatewayRepository
 * @package App\Repositories
 * @version June 25, 2021, 10:14 am UTC
*/

class PaymentGatewayRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'paymentGatewayName',
        'publicKey',
        'secretKey',
        'status',
        'lastUsedDate'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PaymentGateway::class;
    }
}
