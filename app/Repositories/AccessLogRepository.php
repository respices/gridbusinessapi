<?php

namespace App\Repositories;

use App\Models\AccessLog;
use App\Repositories\BaseRepository;

/**
 * Class AccessLogRepository
 * @package App\Repositories
 * @version June 25, 2021, 10:39 am UTC
*/

class AccessLogRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'partnerId',
        'ipAddress',
        'sourceUrl',
        'conStartDateTime',
        'conEndDateTime',
        'sourceCountry',
        'searchedGridCode',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AccessLog::class;
    }

   

   
}
