<?php

namespace App\Enums;

final class BusinessContact
{
    public static $status = ['Pending','Accepted','Rejected'];
}