<?php

namespace App\Enums;

final class ConfigKeyParameter
{
    public static $GOOGLE_API_KEY = 'GOOGLE_API_KEY';
    public static $IP_STACK_ACCESS_KEY= 'IP_STACK_ACCESS_KEY';
    public static $TWILIO_VERIFY_SID= 'TWILIO_VERIFY_SID';
    public static $TWILIO_SID= 'TWILIO_SID';
    public static $TWILIO_AUTH_TOKEN= 'TWILIO_AUTH_TOKEN';
    public static $GRID_CODE_SECRET_KEY= 'GRID_CODE_SECRET_KEY';

    public static $AZURE_STORAGE_NAME ='AZURE_STORAGE_NAME';
    public static $AZURE_STORAGE_KEY ='AZURE_STORAGE_KEY';
    public static $AZURE_STORAGE_CONTAINER ='AZURE_STORAGE_CONTAINER';
    public static $AZURE_STORAGE_URL ='AZURE_STORAGE_URL';
    public static $HMAC_KEY ='hMacKey';
 
  /*
  @var string[] $keys
  */  
    public static function parameters(){
        return [
            self::$GOOGLE_API_KEY,
            self::$IP_STACK_ACCESS_KEY,
            self::$TWILIO_VERIFY_SID,
            self::$TWILIO_SID,
            self::$TWILIO_AUTH_TOKEN,
            self::$GRID_CODE_SECRET_KEY,
            self::$AZURE_STORAGE_NAME,
            self::$AZURE_STORAGE_KEY,
            self::$AZURE_STORAGE_CONTAINER,
            self::$AZURE_STORAGE_URL,
            self::$HMAC_KEY
        ];
    }
}