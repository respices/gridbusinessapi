<?php

namespace App\Enums;

final class BusinessRegistration
{
    public static $status = ['Active','Inactive'];
}