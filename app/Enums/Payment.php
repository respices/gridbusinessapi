<?php

namespace App\Enums;

final class Payment
{
    public static $paymentStatus = ['pending','failed','successful'];
    public static $paymentMethod = ['card','mobile-money','paypal','spenn','Free'];
}