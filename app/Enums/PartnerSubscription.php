<?php

namespace App\Enums;

final class PartnerSubscription
{
    public static $status = ['Active','Inactive','Suspended','Canceled','Expired','Pending'];
}