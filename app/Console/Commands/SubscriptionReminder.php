<?php

namespace App\Console\Commands;

use App\Mail\SendSubscriptionReminderMail;
use App\Models\PartnerSubscription;
use App\Traits\GDateTime;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SubscriptionReminder extends Command
{
    use GDateTime;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscription:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Respectively send an exclusive reminder to the partner via email.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {


        $dateExpired = $this->getNextDate('day', 5);
        $dateExpired = Carbon::parse($dateExpired)->format('Y-m-d');

        $partnerSubscriptions = PartnerSubscription::whereDate('dateExpired','=', $dateExpired)->where('isReminderEmailSent',0)->where('status', 'Active')->get();

        foreach ($partnerSubscriptions as $partnerSubscription) {

            $partner = $partnerSubscription->businessRegistration;
            $businessEmail = $partner->businessEmail;

                    Mail::to($businessEmail)->send(new SendSubscriptionReminderMail([
                        'businessName' => $partner->businessName,
                        'days' => 5,
                        'link' => config('app.portalDomainUrl') . '/mySubscription/'.$partner->partnerId
                    ]));

            $partnerSubscription->isReminderEmailSent = 1;
            $partnerSubscription->save();
        }

        $this->info('Successfully');
    }
}
