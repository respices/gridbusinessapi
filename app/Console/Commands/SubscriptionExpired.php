<?php

namespace App\Console\Commands;

use App\Mail\SubscriptionExpiredMail;
use App\Models\PartnerSubscription;
use App\Traits\GDateTime;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SubscriptionExpired extends Command
{
    use GDateTime;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscription:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Respectively send an exclusive email to tell the partner that email was expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $dateExpired = $this->getTodayDate();
        $dateExpired = Carbon::parse($dateExpired)->format('Y-m-d');

        $partnerSubscriptions = PartnerSubscription::whereDate('dateExpired','<=', $dateExpired)->where('status', 'Active')->get();

        foreach ($partnerSubscriptions as $partnerSubscription) {

            $partner       = $partnerSubscription->businessRegistration;
            $businessEmail = $partner->businessEmail;

                    Mail::to($businessEmail)->send(new SubscriptionExpiredMail([
                        'businessName' => $partner->businessName,
                        'days' => $dateExpired,
                        'subscription'=>"#".$partnerSubscription->id.' / '.$partnerSubscription->subscription->title.' Plan',
                        'link' => config('app.portalDomainUrl') . '/mySubscription/'.$partner->partnerId
                    ]));

            $partnerSubscription->status = 'Expired';
            $partnerSubscription->save();
        }

        $this->info('Successfully');
    }
}
