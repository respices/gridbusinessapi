<?php

namespace App\Common;


class EncryptDecrypt
{
	
	public static function decrypt(
        $message,
        $key,
        $mac_algorithm = 'sha1',
        $enc_algorithm = MCRYPT_RIJNDAEL_256,
        $enc_mode = MCRYPT_MODE_CBC
    ) {
        $message = base64_decode($message);
        $iv_size = mcrypt_get_iv_size($enc_algorithm, $enc_mode);

        $iv_dec = substr($message, 0, $iv_size);
        $message = substr($message, $iv_size);

        $message = mcrypt_decrypt($enc_algorithm, $key, $message, $enc_mode, $iv_dec);

        $mac_block_size = ceil(static::getMacAlgoBlockSize($mac_algorithm) / 8);
        $mac_dec = substr($message, 0, $mac_block_size);
        $message = substr($message, $mac_block_size);

        $mac = hash_hmac($mac_algorithm, $message, $key, true);

        if ($mac_dec == $mac) {
            return $password;
        } else {
            return false;
        }
    }

    public static function encrypt(
        $message,
        $key,
        $mac_algorithm = 'sha1',
        $enc_algorithm = MCRYPT_RIJNDAEL_256,
        $enc_mode = MCRYPT_MODE_CBC
    ) {

        $mac = hash_hmac($mac_algorithm, $message, $key, true);
        $mac = substr($mac, 0, ceil(static::getMacAlgoBlockSize($mac_algorithm) / 8));
        $message = $mac . $message;

        $iv_size = mcrypt_get_iv_size($enc_algorithm, $enc_mode);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

        $ciphertext = mcrypt_encrypt(
            $enc_algorithm,
            $key,
            $message,
            $enc_mode,
            $iv
        );

        return base64_encode($iv . $ciphertext);
    }

    public static function getMacAlgoBlockSize($algorithm = 'sha1')
    {
        switch ($algorithm) {
            case 'sha1': {
                    return 160;
                }
            default: {
                    return false;
                    break;
                }
        }
    }
}
