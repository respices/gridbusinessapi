<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('approvePayment', 'API\PaymentAPIController@approveTransaction');
Route::get('emailVerification', 'API\Auth\VerificationController@verifyFromWeb');

Route::domain(config('app.portalDomainUrl'))->group(function () {
    Route::get('/', 'GriBusinessAppController@portal');
    Route::get('/{all}', 'GriBusinessAppController@portal')->where('all', '.*');

});

// Route::domain(config('app.adminDomainUrl'))->group(function () {
//     Route::get('/', function () {
//         return view('welcome');
//     });
// });

// Route::get('/', function () {
//     return view('welcome');
// });




