<?php

use App\Http\Controllers\API\AccessLogAPIController as AdminAccessLogAPIController;
use App\Http\Controllers\API\Admin\APICallStatisticController;
use App\Http\Controllers\API\Admin\Auth\UserController;
use App\Http\Controllers\API\Admin\PartnersAPIController;
use App\Http\Controllers\API\Admin\PaymentAPIController as AdminPaymentAPIController;
use App\Http\Controllers\API\NotificationTypeAPIController as AdminNotificationTypeAPIController;
use App\Http\Controllers\API\PaymentGatewayAPIController as AdminPaymentGatewayAPIController;
use App\Http\Controllers\API\StateAPIController as AdminStateAPIController;
use App\Http\Controllers\API\SubscriptionAPIController as AdminSubscriptionAPIController;
use App\Http\Controllers\API\TrafficModelAPIController as AdminTrafficModelAPIController;
use App\Http\Controllers\API\TravelModeAPIController as AdminTravelModeAPIController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// forget password doc https://www.toptal.com/laravel/passport-tutorial-auth-user-access


###################################### PUBLIC API #######################
//'check.internet.connection'
Route::group(['middleware' => ['cors', 'json.response']], function () {

    Route::get('/', function () {
        return ['version' => '1.0', 'message' => 'Welcome to gridbusiness and gridnav api'];
    });

    ###################################### GRIDNAV API #######################

    Route::group(['prefix' => 'gridNav'], function () {

        Route::post('generatePublickey', 'GridNav\GeneratePublicKeyAPIController@store');

        //grid.nav.authorize
        Route::group(
            ['middleware' => ['grid.nav.authorize']],
            function () {
                Route::get('partnerGridSearch', 'GridNav\PartnerGridSearchAPIController@partnerGridSearch');
                Route::get('validateGridCode', 'GridNav\ValidateGridCodeAPIController@validateGridCode');
                Route::get('distanceMatrix', 'GridNav\DistanceMatrixAPIController@distanceMatrix');
                Route::get('distanceSingleToMulti ', 'GridNav\DistanceForSingleToMultiAPIController@distanceSingleToMulti');
            }
        );
    });
    ###################################### END GRIDNAV API #######################

    //#################################### GRID BUSINESS API ########################################### 

    Route::group(
        ['prefix' => 'gridBusiness'],
        function () {

            Route::post('/register', 'Auth\RegisterController@register');
            Route::post('/login', 'Auth\LoginController@login');

            Route::group(
                ['middleware' => ['auth:gridBusiness', 'scope:gridBusiness', 'last.user.activity']],
                function () {
                    Route::get('partnerGridSearch', 'GridNav\PartnerGridSearchAPIController@partnerGridSearch');
                    Route::get('me', 'Auth\UserController@me');
                    Route::post('logout', 'Auth\UserController@logout');
                    Route::post('email/verify', 'Auth\VerificationController@verify');
                    Route::get('email/resend', 'Auth\VerificationController@resend');



                    Route::group(
                        ['middleware' => ['isEmailVerified']],
                        function () {
                            Route::resource('businessPartners', BusinessRegistrationAPIController::class);
                            Route::post('updateProfile', 'Auth\UserController@updateProfile');
                            Route::post('addProfileImage', 'Auth\UserController@addProfileImage');
                            Route::post('changePassword', 'Auth\UserController@changePassword');
                            Route::get('deleteAccount', 'Auth\UserController@deleteAccount');
                            Route::get('subscriptions', [App\Http\Controllers\API\SubscriptionAPIController::class, 'index'])->name('subscriptions');
                            Route::get('businessCategories', [App\Http\Controllers\API\BusinessCategoryAPIController::class, 'index'])->name('businessCategories');
                            Route::get('countries', [App\Http\Controllers\API\CountryAPIController::class, 'index'])->name('countries');
                            Route::get('cities', [App\Http\Controllers\API\CityAPIController::class, 'index'])->name('cities');
                            Route::get('states', [App\Http\Controllers\API\StateAPIController::class, 'index'])->name('states');
                            Route::get('currencies', [App\Http\Controllers\API\CurrencyAPIController::class, 'index'])->name('currencies');
                            Route::resource('payments', App\Http\Controllers\API\PaymentAPIController::class);
                            

                            Route::post('paymentOtpValidate', 'PaymentAPIController@otpValidate');
                     
                            // Route::group(
                            //     ['middleware' => ['only.for.active.business']],
                            //     function () {

                                    Route::put('changePartnerBusinessStatus/{partnerId}', 'BusinessRegistrationAPIController@changePartnerBusinessStatus');
                                    Route::post('changePartnerBusinessLogo', 'BusinessRegistrationAPIController@changePartnerBusinessLogo');

                                  
                                    
                                   
                                    Route::get('notificationTypes', [NotificationTypeAPIController::class, 'index'])->name('notificationTypes');
                                    Route::get('paymentGateways', [PaymentGatewayAPIController::class, 'index'])->name('paymentGateways');
                                    Route::get('travelModes', [TravelModeAPIController::class, 'index'])->name('travelModes');
                                    Route::get('trafficModels', [TrafficModelAPIController::class, 'index'])->name('trafficModels');
                                    Route::resource('payments', PaymentAPIController::class);
                                    Route::post('paymentOtpValidate', 'PaymentAPIController@otpValidate');
                                    Route::post('freeSubscription', [App\Http\Controllers\API\PartnerSubscriptionAPIController::class,'storeFreeSubscription']);
                                    Route::resource('partnerSubscriptions', PartnerSubscriptionAPIController::class);
                                    
                                    Route::get('gridBusiness', 'GridBusinessAPIController@gridBusinessFinder');
                                    Route::resource('partnerAddresses', BusinessAddressAPIController::class);
                                    
                                    Route::get('businessAddressByPartnerID/{partnerId}', [App\Http\Controllers\API\BusinessAddressAPIController::class, 'businessAddressByPartnerID']);
                                    Route::get('businessLocations/{partnerId}', [App\Http\Controllers\API\BusinessRegistrationAPIController::class, 'getBusinessLocations']);
                                    Route::get('getPartnerGoogleMapLocationsCoordinates/{partnerId}', [App\Http\Controllers\API\BusinessRegistrationAPIController::class, 'getBusinessLocationsCoordinates']);
                                    Route::get('partnerPrivacy/{partnerId}', [App\Http\Controllers\API\PrivacyAPIController::class, 'getPartnerPrivacy']);
                                    Route::put('partnerPrivacy/{id}', [App\Http\Controllers\API\PrivacyAPIController::class, 'update']); 
                                    Route::get('partnerDashboardStats/{id}', [App\Http\Controllers\API\PartnerDashboardStatisticsAPIController ::class, 'index']); 
                                    
                                    
                                    Route::resource('accessLogs', AccessLogAPIController::class);
                                    Route::resource('notifications', NotificationAPIController::class);
                                    Route::resource('partnerContacts', BusinessContactAPIController::class);
                                    Route::resource('partnerImages', BusinessImageAPIController::class);
                                    Route::delete('partnerDeleteAllImages', [App\Http\Controllers\API\BusinessImageAPIController::class, 'destroyAll']);
                                    

                                    Route::get('/acceptBusinessContactRequest/{id}', 'BusinessContactAPIController@acceptBusinessContactRequest');
                                    Route::get('/rejectBusinessContactRequest/{id}', 'BusinessContactAPIController@rejectBusinessContactRequest');

                                    Route::get('/getPartnerRequests', 'BusinessContactAPIController@getPartnerRequests');
                                    Route::get('/getBusinessContactProfile', 'BusinessContactAPIController@getBusinessContactProfile');

                                    Route::get('/directorySearch', 'DirectorySearchAPIController@directorySearch');
                                    Route::get('/nearByPartners', 'NearByPartnersAPIController@nearByPartners');
                                    Route::get('/nearByRelatedPartners', 'NearByRelatedPartnersAPIController@nearByRelatedPartners');
                                    Route::delete('businessAddressByPartnerID/{partnerId}', 'BusinessAddressAPIController@destoryAllBusinessAddressByPartnerID');
                                    Route::put('resendTelephoneVerificationSms/{businessAddressId}', 'BusinessAddressAPIController@resendTelephoneToTwilio');
                                    Route::put('resendTelephone2VerificationSms/{businessAddressId}', 'BusinessAddressAPIController@resendTelephone2ToTwilio');
                                    Route::put('verifyTelephone/{businessAddressId}', 'BusinessAddressAPIController@verifyTelephone');
                                    Route::put('verifyTelephone2/{businessAddressId}', 'BusinessAddressAPIController@verifyTelephone2');

                                    Route::resource('partner_credits', PartnerCreditAPIController::class);
                                    Route::resource('partner_consumer_credits', PartnerConsumerCreditAPIController::class);
                                    Route::get('partnerGridNavApiKey/{partnerId}', 'GridNav\GeneratePublicKeyAPIController@index');
                                    Route::post('partnerGridNavApiKey', 'GridNav\GeneratePublicKeyAPIController@store');
                            //     }
                            // );
                        }
                    );
                }
            );
        }
    );

    ###################################### END GRIDBUSINESS API #######################

    //################################################### ADMIN API  ###########################

    Route::group(['prefix' => 'admin'], function () {

        Route::post('register', 'Admin\Auth\RegisterController@register');
        Route::post('login', 'Admin\Auth\LoginController@login');

        Route::group(
            ['middleware' => ['auth:admin', 'scope:admin']],
            function () {
                Route::get('me', 'Admin\Auth\UserController@me');
                Route::get('logout', 'Admin\Auth\UserController@logout');
                Route::get('partners', [PartnersAPIController::class, 'index'])->name('partners');
                Route::get('partnerCounts', [PartnersAPIController::class, 'partnerCounts'])->name('partnerCounts');
                Route::get('partnerSubscriptions', [PartnersAPIController::class, 'partnerSubscriptions'])->name('partnerSubscriptions');
                Route::get('allUsers', [UserController::class, 'allUsers'])->name('allUsers');
                Route::get('totalUsers', [UserController::class, 'totalUsers'])->name('totalUsers');
                Route::get('userLastLogin', [UserController::class, 'userLastLogin'])->name('userLastLogin');
                Route::get('payments', [AdminPaymentAPIController::class, 'index'])->name('payments');
                Route::get('allApiCalls', [AdminAccessLogAPIController::class, 'index'])->name('allApiCalls');
                Route::get('apiCallsStatistics', [APICallStatisticController::class, 'apiCallsStatistics'])->name('apiCallsStatistics');
                Route::get('gridCodeStatistics', 'GridCodeStatisticsAPIController@gridCodeStatistics');
                Route::resource('configurations', ConfigurationAPIController::class);
                Route::get('/directorySearch', 'DirectorySearchAPIController@directorySearch');
                Route::get('/nearByPartners', 'NearByPartnersAPIController@nearByPartners');
                Route::get('/nearByRelatedPartners', 'NearByRelatedPartnersAPIController@nearByRelatedPartners');
                Route::resource('countries', CountryAPIController::class);
                Route::resource('cities', CityAPIController::class);
                Route::resource('states', StateAPIController::class);
                Route::resource('currencies', CurrencyAPIController::class);
                Route::resource('businessCategories', BusinessCategoryAPIController::class);
                Route::resource('subscriptions', SubscriptionAPIController::class);
                Route::resource('notificationTypes', AdminNotificationTypeAPIController::class);
                Route::resource('paymentGateways', PaymentGatewayAPIController::class);
                Route::resource('travelModes', TravelModeAPIController::class);
                Route::resource('trafficModels', TrafficModelAPIController::class);
                Route::resource('privacies', PrivacyAPIController::class);
            });
    });
});
