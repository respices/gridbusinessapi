<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/
// Route::domain(config('app.apiDomainUrl'))->group(function () {
    // info("Load from chanell");
    // Broadcast::channel('App.User.{id}', function ($user, $id) {
    //     return (int) $user->id === (int) $id;
    // });

    Broadcast::channel('payment', function () {
        info("Load from chanell");
        return true;
    // });

});
