@component('mail::message')
# Dear, {{$user['firstName']}}

Please use the following security code for the {{ config('app.name') }} account {{$user['email']}}

<h2 style="padding: 0;
    font-family: 'Segoe UI Light','Segoe UI','Helvetica Neue Medium',Arial,sans-serif;
    font-size: 41px;
    color: #2672ec;">
Security code
</h2>


<center><h2> <pre> <code> {{$code}}</code> </pre> </h2></center>

If you did not create an account, no further action is required.
<br>
Thanks,<br>
{{ config('app.name') }}

@endcomponent