@component('mail::message')
# Hi {{ $details['businessName'] }}

Thanks for using our platform – we love having you as our customer. <br>
 Your subscription will expire in {{ $details['days'] }} days, so we thought we’d check in. <br>

If you want to continue taking advantage of our platform and <br>
 retain all your data and preferences, you can easily renew by going to <a href="{{ $details['link'] }}">{{ $details['link'] }}</a>. <br>

Thank you, <br>

{{ config('app.name') }}
@endcomponent
