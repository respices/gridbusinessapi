<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="shortcut icon" href="/images/logo.png" type="image/x-icon">
  <link href="https://fonts.googleapis.com/css?family=Work+Sans:200,300,400,500,600,700,800,900&display=swap"
    rel="stylesheet">

  <link href="https://cdn.jsdelivr.net/npm/morioh@1.0.9/dist/css/morioh.min.css" rel="stylesheet">
  <link href="https://cdn.morioh.net/fa/v5.13.0/css/fontawesome.min.css" rel="stylesheet">
  <link href="https://cdn.morioh.net/fa/v5.13.0/css/regular.min.css" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/animate.css@3.7.2/animate.min.css" rel="stylesheet">
  <link rel="stylesheet" href="/assets/vendors/font-awesome/css/fontawesome.css">
  <link rel="stylesheet" href="/assets/vendors/magnific-popup/magnific-popup.css">
  <link rel="stylesheet" href="/assets/vendors/slick/slick.css">
  <link rel="stylesheet" href="/assets/vendors/animate.css">
  <link rel="stylesheet" href="/assets/vendors/air-datepicker/css/datepicker.min.css">
  <link rel="stylesheet" href="/assets/vendors/jquery-ui/jquery-ui.min.css">
  <link rel="stylesheet" href="/assets/vendors/uploader/css/dropzone.min.css">
  <link rel="stylesheet" href="/assets/css/style.css">
  <base href="/">
  <link href="/css//.b338662d.css" rel="prefetch">
  <link href="/css/createBranchAddress.1fa27bb8.css" rel="prefetch">
  <link href="/css/createBusiness.1fa27bb8.css" rel="prefetch">
  <link href="/css/createBusinessAddress.1fa27bb8.css" rel="prefetch">
  <link href="/css/createBusinessLocation.1fa27bb8.css" rel="prefetch">
  <link href="/css/createBusinessSubscription~createNewSubscription.65c3c66f.css" rel="prefetch">
  <link
    href="/css/editBusinessLocationStepper~myBusiness~mySubscription~uploadBranchImages~uploadBusinessImages.5d5dfa27.css"
    rel="prefetch">
  <link href="/css/editBusinessLocationStepper~myBusiness~uploadBranchImages~uploadBusinessImages.9a181b04.css"
    rel="prefetch">
  <link href="/css/login.d5b6b023.css" rel="prefetch">
  <link href="/css/myBusiness.020c1c58.css" rel="prefetch">
  <link href="/css/myPayment.65d536cd.css" rel="prefetch">
  <link href="/css/myProfile.03e6f64d.css" rel="prefetch">
  <link href="/css/mySetting.5af8127a.css" rel="prefetch">
  <link href="/css/register.d5b6b023.css" rel="prefetch">
  <link href="/css/verify.be12dee2.css" rel="prefetch">
  <link href="/css/verifyBranchBusinessAddressPhone~verifyBusinessAddressPhone.65c3c66f.css" rel="prefetch">
  <link href="/js//.d658dc46.js" rel="prefetch">
  <link href="/js/createBranchAddress.11085b70.js" rel="prefetch">
  <link href="/js/createBusiness.749d2363.js" rel="prefetch">
  <link href="/js/createBusinessAddress.296303dd.js" rel="prefetch">
  <link href="/js/createBusinessLocation.e111a01c.js" rel="prefetch">
  <link href="/js/createBusinessSubscription.691032e2.js" rel="prefetch">
  <link href="/js/createBusinessSubscription~createNewSubscription.c6452525.js" rel="prefetch">
  <link href="/js/createNewSubscription.59c57914.js" rel="prefetch">
  <link href="/js/editBusinessLocationStepper.8ac81050.js" rel="prefetch">
  <link
    href="/js/editBusinessLocationStepper~myBusiness~mySubscription~uploadBranchImages~uploadBusinessImages.f9ed0623.js"
    rel="prefetch">
  <link href="/js/editBusinessLocationStepper~myBusiness~uploadBranchImages~uploadBusinessImages.540b0677.js"
    rel="prefetch">
  <link href="/js/login.44a71f41.js" rel="prefetch">
  <link href="/js/myBusiness.abc47bdc.js" rel="prefetch">
  <link href="/js/myPayment.508790fd.js" rel="prefetch">
  <link href="/js/myProfile.1c320cfa.js" rel="prefetch">
  <link href="/js/mySetting.fc1f639c.js" rel="prefetch">
  <link href="/js/mySubscription.cdc9b4c7.js" rel="prefetch">
  <link href="/js/register.2b41f85b.js" rel="prefetch">
  <link href="/js/uploadBranchImages.e8411e6b.js" rel="prefetch">
  <link href="/js/uploadBusinessImages.82dc95e6.js" rel="prefetch">
  <link href="/js/verify.22019b0a.js" rel="prefetch">
  <link href="/js/verifyBranchBusinessAddressPhone.10831ae3.js" rel="prefetch">
  <link href="/js/verifyBranchBusinessAddressPhone~verifyBusinessAddressPhone.70f9c038.js" rel="prefetch">
  <link href="/js/verifyBusinessAddressPhone.a625b762.js" rel="prefetch">
  <link href="/css/app.b0805f6e.css" rel="preload" as="style">
  <link href="/css/chunk-vendors.69291852.css" rel="preload" as="style">
  <link href="/js/app.bcbb2487.js" rel="preload" as="script">
  <link href="/js/chunk-vendors.f0e63de1.js" rel="preload" as="script">
  <link href="/css/chunk-vendors.69291852.css" rel="stylesheet">
  <link href="/css/app.b0805f6e.css" rel="stylesheet">
    <base href="/">
</head>

<body style="background: #f0f0f0;">

@yield('content')

<script>
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/610e722c649e0a0a5cd000fa/1fcg6s0dv';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
 
 <script src="/assets/vendors/jquery.min.js"></script>
  <script src="/assets/vendors/jquery-ui/jquery-ui.min.js"></script>
  <script src="/assets/vendors/popper/popper.js"></script>
  <script src="/assets/vendors/bootstrap/js/bootstrap.js"></script>
  <script src="/assets/vendors/hc-sticky/hc-sticky.js"></script>
  <script src="/assets/vendors/isotope/isotope.pkgd.js"></script>
  <script src="/assets/vendors/magnific-popup/jquery.magnific-popup.js"></script>
  <script src="/assets/vendors/slick/slick.js"></script>
  <script src="/assets/vendors/waypoints/jquery.waypoints.js"></script>
  <script src="/assets/vendors/air-datepicker/js/datepicker.min.js"></script>
  <script src="/assets/vendors/air-datepicker/js/i18n/datepicker.en.js"></script>
  <script src="/assets/vendors/uploader/js/dropzone.min.js"></script>
  <script src="/assets/js/app.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJ-10ywLsARBlXZnKxnKrc2eHIlwl0YVg"></script>
  <script src="/js/chunk-vendors.f0e63de1.js"></script>
  <script src="/js/app.bcbb2487.js"></script>

</body>

</html>