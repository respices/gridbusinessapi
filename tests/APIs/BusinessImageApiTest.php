<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\BusinessImage;

class BusinessImageApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_business_image()
    {
        $businessImage = BusinessImage::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/business_images', $businessImage
        );

        $this->assertApiResponse($businessImage);
    }

    /**
     * @test
     */
    public function test_read_business_image()
    {
        $businessImage = BusinessImage::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/business_images/'.$businessImage->id
        );

        $this->assertApiResponse($businessImage->toArray());
    }

    /**
     * @test
     */
    public function test_update_business_image()
    {
        $businessImage = BusinessImage::factory()->create();
        $editedBusinessImage = BusinessImage::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/business_images/'.$businessImage->id,
            $editedBusinessImage
        );

        $this->assertApiResponse($editedBusinessImage);
    }

    /**
     * @test
     */
    public function test_delete_business_image()
    {
        $businessImage = BusinessImage::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/business_images/'.$businessImage->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/business_images/'.$businessImage->id
        );

        $this->response->assertStatus(404);
    }
}
