<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TravelMode;

class TravelModeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_travel_mode()
    {
        $travelMode = TravelMode::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/travel_modes', $travelMode
        );

        $this->assertApiResponse($travelMode);
    }

    /**
     * @test
     */
    public function test_read_travel_mode()
    {
        $travelMode = TravelMode::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/travel_modes/'.$travelMode->id
        );

        $this->assertApiResponse($travelMode->toArray());
    }

    /**
     * @test
     */
    public function test_update_travel_mode()
    {
        $travelMode = TravelMode::factory()->create();
        $editedTravelMode = TravelMode::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/travel_modes/'.$travelMode->id,
            $editedTravelMode
        );

        $this->assertApiResponse($editedTravelMode);
    }

    /**
     * @test
     */
    public function test_delete_travel_mode()
    {
        $travelMode = TravelMode::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/travel_modes/'.$travelMode->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/travel_modes/'.$travelMode->id
        );

        $this->response->assertStatus(404);
    }
}
