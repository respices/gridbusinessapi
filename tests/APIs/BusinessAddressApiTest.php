<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\BusinessAddress;

class BusinessAddressApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_business_address()
    {
        $businessAddress = BusinessAddress::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/business_addresses', $businessAddress
        );

        $this->assertApiResponse($businessAddress);
    }

    /**
     * @test
     */
    public function test_read_business_address()
    {
        $businessAddress = BusinessAddress::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/business_addresses/'.$businessAddress->id
        );

        $this->assertApiResponse($businessAddress->toArray());
    }

    /**
     * @test
     */
    public function test_update_business_address()
    {
        $businessAddress = BusinessAddress::factory()->create();
        $editedBusinessAddress = BusinessAddress::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/business_addresses/'.$businessAddress->id,
            $editedBusinessAddress
        );

        $this->assertApiResponse($editedBusinessAddress);
    }

    /**
     * @test
     */
    public function test_delete_business_address()
    {
        $businessAddress = BusinessAddress::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/business_addresses/'.$businessAddress->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/business_addresses/'.$businessAddress->id
        );

        $this->response->assertStatus(404);
    }
}
