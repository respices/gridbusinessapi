<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\BusinessContact;

class BusinessContactApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_business_contact()
    {
        $businessContact = BusinessContact::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/business_contacts', $businessContact
        );

        $this->assertApiResponse($businessContact);
    }

    /**
     * @test
     */
    public function test_read_business_contact()
    {
        $businessContact = BusinessContact::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/business_contacts/'.$businessContact->id
        );

        $this->assertApiResponse($businessContact->toArray());
    }

    /**
     * @test
     */
    public function test_update_business_contact()
    {
        $businessContact = BusinessContact::factory()->create();
        $editedBusinessContact = BusinessContact::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/business_contacts/'.$businessContact->id,
            $editedBusinessContact
        );

        $this->assertApiResponse($editedBusinessContact);
    }

    /**
     * @test
     */
    public function test_delete_business_contact()
    {
        $businessContact = BusinessContact::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/business_contacts/'.$businessContact->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/business_contacts/'.$businessContact->id
        );

        $this->response->assertStatus(404);
    }
}
