<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\BusinessRegistration;

class BusinessRegistrationApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_business_registration()
    {
        $businessRegistration = BusinessRegistration::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/business_registrations', $businessRegistration
        );

        $this->assertApiResponse($businessRegistration);
    }

    /**
     * @test
     */
    public function test_read_business_registration()
    {
        $businessRegistration = BusinessRegistration::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/business_registrations/'.$businessRegistration->id
        );

        $this->assertApiResponse($businessRegistration->toArray());
    }

    /**
     * @test
     */
    public function test_update_business_registration()
    {
        $businessRegistration = BusinessRegistration::factory()->create();
        $editedBusinessRegistration = BusinessRegistration::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/business_registrations/'.$businessRegistration->id,
            $editedBusinessRegistration
        );

        $this->assertApiResponse($editedBusinessRegistration);
    }

    /**
     * @test
     */
    public function test_delete_business_registration()
    {
        $businessRegistration = BusinessRegistration::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/business_registrations/'.$businessRegistration->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/business_registrations/'.$businessRegistration->id
        );

        $this->response->assertStatus(404);
    }
}
