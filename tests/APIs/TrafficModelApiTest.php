<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TrafficModel;

class TrafficModelApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_traffic_model()
    {
        $trafficModel = TrafficModel::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/traffic_models', $trafficModel
        );

        $this->assertApiResponse($trafficModel);
    }

    /**
     * @test
     */
    public function test_read_traffic_model()
    {
        $trafficModel = TrafficModel::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/traffic_models/'.$trafficModel->id
        );

        $this->assertApiResponse($trafficModel->toArray());
    }

    /**
     * @test
     */
    public function test_update_traffic_model()
    {
        $trafficModel = TrafficModel::factory()->create();
        $editedTrafficModel = TrafficModel::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/traffic_models/'.$trafficModel->id,
            $editedTrafficModel
        );

        $this->assertApiResponse($editedTrafficModel);
    }

    /**
     * @test
     */
    public function test_delete_traffic_model()
    {
        $trafficModel = TrafficModel::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/traffic_models/'.$trafficModel->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/traffic_models/'.$trafficModel->id
        );

        $this->response->assertStatus(404);
    }
}
