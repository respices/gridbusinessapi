<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\PartnerCredit;

class PartnerCreditApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_partner_credit()
    {
        $partnerCredit = PartnerCredit::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/partner_credits', $partnerCredit
        );

        $this->assertApiResponse($partnerCredit);
    }

    /**
     * @test
     */
    public function test_read_partner_credit()
    {
        $partnerCredit = PartnerCredit::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/partner_credits/'.$partnerCredit->id
        );

        $this->assertApiResponse($partnerCredit->toArray());
    }

    /**
     * @test
     */
    public function test_update_partner_credit()
    {
        $partnerCredit = PartnerCredit::factory()->create();
        $editedPartnerCredit = PartnerCredit::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/partner_credits/'.$partnerCredit->id,
            $editedPartnerCredit
        );

        $this->assertApiResponse($editedPartnerCredit);
    }

    /**
     * @test
     */
    public function test_delete_partner_credit()
    {
        $partnerCredit = PartnerCredit::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/partner_credits/'.$partnerCredit->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/partner_credits/'.$partnerCredit->id
        );

        $this->response->assertStatus(404);
    }
}
