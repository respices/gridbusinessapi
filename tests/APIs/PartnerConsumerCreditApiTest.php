<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\PartnerConsumerCredit;

class PartnerConsumerCreditApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_partner_consumer_credit()
    {
        $partnerConsumerCredit = PartnerConsumerCredit::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/partner_consumer_credits', $partnerConsumerCredit
        );

        $this->assertApiResponse($partnerConsumerCredit);
    }

    /**
     * @test
     */
    public function test_read_partner_consumer_credit()
    {
        $partnerConsumerCredit = PartnerConsumerCredit::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/partner_consumer_credits/'.$partnerConsumerCredit->id
        );

        $this->assertApiResponse($partnerConsumerCredit->toArray());
    }

    /**
     * @test
     */
    public function test_update_partner_consumer_credit()
    {
        $partnerConsumerCredit = PartnerConsumerCredit::factory()->create();
        $editedPartnerConsumerCredit = PartnerConsumerCredit::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/partner_consumer_credits/'.$partnerConsumerCredit->id,
            $editedPartnerConsumerCredit
        );

        $this->assertApiResponse($editedPartnerConsumerCredit);
    }

    /**
     * @test
     */
    public function test_delete_partner_consumer_credit()
    {
        $partnerConsumerCredit = PartnerConsumerCredit::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/partner_consumer_credits/'.$partnerConsumerCredit->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/partner_consumer_credits/'.$partnerConsumerCredit->id
        );

        $this->response->assertStatus(404);
    }
}
