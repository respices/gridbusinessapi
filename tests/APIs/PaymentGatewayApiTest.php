<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\PaymentGateway;

class PaymentGatewayApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_payment_gateway()
    {
        $paymentGateway = PaymentGateway::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/payment_gateways', $paymentGateway
        );

        $this->assertApiResponse($paymentGateway);
    }

    /**
     * @test
     */
    public function test_read_payment_gateway()
    {
        $paymentGateway = PaymentGateway::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/payment_gateways/'.$paymentGateway->id
        );

        $this->assertApiResponse($paymentGateway->toArray());
    }

    /**
     * @test
     */
    public function test_update_payment_gateway()
    {
        $paymentGateway = PaymentGateway::factory()->create();
        $editedPaymentGateway = PaymentGateway::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/payment_gateways/'.$paymentGateway->id,
            $editedPaymentGateway
        );

        $this->assertApiResponse($editedPaymentGateway);
    }

    /**
     * @test
     */
    public function test_delete_payment_gateway()
    {
        $paymentGateway = PaymentGateway::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/payment_gateways/'.$paymentGateway->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/payment_gateways/'.$paymentGateway->id
        );

        $this->response->assertStatus(404);
    }
}
