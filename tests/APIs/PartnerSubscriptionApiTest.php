<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\PartnerSubscription;

class PartnerSubscriptionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_partner_subscription()
    {
        $partnerSubscription = PartnerSubscription::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/partner_subscriptions', $partnerSubscription
        );

        $this->assertApiResponse($partnerSubscription);
    }

    /**
     * @test
     */
    public function test_read_partner_subscription()
    {
        $partnerSubscription = PartnerSubscription::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/partner_subscriptions/'.$partnerSubscription->id
        );

        $this->assertApiResponse($partnerSubscription->toArray());
    }

    /**
     * @test
     */
    public function test_update_partner_subscription()
    {
        $partnerSubscription = PartnerSubscription::factory()->create();
        $editedPartnerSubscription = PartnerSubscription::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/partner_subscriptions/'.$partnerSubscription->id,
            $editedPartnerSubscription
        );

        $this->assertApiResponse($editedPartnerSubscription);
    }

    /**
     * @test
     */
    public function test_delete_partner_subscription()
    {
        $partnerSubscription = PartnerSubscription::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/partner_subscriptions/'.$partnerSubscription->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/partner_subscriptions/'.$partnerSubscription->id
        );

        $this->response->assertStatus(404);
    }
}
