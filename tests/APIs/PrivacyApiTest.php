<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Privacy;

class PrivacyApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_privacy()
    {
        $privacy = Privacy::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/privacies', $privacy
        );

        $this->assertApiResponse($privacy);
    }

    /**
     * @test
     */
    public function test_read_privacy()
    {
        $privacy = Privacy::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/privacies/'.$privacy->id
        );

        $this->assertApiResponse($privacy->toArray());
    }

    /**
     * @test
     */
    public function test_update_privacy()
    {
        $privacy = Privacy::factory()->create();
        $editedPrivacy = Privacy::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/privacies/'.$privacy->id,
            $editedPrivacy
        );

        $this->assertApiResponse($editedPrivacy);
    }

    /**
     * @test
     */
    public function test_delete_privacy()
    {
        $privacy = Privacy::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/privacies/'.$privacy->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/privacies/'.$privacy->id
        );

        $this->response->assertStatus(404);
    }
}
