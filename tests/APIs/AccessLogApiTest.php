<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\AccessLog;

class AccessLogApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_access_log()
    {
        $accessLog = AccessLog::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/access_logs', $accessLog
        );

        $this->assertApiResponse($accessLog);
    }

    /**
     * @test
     */
    public function test_read_access_log()
    {
        $accessLog = AccessLog::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/access_logs/'.$accessLog->id
        );

        $this->assertApiResponse($accessLog->toArray());
    }

    /**
     * @test
     */
    public function test_update_access_log()
    {
        $accessLog = AccessLog::factory()->create();
        $editedAccessLog = AccessLog::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/access_logs/'.$accessLog->id,
            $editedAccessLog
        );

        $this->assertApiResponse($editedAccessLog);
    }

    /**
     * @test
     */
    public function test_delete_access_log()
    {
        $accessLog = AccessLog::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/access_logs/'.$accessLog->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/access_logs/'.$accessLog->id
        );

        $this->response->assertStatus(404);
    }
}
