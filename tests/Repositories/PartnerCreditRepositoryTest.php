<?php namespace Tests\Repositories;

use App\Models\PartnerCredit;
use App\Repositories\PartnerCreditRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PartnerCreditRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PartnerCreditRepository
     */
    protected $partnerCreditRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->partnerCreditRepo = \App::make(PartnerCreditRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_partner_credit()
    {
        $partnerCredit = PartnerCredit::factory()->make()->toArray();

        $createdPartnerCredit = $this->partnerCreditRepo->create($partnerCredit);

        $createdPartnerCredit = $createdPartnerCredit->toArray();
        $this->assertArrayHasKey('id', $createdPartnerCredit);
        $this->assertNotNull($createdPartnerCredit['id'], 'Created PartnerCredit must have id specified');
        $this->assertNotNull(PartnerCredit::find($createdPartnerCredit['id']), 'PartnerCredit with given id must be in DB');
        $this->assertModelData($partnerCredit, $createdPartnerCredit);
    }

    /**
     * @test read
     */
    public function test_read_partner_credit()
    {
        $partnerCredit = PartnerCredit::factory()->create();

        $dbPartnerCredit = $this->partnerCreditRepo->find($partnerCredit->id);

        $dbPartnerCredit = $dbPartnerCredit->toArray();
        $this->assertModelData($partnerCredit->toArray(), $dbPartnerCredit);
    }

    /**
     * @test update
     */
    public function test_update_partner_credit()
    {
        $partnerCredit = PartnerCredit::factory()->create();
        $fakePartnerCredit = PartnerCredit::factory()->make()->toArray();

        $updatedPartnerCredit = $this->partnerCreditRepo->update($fakePartnerCredit, $partnerCredit->id);

        $this->assertModelData($fakePartnerCredit, $updatedPartnerCredit->toArray());
        $dbPartnerCredit = $this->partnerCreditRepo->find($partnerCredit->id);
        $this->assertModelData($fakePartnerCredit, $dbPartnerCredit->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_partner_credit()
    {
        $partnerCredit = PartnerCredit::factory()->create();

        $resp = $this->partnerCreditRepo->delete($partnerCredit->id);

        $this->assertTrue($resp);
        $this->assertNull(PartnerCredit::find($partnerCredit->id), 'PartnerCredit should not exist in DB');
    }
}
