<?php namespace Tests\Repositories;

use App\Models\TravelMode;
use App\Repositories\TravelModeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TravelModeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TravelModeRepository
     */
    protected $travelModeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->travelModeRepo = \App::make(TravelModeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_travel_mode()
    {
        $travelMode = TravelMode::factory()->make()->toArray();

        $createdTravelMode = $this->travelModeRepo->create($travelMode);

        $createdTravelMode = $createdTravelMode->toArray();
        $this->assertArrayHasKey('id', $createdTravelMode);
        $this->assertNotNull($createdTravelMode['id'], 'Created TravelMode must have id specified');
        $this->assertNotNull(TravelMode::find($createdTravelMode['id']), 'TravelMode with given id must be in DB');
        $this->assertModelData($travelMode, $createdTravelMode);
    }

    /**
     * @test read
     */
    public function test_read_travel_mode()
    {
        $travelMode = TravelMode::factory()->create();

        $dbTravelMode = $this->travelModeRepo->find($travelMode->id);

        $dbTravelMode = $dbTravelMode->toArray();
        $this->assertModelData($travelMode->toArray(), $dbTravelMode);
    }

    /**
     * @test update
     */
    public function test_update_travel_mode()
    {
        $travelMode = TravelMode::factory()->create();
        $fakeTravelMode = TravelMode::factory()->make()->toArray();

        $updatedTravelMode = $this->travelModeRepo->update($fakeTravelMode, $travelMode->id);

        $this->assertModelData($fakeTravelMode, $updatedTravelMode->toArray());
        $dbTravelMode = $this->travelModeRepo->find($travelMode->id);
        $this->assertModelData($fakeTravelMode, $dbTravelMode->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_travel_mode()
    {
        $travelMode = TravelMode::factory()->create();

        $resp = $this->travelModeRepo->delete($travelMode->id);

        $this->assertTrue($resp);
        $this->assertNull(TravelMode::find($travelMode->id), 'TravelMode should not exist in DB');
    }
}
