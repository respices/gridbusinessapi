<?php namespace Tests\Repositories;

use App\Models\PartnerSubscription;
use App\Repositories\PartnerSubscriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PartnerSubscriptionRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PartnerSubscriptionRepository
     */
    protected $partnerSubscriptionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->partnerSubscriptionRepo = \App::make(PartnerSubscriptionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_partner_subscription()
    {
        $partnerSubscription = PartnerSubscription::factory()->make()->toArray();

        $createdPartnerSubscription = $this->partnerSubscriptionRepo->create($partnerSubscription);

        $createdPartnerSubscription = $createdPartnerSubscription->toArray();
        $this->assertArrayHasKey('id', $createdPartnerSubscription);
        $this->assertNotNull($createdPartnerSubscription['id'], 'Created PartnerSubscription must have id specified');
        $this->assertNotNull(PartnerSubscription::find($createdPartnerSubscription['id']), 'PartnerSubscription with given id must be in DB');
        $this->assertModelData($partnerSubscription, $createdPartnerSubscription);
    }

    /**
     * @test read
     */
    public function test_read_partner_subscription()
    {
        $partnerSubscription = PartnerSubscription::factory()->create();

        $dbPartnerSubscription = $this->partnerSubscriptionRepo->find($partnerSubscription->id);

        $dbPartnerSubscription = $dbPartnerSubscription->toArray();
        $this->assertModelData($partnerSubscription->toArray(), $dbPartnerSubscription);
    }

    /**
     * @test update
     */
    public function test_update_partner_subscription()
    {
        $partnerSubscription = PartnerSubscription::factory()->create();
        $fakePartnerSubscription = PartnerSubscription::factory()->make()->toArray();

        $updatedPartnerSubscription = $this->partnerSubscriptionRepo->update($fakePartnerSubscription, $partnerSubscription->id);

        $this->assertModelData($fakePartnerSubscription, $updatedPartnerSubscription->toArray());
        $dbPartnerSubscription = $this->partnerSubscriptionRepo->find($partnerSubscription->id);
        $this->assertModelData($fakePartnerSubscription, $dbPartnerSubscription->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_partner_subscription()
    {
        $partnerSubscription = PartnerSubscription::factory()->create();

        $resp = $this->partnerSubscriptionRepo->delete($partnerSubscription->id);

        $this->assertTrue($resp);
        $this->assertNull(PartnerSubscription::find($partnerSubscription->id), 'PartnerSubscription should not exist in DB');
    }
}
