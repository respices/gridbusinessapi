<?php namespace Tests\Repositories;

use App\Models\AccessLog;
use App\Repositories\AccessLogRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class AccessLogRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var AccessLogRepository
     */
    protected $accessLogRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->accessLogRepo = \App::make(AccessLogRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_access_log()
    {
        $accessLog = AccessLog::factory()->make()->toArray();

        $createdAccessLog = $this->accessLogRepo->create($accessLog);

        $createdAccessLog = $createdAccessLog->toArray();
        $this->assertArrayHasKey('id', $createdAccessLog);
        $this->assertNotNull($createdAccessLog['id'], 'Created AccessLog must have id specified');
        $this->assertNotNull(AccessLog::find($createdAccessLog['id']), 'AccessLog with given id must be in DB');
        $this->assertModelData($accessLog, $createdAccessLog);
    }

    /**
     * @test read
     */
    public function test_read_access_log()
    {
        $accessLog = AccessLog::factory()->create();

        $dbAccessLog = $this->accessLogRepo->find($accessLog->id);

        $dbAccessLog = $dbAccessLog->toArray();
        $this->assertModelData($accessLog->toArray(), $dbAccessLog);
    }

    /**
     * @test update
     */
    public function test_update_access_log()
    {
        $accessLog = AccessLog::factory()->create();
        $fakeAccessLog = AccessLog::factory()->make()->toArray();

        $updatedAccessLog = $this->accessLogRepo->update($fakeAccessLog, $accessLog->id);

        $this->assertModelData($fakeAccessLog, $updatedAccessLog->toArray());
        $dbAccessLog = $this->accessLogRepo->find($accessLog->id);
        $this->assertModelData($fakeAccessLog, $dbAccessLog->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_access_log()
    {
        $accessLog = AccessLog::factory()->create();

        $resp = $this->accessLogRepo->delete($accessLog->id);

        $this->assertTrue($resp);
        $this->assertNull(AccessLog::find($accessLog->id), 'AccessLog should not exist in DB');
    }
}
