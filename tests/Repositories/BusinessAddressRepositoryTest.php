<?php namespace Tests\Repositories;

use App\Models\BusinessAddress;
use App\Repositories\BusinessAddressRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class BusinessAddressRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var BusinessAddressRepository
     */
    protected $businessAddressRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->businessAddressRepo = \App::make(BusinessAddressRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_business_address()
    {
        $businessAddress = BusinessAddress::factory()->make()->toArray();

        $createdBusinessAddress = $this->businessAddressRepo->create($businessAddress);

        $createdBusinessAddress = $createdBusinessAddress->toArray();
        $this->assertArrayHasKey('id', $createdBusinessAddress);
        $this->assertNotNull($createdBusinessAddress['id'], 'Created BusinessAddress must have id specified');
        $this->assertNotNull(BusinessAddress::find($createdBusinessAddress['id']), 'BusinessAddress with given id must be in DB');
        $this->assertModelData($businessAddress, $createdBusinessAddress);
    }

    /**
     * @test read
     */
    public function test_read_business_address()
    {
        $businessAddress = BusinessAddress::factory()->create();

        $dbBusinessAddress = $this->businessAddressRepo->find($businessAddress->id);

        $dbBusinessAddress = $dbBusinessAddress->toArray();
        $this->assertModelData($businessAddress->toArray(), $dbBusinessAddress);
    }

    /**
     * @test update
     */
    public function test_update_business_address()
    {
        $businessAddress = BusinessAddress::factory()->create();
        $fakeBusinessAddress = BusinessAddress::factory()->make()->toArray();

        $updatedBusinessAddress = $this->businessAddressRepo->update($fakeBusinessAddress, $businessAddress->id);

        $this->assertModelData($fakeBusinessAddress, $updatedBusinessAddress->toArray());
        $dbBusinessAddress = $this->businessAddressRepo->find($businessAddress->id);
        $this->assertModelData($fakeBusinessAddress, $dbBusinessAddress->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_business_address()
    {
        $businessAddress = BusinessAddress::factory()->create();

        $resp = $this->businessAddressRepo->delete($businessAddress->id);

        $this->assertTrue($resp);
        $this->assertNull(BusinessAddress::find($businessAddress->id), 'BusinessAddress should not exist in DB');
    }
}
