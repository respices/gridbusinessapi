<?php namespace Tests\Repositories;

use App\Models\BusinessRegistration;
use App\Repositories\BusinessRegistrationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class BusinessRegistrationRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var BusinessRegistrationRepository
     */
    protected $businessRegistrationRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->businessRegistrationRepo = \App::make(BusinessRegistrationRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_business_registration()
    {
        $businessRegistration = BusinessRegistration::factory()->make()->toArray();

        $createdBusinessRegistration = $this->businessRegistrationRepo->create($businessRegistration);

        $createdBusinessRegistration = $createdBusinessRegistration->toArray();
        $this->assertArrayHasKey('id', $createdBusinessRegistration);
        $this->assertNotNull($createdBusinessRegistration['id'], 'Created BusinessRegistration must have id specified');
        $this->assertNotNull(BusinessRegistration::find($createdBusinessRegistration['id']), 'BusinessRegistration with given id must be in DB');
        $this->assertModelData($businessRegistration, $createdBusinessRegistration);
    }

    /**
     * @test read
     */
    public function test_read_business_registration()
    {
        $businessRegistration = BusinessRegistration::factory()->create();

        $dbBusinessRegistration = $this->businessRegistrationRepo->find($businessRegistration->id);

        $dbBusinessRegistration = $dbBusinessRegistration->toArray();
        $this->assertModelData($businessRegistration->toArray(), $dbBusinessRegistration);
    }

    /**
     * @test update
     */
    public function test_update_business_registration()
    {
        $businessRegistration = BusinessRegistration::factory()->create();
        $fakeBusinessRegistration = BusinessRegistration::factory()->make()->toArray();

        $updatedBusinessRegistration = $this->businessRegistrationRepo->update($fakeBusinessRegistration, $businessRegistration->id);

        $this->assertModelData($fakeBusinessRegistration, $updatedBusinessRegistration->toArray());
        $dbBusinessRegistration = $this->businessRegistrationRepo->find($businessRegistration->id);
        $this->assertModelData($fakeBusinessRegistration, $dbBusinessRegistration->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_business_registration()
    {
        $businessRegistration = BusinessRegistration::factory()->create();

        $resp = $this->businessRegistrationRepo->delete($businessRegistration->id);

        $this->assertTrue($resp);
        $this->assertNull(BusinessRegistration::find($businessRegistration->id), 'BusinessRegistration should not exist in DB');
    }
}
