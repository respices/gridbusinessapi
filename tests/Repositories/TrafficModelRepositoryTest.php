<?php namespace Tests\Repositories;

use App\Models\TrafficModel;
use App\Repositories\TrafficModelRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TrafficModelRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TrafficModelRepository
     */
    protected $trafficModelRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->trafficModelRepo = \App::make(TrafficModelRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_traffic_model()
    {
        $trafficModel = TrafficModel::factory()->make()->toArray();

        $createdTrafficModel = $this->trafficModelRepo->create($trafficModel);

        $createdTrafficModel = $createdTrafficModel->toArray();
        $this->assertArrayHasKey('id', $createdTrafficModel);
        $this->assertNotNull($createdTrafficModel['id'], 'Created TrafficModel must have id specified');
        $this->assertNotNull(TrafficModel::find($createdTrafficModel['id']), 'TrafficModel with given id must be in DB');
        $this->assertModelData($trafficModel, $createdTrafficModel);
    }

    /**
     * @test read
     */
    public function test_read_traffic_model()
    {
        $trafficModel = TrafficModel::factory()->create();

        $dbTrafficModel = $this->trafficModelRepo->find($trafficModel->id);

        $dbTrafficModel = $dbTrafficModel->toArray();
        $this->assertModelData($trafficModel->toArray(), $dbTrafficModel);
    }

    /**
     * @test update
     */
    public function test_update_traffic_model()
    {
        $trafficModel = TrafficModel::factory()->create();
        $fakeTrafficModel = TrafficModel::factory()->make()->toArray();

        $updatedTrafficModel = $this->trafficModelRepo->update($fakeTrafficModel, $trafficModel->id);

        $this->assertModelData($fakeTrafficModel, $updatedTrafficModel->toArray());
        $dbTrafficModel = $this->trafficModelRepo->find($trafficModel->id);
        $this->assertModelData($fakeTrafficModel, $dbTrafficModel->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_traffic_model()
    {
        $trafficModel = TrafficModel::factory()->create();

        $resp = $this->trafficModelRepo->delete($trafficModel->id);

        $this->assertTrue($resp);
        $this->assertNull(TrafficModel::find($trafficModel->id), 'TrafficModel should not exist in DB');
    }
}
