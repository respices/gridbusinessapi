<?php namespace Tests\Repositories;

use App\Models\BusinessContact;
use App\Repositories\BusinessContactRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class BusinessContactRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var BusinessContactRepository
     */
    protected $businessContactRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->businessContactRepo = \App::make(BusinessContactRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_business_contact()
    {
        $businessContact = BusinessContact::factory()->make()->toArray();

        $createdBusinessContact = $this->businessContactRepo->create($businessContact);

        $createdBusinessContact = $createdBusinessContact->toArray();
        $this->assertArrayHasKey('id', $createdBusinessContact);
        $this->assertNotNull($createdBusinessContact['id'], 'Created BusinessContact must have id specified');
        $this->assertNotNull(BusinessContact::find($createdBusinessContact['id']), 'BusinessContact with given id must be in DB');
        $this->assertModelData($businessContact, $createdBusinessContact);
    }

    /**
     * @test read
     */
    public function test_read_business_contact()
    {
        $businessContact = BusinessContact::factory()->create();

        $dbBusinessContact = $this->businessContactRepo->find($businessContact->id);

        $dbBusinessContact = $dbBusinessContact->toArray();
        $this->assertModelData($businessContact->toArray(), $dbBusinessContact);
    }

    /**
     * @test update
     */
    public function test_update_business_contact()
    {
        $businessContact = BusinessContact::factory()->create();
        $fakeBusinessContact = BusinessContact::factory()->make()->toArray();

        $updatedBusinessContact = $this->businessContactRepo->update($fakeBusinessContact, $businessContact->id);

        $this->assertModelData($fakeBusinessContact, $updatedBusinessContact->toArray());
        $dbBusinessContact = $this->businessContactRepo->find($businessContact->id);
        $this->assertModelData($fakeBusinessContact, $dbBusinessContact->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_business_contact()
    {
        $businessContact = BusinessContact::factory()->create();

        $resp = $this->businessContactRepo->delete($businessContact->id);

        $this->assertTrue($resp);
        $this->assertNull(BusinessContact::find($businessContact->id), 'BusinessContact should not exist in DB');
    }
}
