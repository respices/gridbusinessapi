<?php namespace Tests\Repositories;

use App\Models\PartnerConsumerCredit;
use App\Repositories\PartnerConsumerCreditRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PartnerConsumerCreditRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PartnerConsumerCreditRepository
     */
    protected $partnerConsumerCreditRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->partnerConsumerCreditRepo = \App::make(PartnerConsumerCreditRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_partner_consumer_credit()
    {
        $partnerConsumerCredit = PartnerConsumerCredit::factory()->make()->toArray();

        $createdPartnerConsumerCredit = $this->partnerConsumerCreditRepo->create($partnerConsumerCredit);

        $createdPartnerConsumerCredit = $createdPartnerConsumerCredit->toArray();
        $this->assertArrayHasKey('id', $createdPartnerConsumerCredit);
        $this->assertNotNull($createdPartnerConsumerCredit['id'], 'Created PartnerConsumerCredit must have id specified');
        $this->assertNotNull(PartnerConsumerCredit::find($createdPartnerConsumerCredit['id']), 'PartnerConsumerCredit with given id must be in DB');
        $this->assertModelData($partnerConsumerCredit, $createdPartnerConsumerCredit);
    }

    /**
     * @test read
     */
    public function test_read_partner_consumer_credit()
    {
        $partnerConsumerCredit = PartnerConsumerCredit::factory()->create();

        $dbPartnerConsumerCredit = $this->partnerConsumerCreditRepo->find($partnerConsumerCredit->id);

        $dbPartnerConsumerCredit = $dbPartnerConsumerCredit->toArray();
        $this->assertModelData($partnerConsumerCredit->toArray(), $dbPartnerConsumerCredit);
    }

    /**
     * @test update
     */
    public function test_update_partner_consumer_credit()
    {
        $partnerConsumerCredit = PartnerConsumerCredit::factory()->create();
        $fakePartnerConsumerCredit = PartnerConsumerCredit::factory()->make()->toArray();

        $updatedPartnerConsumerCredit = $this->partnerConsumerCreditRepo->update($fakePartnerConsumerCredit, $partnerConsumerCredit->id);

        $this->assertModelData($fakePartnerConsumerCredit, $updatedPartnerConsumerCredit->toArray());
        $dbPartnerConsumerCredit = $this->partnerConsumerCreditRepo->find($partnerConsumerCredit->id);
        $this->assertModelData($fakePartnerConsumerCredit, $dbPartnerConsumerCredit->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_partner_consumer_credit()
    {
        $partnerConsumerCredit = PartnerConsumerCredit::factory()->create();

        $resp = $this->partnerConsumerCreditRepo->delete($partnerConsumerCredit->id);

        $this->assertTrue($resp);
        $this->assertNull(PartnerConsumerCredit::find($partnerConsumerCredit->id), 'PartnerConsumerCredit should not exist in DB');
    }
}
