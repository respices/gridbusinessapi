<?php namespace Tests\Repositories;

use App\Models\BusinessImage;
use App\Repositories\BusinessImageRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class BusinessImageRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var BusinessImageRepository
     */
    protected $businessImageRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->businessImageRepo = \App::make(BusinessImageRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_business_image()
    {
        $businessImage = BusinessImage::factory()->make()->toArray();

        $createdBusinessImage = $this->businessImageRepo->create($businessImage);

        $createdBusinessImage = $createdBusinessImage->toArray();
        $this->assertArrayHasKey('id', $createdBusinessImage);
        $this->assertNotNull($createdBusinessImage['id'], 'Created BusinessImage must have id specified');
        $this->assertNotNull(BusinessImage::find($createdBusinessImage['id']), 'BusinessImage with given id must be in DB');
        $this->assertModelData($businessImage, $createdBusinessImage);
    }

    /**
     * @test read
     */
    public function test_read_business_image()
    {
        $businessImage = BusinessImage::factory()->create();

        $dbBusinessImage = $this->businessImageRepo->find($businessImage->id);

        $dbBusinessImage = $dbBusinessImage->toArray();
        $this->assertModelData($businessImage->toArray(), $dbBusinessImage);
    }

    /**
     * @test update
     */
    public function test_update_business_image()
    {
        $businessImage = BusinessImage::factory()->create();
        $fakeBusinessImage = BusinessImage::factory()->make()->toArray();

        $updatedBusinessImage = $this->businessImageRepo->update($fakeBusinessImage, $businessImage->id);

        $this->assertModelData($fakeBusinessImage, $updatedBusinessImage->toArray());
        $dbBusinessImage = $this->businessImageRepo->find($businessImage->id);
        $this->assertModelData($fakeBusinessImage, $dbBusinessImage->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_business_image()
    {
        $businessImage = BusinessImage::factory()->create();

        $resp = $this->businessImageRepo->delete($businessImage->id);

        $this->assertTrue($resp);
        $this->assertNull(BusinessImage::find($businessImage->id), 'BusinessImage should not exist in DB');
    }
}
