<?php namespace Tests\Repositories;

use App\Models\PaymentGateway;
use App\Repositories\PaymentGatewayRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PaymentGatewayRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PaymentGatewayRepository
     */
    protected $paymentGatewayRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->paymentGatewayRepo = \App::make(PaymentGatewayRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_payment_gateway()
    {
        $paymentGateway = PaymentGateway::factory()->make()->toArray();

        $createdPaymentGateway = $this->paymentGatewayRepo->create($paymentGateway);

        $createdPaymentGateway = $createdPaymentGateway->toArray();
        $this->assertArrayHasKey('id', $createdPaymentGateway);
        $this->assertNotNull($createdPaymentGateway['id'], 'Created PaymentGateway must have id specified');
        $this->assertNotNull(PaymentGateway::find($createdPaymentGateway['id']), 'PaymentGateway with given id must be in DB');
        $this->assertModelData($paymentGateway, $createdPaymentGateway);
    }

    /**
     * @test read
     */
    public function test_read_payment_gateway()
    {
        $paymentGateway = PaymentGateway::factory()->create();

        $dbPaymentGateway = $this->paymentGatewayRepo->find($paymentGateway->id);

        $dbPaymentGateway = $dbPaymentGateway->toArray();
        $this->assertModelData($paymentGateway->toArray(), $dbPaymentGateway);
    }

    /**
     * @test update
     */
    public function test_update_payment_gateway()
    {
        $paymentGateway = PaymentGateway::factory()->create();
        $fakePaymentGateway = PaymentGateway::factory()->make()->toArray();

        $updatedPaymentGateway = $this->paymentGatewayRepo->update($fakePaymentGateway, $paymentGateway->id);

        $this->assertModelData($fakePaymentGateway, $updatedPaymentGateway->toArray());
        $dbPaymentGateway = $this->paymentGatewayRepo->find($paymentGateway->id);
        $this->assertModelData($fakePaymentGateway, $dbPaymentGateway->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_payment_gateway()
    {
        $paymentGateway = PaymentGateway::factory()->create();

        $resp = $this->paymentGatewayRepo->delete($paymentGateway->id);

        $this->assertTrue($resp);
        $this->assertNull(PaymentGateway::find($paymentGateway->id), 'PaymentGateway should not exist in DB');
    }
}
