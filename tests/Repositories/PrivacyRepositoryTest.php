<?php namespace Tests\Repositories;

use App\Models\Privacy;
use App\Repositories\PrivacyRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PrivacyRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PrivacyRepository
     */
    protected $privacyRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->privacyRepo = \App::make(PrivacyRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_privacy()
    {
        $privacy = Privacy::factory()->make()->toArray();

        $createdPrivacy = $this->privacyRepo->create($privacy);

        $createdPrivacy = $createdPrivacy->toArray();
        $this->assertArrayHasKey('id', $createdPrivacy);
        $this->assertNotNull($createdPrivacy['id'], 'Created Privacy must have id specified');
        $this->assertNotNull(Privacy::find($createdPrivacy['id']), 'Privacy with given id must be in DB');
        $this->assertModelData($privacy, $createdPrivacy);
    }

    /**
     * @test read
     */
    public function test_read_privacy()
    {
        $privacy = Privacy::factory()->create();

        $dbPrivacy = $this->privacyRepo->find($privacy->id);

        $dbPrivacy = $dbPrivacy->toArray();
        $this->assertModelData($privacy->toArray(), $dbPrivacy);
    }

    /**
     * @test update
     */
    public function test_update_privacy()
    {
        $privacy = Privacy::factory()->create();
        $fakePrivacy = Privacy::factory()->make()->toArray();

        $updatedPrivacy = $this->privacyRepo->update($fakePrivacy, $privacy->id);

        $this->assertModelData($fakePrivacy, $updatedPrivacy->toArray());
        $dbPrivacy = $this->privacyRepo->find($privacy->id);
        $this->assertModelData($fakePrivacy, $dbPrivacy->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_privacy()
    {
        $privacy = Privacy::factory()->create();

        $resp = $this->privacyRepo->delete($privacy->id);

        $this->assertTrue($resp);
        $this->assertNull(Privacy::find($privacy->id), 'Privacy should not exist in DB');
    }
}
